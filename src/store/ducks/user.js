/**
 * Types
 */

export const Types = {
	USER: "USER",
	USER_FACE: "USER_FACE",
	USER_GOOGLE: "USER_GOOGLE",
};

/**
 * Reducers
 */

export const INITIAL_STATE = {
	user: {},
	face: {},
	google: {},
};

export default function User(state = INITIAL_STATE, action) {
	switch (action.type) {
		case Types.USER:
			return { user: action.data };

		case Types.USER_FACE:
			return { face: action.payload };

		case Types.USER_GOOGLE:
			return { google: action.payload };

		default:
			return state;
	}
}

/**
 * Actions
 */

export const Creators = {
	setUserInfsFace: (data) => ({
		type: Types.USER_FACE,
		payload: {
			...data,
		},
	}),

	setUserInfsGoogle: (data) => ({
		type: Types.USER_GOOGLE,
		payload: {
			...data,
		},
	}),
};
