/**
 * Types
 */

export const Types = {
	MENU_CIDADES_HOTEIS: "MENU_CIDADES_HOTEIS",
	MODAL_ORCAMENTO: "MODAL_ORCAMENTO",
	MODAL_SUCESSO_ORCAMENTO: "MODAL_SUCESSO_ORCAMENTO",
};

/**
 * Reducers
 */

export const INITIAL_STATE = {
	menu_cidade_hoteis: {
		active: false,
	},

	modal_orcamento: {
		active: false,
	},

	modal_sucesso_orcamento: {
		active: true,
	},
};

export default function Microinteracoes(state = INITIAL_STATE, action) {
	switch (action.type) {
		case Types.MENU_CIDADES_HOTEIS:
			return { ...state, menu_cidade_hoteis: { active: action.payload } };

		case Types.MODAL_ORCAMENTO:
			return { ...state, modal_orcamento: { active: action.payload } };

		case Types.MODAL_SUCESSO_ORCAMENTO:
			return {
				...state,
				modal_sucesso_orcamento: { active: action.payload },
			};

		default:
			return state;
	}
}

export const Creators = {
	toggleMenuCidadeHoteis: (active) => ({
		type: Types.MENU_CIDADES_HOTEIS,
		payload: {
			active,
		},
	}),

	ModalOrcamento: (active) => ({
		type: Types.MODAL_ORCAMENTO,
		payload: {
			active,
		},
	}),

	SucessoOrcamento: (active) => ({
		type: Types.MODAL_SUCESSO_ORCAMENTO,
		payload: {
			active,
		},
	}),
};
