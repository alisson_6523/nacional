/**
 * Types
 */

export const Types = {
	REQUEST_LOGIN: 'REQUEST_LOGIN',
	REQUEST_LOGIN_FACEBOOK: 'REQUEST_LOGIN_FACEBOOK',
	REQUEST_LOGIN_GOOGLE: 'REQUEST_LOGIN_GOOGLE',
	REQUEST_CREATE_USER: 'REQUEST_CREATE_USER',
	REQUEST_SUCCESS: 'REQUEST_SUCCESS',
	REQUEST_FAILURE: 'REQUEST_FAILURE',
}

/**
 * Reducers
 */

const INITIAL_STATE = {
	loading: false,
	error: false,
	token: false,
	authenticate: false,
}

export default function Login(state = INITIAL_STATE, action) {
	switch (action.type) {
		case Types.REQUEST_LOGIN:
			return { ...state, loading: true }

		case Types.REQUEST_CREATE_USER:
			return { ...state, loading: true }

		case Types.REQUEST_SUCCESS:
			return { loading: false, error: false, token: action.token }

		case Types.REQUEST_LOGIN_FACEBOOK:
			return { loading: false, error: false, token: action.payload.data }

		case Types.REQUEST_LOGIN_GOOGLE:
			return { loading: false, error: false, token: action.payload.data }

		case Types.REQUEST_FAILURE:
			return { ...state, loading: false, error: true }

		default:
			return state
	}
}

/**
 * Actions
 */

export const Creators = {
	requestLogin: data => ({
		type: Types.REQUEST_LOGIN,
		payload: {
			...data
		}
	}),

	requestCreateUser: data => ({
		type: Types.REQUEST_CREATE_USER,
		payload: {
			...data
		}
	}),

	requestLoginFaceBook: data => ({
		type: Types.REQUEST_LOGIN_FACEBOOK,
		payload: {
			data
		}
	}),

	requestLoginGoogle: data => ({
		type: Types.REQUEST_LOGIN_GOOGLE,
		payload: {
			data
		}
	})
}