import { all, takeLatest } from "redux-saga/effects";
import { Types as loginTypes } from "../ducks/login";
import { login, CreateUser } from "./login";

export default function* rootSaga() {
	yield all([
		takeLatest(loginTypes.REQUEST_LOGIN, login),
		takeLatest(loginTypes.REQUEST_CREATE_USER, CreateUser),
	]);
}
