import { put, call } from "redux-saga/effects";
import { Types as LoginTypes } from "../ducks/login";
import { Types as UserTypes } from "../ducks/user";

import { api, setToken } from "../../services/api";

export function* login({ payload }) {
	//teste@testeasdad.com
	const { value, history } = payload;

	try {
		const login = yield call(api.post, "login", { ...value });

		const { access_token: token } = login.data.token.original;

		const data = {
			name: login.data.hospede.nome,
			email: login.data.hospede.email,
			picture: login.data.hospede.avatar,
		};
		yield setToken(token);
		yield put({ type: LoginTypes.REQUEST_SUCCESS, token });
		yield put({ type: UserTypes.USER, data });

		history.push("/user");
	} catch (error) {
		console.log(error);
	}
}

export function* CreateUser({ payload }) {
	const { value, history } = payload;

	try {
		const response = yield call(api.post, "cadastro-rapido", { ...value });

		const {
			hospede,
			token: { original },
		} = response.data;

		const { access_token: token } = original;

		const data = {
			name: hospede.nome,
			email: hospede.email,
		};

		yield setToken(token);
		yield put({ type: LoginTypes.REQUEST_SUCCESS, token });
		yield put({ type: UserTypes.USER, data });
		history.push("/user");
	} catch (error) {
		console.log(error);
	}
}
