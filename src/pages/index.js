import React from "react";
import { Route } from "react-router-dom";
import Header from "../components/header";
import Footer from "../components/Footer";
import Home from "../pages/home";
import Detalhe from "../pages/Detalhes";
import CadastroDados from "../pages/CadastroDados";
import CadastroPagamento from "../pages/CadastroPagamento";
import ConfirmacaoDados from "../pages/CadastroConfirmacao";
import TodosHoteis from "../pages/TodosHoteis";
import QuemSomos from "../pages/QuemSomos";
import ScrollToTop from "../util/ScrollToTop";
import Grupos from "../pages/Grupos";
import Destinos from "../pages/TodosDestinos";
import Eventos from "../pages/Eventos";
import TodosEventos from "../pages/TodosEventos";
import Sala from "../pages/Salas";
import RoutesAuth from "../pages/Auth";
import Menu from "../components/Menu";
import Modais from "../components/Modal";

export default function pages(props) {
	return (
		<>
			<ScrollToTop />

			<Header {...props} />

			<Route exact path="/" component={Home} />
			<Route path="/detalhe" component={Detalhe} />
			<Route path="/cadastro-dados" component={CadastroDados} />
			<Route path="/cadastro-pagamento" component={CadastroPagamento} />
			<Route path="/confirmacao-dados" component={ConfirmacaoDados} />
			<Route path="/todos-hoteis" component={TodosHoteis} />
			<Route path="/quem-somos" component={QuemSomos} />
			<Route path="/grupos" component={Grupos} />
			<Route path="/destinos" component={Destinos} />
			<Route path="/eventos" component={Eventos} />
			<Route path="/todos-destino" component={TodosEventos} />
			<Route path="/sala" component={Sala} />

			<RoutesAuth />

			<Footer />

			<Menu />

			<Modais />
		</>
	);
}
