import React from 'react';

// import { Container } from './styles';
import HeaderDetalhe from '../../components/header/detalhe'
import Banner from '../../components/BannerPromo'
import Contem from '../../components/Contem'
import Reserva from '../../components/Reserva'
import Promocao from '../../components/Promocao'
import PrincipaisOpcoes from '../../components/PricipaisOpcoes'
import Hotel from '../../components/Hotel'

export default function Detalhes() {
	const title = {
		title: "Aproveite as ofertas e promoções para esse hotel",
		subTitle: "Temos ofertas e condições imperdiveis para  voce e sua família.",
	}

	return (
		<>
			<HeaderDetalhe />
			<Banner styledDefault="detalhe"/>
			<Banner styledDefault="galeria"/>
			<Contem />
			<Reserva />
			<Promocao title={title} detalhe={true}/>
			<PrincipaisOpcoes />
			<Hotel />
		</>
	);
}
