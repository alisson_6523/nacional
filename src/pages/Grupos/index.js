import React from 'react';

// import { Container } from './styles';
import BannerImg from '../../assets/quemSomos/somos-17.png'
import BannerQuemSomo from '../../components/BannerQuemSomos'
import Viagem from '../../components/ViagenGrupos'
import Amigo from '../../components/Amigos'

export default function Grupos() {
	const content = {
		title: "Descontos",
		subTitle: "especiais para grupos",
		img: BannerImg,
		style: 'right',
	}
	return (
		<>
			<BannerQuemSomo content={content} />
			<Viagem />
			<Amigo />
		</>
	);
}
