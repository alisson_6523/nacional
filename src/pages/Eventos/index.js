import React from 'react';

// import { Container } from './styles';
import Evento from '../../components/Evento'
import Orcamento from '../../components/Orcamento'
import TipoEvento from '../../components/TiposEventos'
import BannerSalas from '../../components/BannerSalas'
import Salas from '../../components/Salas'

function Eventos() {
	return (
		<>
			<Evento />
			<Orcamento />
			<TipoEvento />
			<BannerSalas />
			<Salas />
		</>
	);
}

export default Eventos;