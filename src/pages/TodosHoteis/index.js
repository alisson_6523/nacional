import React from 'react';
import BuscaHoteis from '../../components/BuscaHoteis'
import ListHotel from '../../components/listHotel'
import Todos from '../../components/TodosHoteis'
import Promocao from '../../components/Promocao'
import Banner from '../../components/BannerPromo'

export default function TodosHoteis() {
	const title = {
		title: "Aproveite as ofertas e promoções Poços de Caldas",
		subTitle: "Temos ofertas e condições imperdiveis para  voce e sua família.",
	}
	return (
		<>
			<ListHotel />
			<BuscaHoteis />
			<Todos />
			<Promocao title={title} detalhe={true}/>
			<Banner />
		</>
	);
}
