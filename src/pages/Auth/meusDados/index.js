import React from 'react';

import { Formik } from 'formik'

import MeusDadosUser from '../../../components/Forms/meusDadosUser'
import { ContainerMeusDados } from './styles'
import { FormInitialState, validadtionUserDados } from '../../../util/configForm'
import Title from '../../../components/titleUser'


function MeusDados() {
	return (
		<ContainerMeusDados>
			<div className="container">
				<Title />

				<Formik
					enableReinitialize

					initialValues={FormInitialState.userDados}

					onSubmit={(value, actions) => {
						console.log(value, actions)
					}}

					render={props => <MeusDadosUser {...props} />}

					validationSchema={validadtionUserDados}
				/>


			</div>
		</ContainerMeusDados>
	)
}

export default MeusDados;