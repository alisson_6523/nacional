import styled from 'styled-components';
import search from '../../../assets/header/header-21.svg'


export const ContainerHome = styled.section`
	padding-top: 56px;
	padding-bottom: 148px;
	.container{
		h1{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			margin-bottom: 19px;
		}

		.tabs{
			height: 53px;
			display: flex;
			width: 832px;
			align-items: center;
			border-top: 1px solid #E0E0E0;
			border-bottom: 1px solid #E0E0E0;
			margin-bottom: 24px;
			span{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 13px;
				line-height: 100%;
				opacity: 0.4;
				display: block;
				margin-right: 64px;
				cursor: pointer;
				&.active{
					opacity: 1;
					position: relative;
					&:before{
						content: '';
						display: block;
						width: 100%;
						height: 2px;
						background-color: #8AC537;
						position: absolute;
						bottom: -20px;
						left: 0px;
					}
				}
				&:last-child{
					margin-right: 0px;
				}
			}

			.container-search{
				position: relative;
				margin-left: auto;
				width: 150px;
				input{
					border: none;
					margin-left: auto;
    				padding-right: 20px;
					width: 100%;
				}
				&:before{
					content: '';
					display: block;
					width: 16px;
					height: 16px;
					position: absolute;
					top: 50%;
					right: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${search});
					-webkit-mask-repeat: no-repeat;
					background-color: #000;
					z-index: 20;
				}
			}
		}

		.container-cards{
			position: relative;
			svg{
				padding: 33px;	
				border: 1px solid #E0E0E0;
			}
			.reserva{
				position: absolute;
				top: 0px;
				left: 0px;
				opacity: 0;
				pointer-events: none;
				&.active{
					position: static;
					opacity: 1;
					pointer-events: all;
					transition: opacity .5s;
				}
			}
		}

	}
`;
