import React from 'react';

import { ContainerMinhaLista } from './styles';
import Title from '../../../components/titleUser'
import CardsMinhaLista from '../../../components/CardMinhaLista'


function MinhaLista() {
	return (
		<ContainerMinhaLista>
			<div className="container">
				<Title title="Minha Lista" />

				<div className="container-cards">
					<CardsMinhaLista />
					<CardsMinhaLista />
					<CardsMinhaLista />
					<CardsMinhaLista />
				</div>
			</div>
		</ContainerMinhaLista>
	);
}

export default MinhaLista;