import React from 'react';

import { Container } from './styles';
import Destino from '../../components/TodosDestinos'
import BuscaHoteis from '../../components/BuscaHoteis'


export default function TodosDestinos() {
	return (
		<>
			<BuscaHoteis />
			<Destino />
		</>
	);
}
