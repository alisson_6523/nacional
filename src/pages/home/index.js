import React from 'react';

import ListHotel from '../../components/listHotel'
import BannerHome from '../../components/bannerHome'
import OfertaDoDia from '../../components/OfertaDoDia'
import PrincipaisDestino from '../../components/PrincipaisDestinos'
import BannerPromo from '../../components/BannerPromo'
import Pacotes from '../../components/Pacotes'
import Promocao from '../../components/Promocao'
import ExcelenciaEvento from '../../components/ExcelenciaEvento'
import Blog from '../../components/Blog'


export default function Home() {

	return (
		<>
			<ListHotel />
			<BannerHome />
			<OfertaDoDia />
			<PrincipaisDestino />
			<BannerPromo />
			<Pacotes />
			<Promocao />
			<BannerPromo />
			<ExcelenciaEvento />
			{/* <Blog /> */}
		</>
	);
}
