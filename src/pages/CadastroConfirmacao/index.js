import React from 'react';

import Confirmacao from '../../components/CadastroConfirmacao'
import Faq from '../../components/Faq'

export default function CadastroConfirmacao() {
	return (
		<>
			<Confirmacao />
			<Faq />
		</>
	);
}
