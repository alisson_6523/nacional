import React from 'react';

// import { Container } from './styles';
import BannerImg from '../../assets/BannerQuemSomos/banner-01.png'

import BannerQuemSomo from '../../components/BannerQuemSomos'
import ContainerQuemSomos from '../../components/QuemSomos'
import Banner from '../../components/BannerPromo'
import Destino from '../../components/Destino'
import CategoriaHotel from '../../components/CategoriaHotel'
import Referencia from '../../components/Referencia'

export default function QuemSomos() {
	const content = {
		title: "60 hotéis em",
		subTitle: "25 destinos em todo o Brasil!",
		img: BannerImg,
	}
	return (
		<>
			<BannerQuemSomo content={content}/>
			<ContainerQuemSomos />
			<Banner styledDefault="quem-somos" />
			<Destino />
			<CategoriaHotel />
			<Referencia />
		</>
	);
}
