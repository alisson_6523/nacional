import styled from 'styled-components';

export const ContainerOfertaDia = styled.section`
	margin-top: 100px;
	margin-bottom: 100px;
	.container{
		.container-title{
			position: relative;
			h2{
				font-family: ${(props) => props.theme.fonts.poppins};
				color: ${(props) => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;
				margin-bottom: 12px;
			}

			span{
				font-family: ${(props) => props.theme.fonts.poppins};
				color: ${(props) => props.theme.colors.gray3};
				font-style: normal;
				font-weight: normal;
				font-size: 16px;
				line-height: 100%;
				display: block;
				margin-bottom: 42px;
			}
		}

		.container-oferta{
			overflow: hidden;
			position: relative;
			height: 453px;
		}
	}
`;
