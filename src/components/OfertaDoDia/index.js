import React, { useEffect, useRef } from 'react';
import Swiper from 'swiper';
import { Link } from 'react-router-dom'

import { ContainerOfertaDia } from './styles';
import CardOfertaDia from '../../components/CardOfertaDia'
import Controles from '../../components/btns/Controles'
import { swiperOfertaSlide } from '../../styles/plugins/swiper'

export default function OfertaDoDia() {
	const swiperOferta = useRef(null)
	useEffect(() => {
		new Swiper(swiperOferta.current, swiperOfertaSlide);
	}, [])

	return (
		<ContainerOfertaDia>
			<div className="container">
				<div className="container-title">
					<h2>Aproveite nossas ofertas para hoje</h2>
					<span>Aproveite as ofertas nos destinos mais procurado do Brasil.</span>

					<Controles esq='seta-esq-oferta-especial' dir="seta-dir-oferta-especial" />

				</div>

				<div className="container-oferta" ref={swiperOferta}>
					<div className="swiper-wrapper">
						<div className="swiper-slide">
							<Link to="/detalhe"><CardOfertaDia /></Link>
						</div>
						<div className="swiper-slide">
							<CardOfertaDia />
						</div>
						<div className="swiper-slide">
							<CardOfertaDia />
						</div>
						<div className="swiper-slide">
							<CardOfertaDia />
						</div>
						<div className="swiper-slide">
							<CardOfertaDia />
						</div>
					</div>
				</div>
			</div>
		</ContainerOfertaDia>
	);
}