import styled from 'styled-components';

export const ContainerExcelenciaEvento = styled.section`
	margin-bottom: 65px;
	margin-top: 19px;
	.container{
		display: flex;
		justify-content: space-between;
		h3{
			color: ${props => props.theme.colors.gray1};
			font-style: normal;
			font-weight: bold;
			font-size: 22px;
			line-height: 132%;
		}
		
		.container-viagem{
			font-family: ${props => props.theme.fonts.poppins};
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			text-align: center;
			border: 1px solid ${props => props.theme.colors.gray6};
			width: 277px;
			height: 311px;
			img{
				width: 176px;
				height: 154px;
			}	

			h3{
				margin-top: 32px;
				margin-bottom: 16px;
			}	

			span{
				color: ${props => props.theme.colors.gray3};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 100%;	
			}
		}

		.container-excelencia{
			display: flex;
			flex-direction: column;
			align-items: flex-start;
			padding-left: 54px;
			width: 592px;
			border: 1px solid ${props => props.theme.colors.gray6};
			position: relative;
			h3{
				font-family: ${props => props.theme.fonts.poppins};
				width: 185px;
				margin-top: 63px;
				margin-bottom: 16px;
				font-weight: bold;
			}

			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.gray3};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 150%;
				width: 246px;		
			}

			img{
				position: absolute;
				top: 0px;
				right: 0px;
				z-index: -1;
			}
		}
	}
`;
