import React from 'react';

import { ContainerExcelenciaEvento } from './styles';
import mala from '../../assets/evento/evento-01.jpg';
import casal from '../../assets/evento/evento-02.jpg';

export default function ExcelenciaEvento() {
	return (
		<ContainerExcelenciaEvento>
			<div className="container">
				<div className="container-viagem">
					<img src={mala} alt="" />
					<h3>Viagem em grupo</h3>
					<span>Condições speciais!</span>
				</div>

				<div className="container-viagem">
					<img src={mala} alt="" />
					<h3>Viagem em grupo</h3>
					<span>Condições speciais!</span>
				</div>

				<div className="container-excelencia">
					<h3>Excelência para o seu evento!</h3>
					<p>A Nacional Inn Hotéis dispõe de amplos espaços para a promoção de eventos em diversos destinos brasileiros. </p>
					<img src={casal} alt="" />
				</div>
			</div>
		</ContainerExcelenciaEvento>
	);
}
