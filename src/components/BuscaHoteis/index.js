import React from 'react';

import Busca from '../../components/Busca'
import { ContainerHotel } from './styles'


export default function TodosHoteis() {
	return (
		<ContainerHotel>
			<div className="container">
				<Busca />
			</div>
		</ContainerHotel>
	);
}
