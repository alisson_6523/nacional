import styled from 'styled-components';

export const ContainerHotel = styled.section`
	border-top: 1px solid rgba(89,92,118,0.3);
	border-bottom: 1px solid rgba(89,92,118,0.3);
	.container{
		border-right: 1px solid rgba(89,92,118,0.3);
	}
`;
