import styled from 'styled-components';

export const ContainerPromocao = styled.section`
	margin-bottom: 100px;
	.container{
		.container-cards{
			display: flex;
			justify-content: space-between;
		}
	}
`;
