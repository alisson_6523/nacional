import React from 'react';

import { ContainerPromocao } from './styles'
import Title from '../../components/Titles'
import CardOfertaDia from '../../components/CardOfertaDia'

export default function Promocao({title = {}}) {
	let titleDefault = title.title ? title.title : "Aproveite nossas ofertas e promoções"
	let subTitle = title.subTitle ? title.subTitle : "Temos ofertas e condições imperdiveis para  voce e sua família."
	return (
		<ContainerPromocao>
			<div className="container">
				<Title title={titleDefault} subTitle={subTitle} />


				<div className="container-cards">
					<CardOfertaDia />
					<CardOfertaDia />
					<CardOfertaDia />
					<CardOfertaDia />
				</div>
			</div>
		</ContainerPromocao>
	);
}
