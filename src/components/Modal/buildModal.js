import React from "react";

import { Container } from "./styles";

export default function BuildModais({ component: Component, ...props }) {
	const { active, func } = props;

	return (
		<Container className={`${active.active ? "active" : ""}`}>
			<div
				className="body-modal"
				onClick={(event) => func(event, Component.name)}
			>
				<Component {...props} />
			</div>
		</Container>
	);
}
