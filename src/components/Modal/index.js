import React from "react";
import { useSelector, useDispatch } from "react-redux";

import BuildModal from "./buildModal";
import Orcamento from "./orcamento";
import SucessoOrcameno from "./sucessoOrcameno";
import { toggleMenu } from "../../util/helper";
import { Creators } from "../../store/ducks/microinteracoes";

export default function Modal() {
	const activeOrcamento = useSelector(
		(state) => state.microinteracoes.modal_orcamento.active
	);

	const activeModalSucesso = useSelector(
		(state) => state.microinteracoes.modal_sucesso_orcamento.active
	);

	const dispatch = useDispatch();

	function modal(event, nameAction) {
		if (event) {
			event.target.classList.value.includes("body-modal") &&
				toggleMenu((active) => dispatch(Creators[nameAction](active)));
		} else {
			toggleMenu((active) => dispatch(Creators[nameAction](active)));
		}
	}

	return (
		<>
			<BuildModal
				component={Orcamento}
				active={activeOrcamento}
				func={modal}
			/>
			<BuildModal
				component={SucessoOrcameno}
				active={activeModalSucesso}
				func={modal}
			/>
		</>
	);
}
