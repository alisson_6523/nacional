import styled from "styled-components";
import { Link } from "react-router-dom";
import LogoTop from "../../assets/menu/menu-02.png";
import { Container as select } from "../../components/Select/styles";

export const Container = styled.div`
	position: fixed;
	top: 0px;
	left: 0px;
	width: 100vw;
	height: 100vh;
	background-color: rgba(0, 0, 0, 0);
	z-index: -1;
	pointer-events: none;
	transition: all 0.5s;
	&.active {
		background-color: rgba(0, 0, 0, 0.5);
		z-index: 99;
		pointer-events: all;
		transition: all 0.5s;
		.body-modal {
			top: 0px;
			left: 0px;
			transition: all 0.5s;
		}
	}
	.body-modal {
		height: 100%;
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
		position: absolute;
		top: -100vw;
		left: 0px;
		transition: all 0.5s;
	}
`;

export const Orcamento = styled.div`
	height: 524px;
	width: 527px;
	background-color: #fff;
	transition: height 0.5s;
	&.active {
		height: 80%;
		overflow-y: scroll;
		transition: height 0.5s;
	}
	.container-orcamento {
		padding-top: 68px;
		padding-left: 79px;
		padding-right: 79px;
		padding-bottom: 76px;
		background-image: url(${LogoTop});
		background-position: top right;
		background-repeat: no-repeat;
		background-size: contain;
		.contato {
			display: flex;
			align-items: center;
			margin-bottom: 15px;

			img {
				display: block;
				margin-right: 16px;
			}
			h4 {
				font-family: ${(props) => props.theme.fonts.inter};
				color: ${(props) => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;
			}
		}

		h2 {
			font-family: ${(props) => props.theme.fonts.poppins};
			color: ${(props) => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;
			margin-bottom: 13px;
			margin-top: 54px;
		}

		p {
			font-family: ${(props) => props.theme.fonts.inter};
			color: rgba(89, 92, 118, 0.5);
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;
			margin-bottom: 45px;
		}

		h3 {
			font-family: ${(props) => props.theme.fonts.poppins};
			color: ${(props) => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 16px;
			line-height: 100%;
			margin-bottom: 8px;
		}

		${select} {
			width: 336px;
			height: 39px;
			border: 1px solid #dcdcdc;
			border-radius: 4px;
			background: #fbfbfb;
			margin-bottom: 23px;
			.react-select__control {
				width: 100%;
				height: 100%;
				min-height: unset;
				.react-select__value-container {
					height: 37px;
					background: #fbfbfb;
					border-radius: 4px;
				}
			}

			.react-select__menu {
				width: 100.8%;
				margin-top: -1px;
				margin-left: -1px;
				background: #fbfbfb;
				border-radius: 0px 0px 4px 4px;
				box-shadow: none;
				border: 1px solid #dcdcdc;
				border-top: none;
			}

			.react-select__option {
				&:hover {
					background: #2d9cdb;
					color: #fff;
				}
			}

			.react-select__option {
				font-family: ${(props) => props.theme.fonts.inter};
				color: rgba(89, 92, 118, 0.5);
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				width: 100%;
			}

			.react-select__single-value {
				font-family: ${(props) => props.theme.fonts.inter};
				color: rgba(89, 92, 118, 0.5);
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
			}

			.react-select__placeholder {
				margin-left: 55px;
				font-family: ${(props) => props.theme.fonts.inter};
				color: rgba(89, 92, 118, 0.5);
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				&:after {
					display: none;
				}
			}
		}
	}
`;

export const ContainerSucessoOrcamento = styled.div`
	width: 494px;
	height: 527px;
	background-color: #fff;
	background-image: url(${LogoTop});
	background-position: top right;
	background-repeat: no-repeat;
	background-size: contain;
	padding: 0px 62px 0px 79px;
	display: flex;
	align-items: center;
	.container-text {
		margin-top: 30px;
		h2 {
			font-family: ${(props) => props.theme.fonts.poppins};
			color: ${(props) => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;
			margin-bottom: 14px;
		}

		p {
			font-family: ${(props) => props.theme.fonts.poppins};
			color: ${(props) => props.theme.colors.preto2};
			font-style: normal;
			font-weight: 500;
			font-size: 16px;
			line-height: 150%;
			margin-bottom: 56px;
		}

		a {
			background: #409a3c;
			font-family: ${(props) => props.theme.fonts.poppins};
			color: #fff;
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			width: 336px;
			height: 57px;
			display: flex;
			align-items: center;
			justify-content: center;
		}
	}
`;
