import React, { useState } from "react";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { FormInitialState, validadtionOrcamento } from "../../util/configForm";

import { Orcamento } from "./styles";
import whatsapp from "../../assets/header/header-02.svg";
import tel from "../../assets/header/header-03.svg";
import Select from "../../components/Select";
import FormOrcamento from "../../components/Forms/orcamento";
import { Creators } from "../../store/ducks/microinteracoes";

export default function ModalOrcamento() {
	const dispatch = useDispatch();
	const [change, setChanger] = useState({});

	const options = [
		{ value: "1", label: "Pessoa Física" },
		{ value: "2", label: "Grupo" },
		{ value: "3", label: "Evento" },
	];

	function renderForm() {
		return (
			<Formik
				enableReinitialize
				initialValues={FormInitialState.orcamento}
				onSubmit={(value, actions) => {
					console.log(value);
					dispatch(Creators.ModalOrcamento(false));
					dispatch(Creators.SucessoOrcamento(true));
					// console.log(actions);
				}}
				render={(props) => <FormOrcamento {...props} />}
				validationSchema={validadtionOrcamento}
			/>
		);
	}

	return (
		<Orcamento className={`${change.value ? "active scroll-y" : ""}`}>
			<div className="container-orcamento">
				<div className="contato">
					<img src={tel} alt="" />
					<h4>(35) 99756.7341</h4>
				</div>
				<div className="contato">
					<img src={whatsapp} alt="" />
					<h4>(11) 3292.9087</h4>
				</div>

				<h2>Orçamento</h2>
				<p>Utilize o formulário abaixo:</p>

				<h3>Selecione o tipo de orçamento</h3>

				<Select
					change={setChanger}
					options={options}
					label="Selecione o tipo de orçamento"
				/>

				{change.value && renderForm()}
			</div>
		</Orcamento>
	);
}
