import React from "react";
import { Link } from "react-router-dom";
import { ContainerSucessoOrcamento } from "./styles";

export default function SucessoOrcamento(props) {
	const { func } = props;

	return (
		<ContainerSucessoOrcamento>
			<div className="container-text">
				<h2>Sucesso</h2>

				<p>
					Seu orçamento foi enviada com sucesso! Logo entraremos em
					contato com você.
				</p>

				<Link onClick={() => func(false, "SucessoOrcamento")} to="/">
					Voltar a Home
				</Link>
			</div>
		</ContainerSucessoOrcamento>
	);
}
