import styled from 'styled-components';

export const ContainerQuemSomos = styled.section`
	padding-top: 124px;
	padding-bottom: 80px;
	.container{
		display: flex;
		justify-content: center;
		.container-img{
			display: grid;
			grid-template-columns: repeat(2, 1fr);
			width: 482px;
			grid-gap: 3.82px;
			margin-right: 103px;
			img{
				width: 241px;
				object-fit: cover;	
				border-radius: 4px;
			}
		}

		.container-text{
			width: 416px;
			margin-top: 29px;
			h3{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;	
				margin-bottom: 16px;
			}

			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 150%;
			}
		}
	}
`;
