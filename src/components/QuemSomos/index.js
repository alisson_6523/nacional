import React from 'react'

import galeria from '../../assets/quemSomos/somos-01.png'
import { ContainerQuemSomos } from './styles'

export default function QuemSomos() {
	return (
		<ContainerQuemSomos>
			<div className="container">
				<div className="container-img">
					<img src={galeria} alt=""/>
					<img src={galeria} alt=""/>
					<img src={galeria} alt=""/>
					<img src={galeria} alt=""/>
				</div>

				<div className="container-text">
					<h3>Quem Somos</h3>

					<p>Há mais de 45 anos no mercado, a Nacional Inn Hotéis é referência no setor de hotelaria. Presente em sete estados e 23 destinos das regiões Sudeste, Sul  e Nordeste, a empresa está em expansão no mercado nacional.</p>
					<br />
					<p>A Nacional Inn marca presença em importantes capitais como São Paulo, Rio de Janeiro, Belo Horizonte, Curitiba, Salvador, Porto Alegre e Recife.</p>
					<br />
					<p>As unidades se destacam por oferecer a melhor relação custo x benefício, localização estratégica e estrutura completa para lazer, negócios e eventos.</p>
				</div>
			</div>
		</ContainerQuemSomos>
	);
}
