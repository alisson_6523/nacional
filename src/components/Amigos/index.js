import React from 'react';

import { ContainerAmigos } from './styles';
import banner from '../../assets/grupo/grupo-08.png'
import cardBanner from '../../assets/grupo/grupo-09.png'
import cardBanner1 from '../../assets/grupo/grupo-10.png'
import cardBanner2 from '../../assets/grupo/grupo-11.png'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'
import { Link } from 'react-router-dom'

export default function Amigos() {
	return (
		<ContainerAmigos>
			<div className="container-img-banner">
				<img src={banner} alt=""/>
			</div>

			<div className="container">
				<div className="container-cards">

					<div className="container-text-button">
						<div className="container-text">
							<h2>Junte seus amigos </h2>
							<h3>e graranta grandes momentos.</h3>
						</div>

						<ButtonDefault>
							<Link to="/">Faça sua reserva!</Link>
						</ButtonDefault>
					</div>


					<div className="container-img">
						<img src={cardBanner} alt=""/>
						<p>Novas Experiências</p>
					</div>

					<div className="container-img">
						<img src={cardBanner1} alt=""/>
						<p>Aventuras</p>
					</div>

					<div className="container-img">
						<img src={cardBanner2} alt=""/>
						<p>Memórias <br /> Inesquecíveis</p>
					</div>


					<div className="msg">
						<p>Tudo isso com acomodações confortáveis, localizações estratégicas e ótima relação <br />
						custo x benefício! Conte com a gente!</p>	
					</div>
				</div>
			</div>
		</ContainerAmigos>
	);
}
