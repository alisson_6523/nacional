import styled from 'styled-components';

export const ContainerAmigos = styled.section`
	padding-bottom: 219px;
	.container-img-banner{
		height: 440px;
		img{
			height: 100%;
			width: 100%;
			object-fit: cover;
		}
	}

	.container{
		display: flex;
		justify-content: center;
		margin-top: -185px;
		.container-cards{
			width: 906px;
			display: grid;
    		grid-template-columns: repeat(3, 280px);
			grid-gap: 16px 0px;
			justify-content: space-between;
			.container-text-button{
				display: flex;
				align-items: center;
				justify-content: space-between;
				grid-column: span 3;
				.container-text{
					width: 449px;
					h2{
						font-family: ${props => props.theme.fonts.poppins};
						color: ${props => props.theme.colors.gray6};
						font-style: normal;
						font-weight: 600;
						font-size: 36px;
							
					}
					
					h3{
						font-family: ${props => props.theme.fonts.poppins};
						color: ${props => props.theme.colors.gray6};
						font-style: normal;
						font-weight: 600;
						font-size: 28px;
						
					}
				}
			}
			.container-img{
				width: 100%;
				position: relative;
				border-radius: 8px;
				&:before{
					content: '';
					display: block;
					width: 100%;
					height: 100%;
					background: linear-gradient(180deg, rgba(138, 197, 55, 0) 0%,  #409A3C 100%);
					position: absolute;
					top: 0px;
					left: 0px;
					border-radius: 8px;
				}

				p{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.gray6};
					font-style: normal;
					font-weight: 600;
					font-size: 18px;
					line-height: 100%;	
					position: absolute;
					bottom: 40px;
					left: 34px;
				}
				img{
					width: 100%;
					border-radius: 8px;
				}
			}	

			.msg{
				grid-column: span 3;
				margin-top: 56px;
				p{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.preto2};
					font-style: normal;
					font-weight: normal;
					font-size: 16px;
					line-height: 150%;	
				}
			}
		}
	}	
`;
