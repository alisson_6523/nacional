import React from 'react';

import { ContainerPacotes } from './styles'
import Title from '../../components/Titles'
import CardOfertaDia from '../../components/CardOfertaDia'

export default function Pacotes() {
	return (
		<ContainerPacotes>
			<div className="container">
				<Title />

				<div className="container-cards">
					<CardOfertaDia tipo="pacote" />
					<CardOfertaDia tipo="pacote" />
					<CardOfertaDia tipo="pacote" />
					<CardOfertaDia tipo="pacote" />
				</div>
			</div>
		</ContainerPacotes>
	);
}
