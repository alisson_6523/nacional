import styled from 'styled-components';

export const ContainerPacotes = styled.section`
	margin-bottom: 90px;
	.container{
		.container-cards{
			display: flex;
			justify-content: space-between;
		}
	}
`;
