import React from 'react';

import { ContainerQuemSomos } from './styles';


export default function BannerQuemSomos({content}) {
	
	const { title, subTitle, img, style = false } = content
	return (
		<ContainerQuemSomos>
			<div className="container-img">
				<img src={img} alt=""/>
			</div>

			<div className="container">
				<h1 className={`${style ? style : ''}`} >{title}</h1>
				<h2 className={`${style ? style : ''}`} >{subTitle}</h2>	
			</div>
		</ContainerQuemSomos>
	);
}
