import styled from 'styled-components';

export const ContainerQuemSomos = styled.section`
	position: relative;
	.container-img{
		height: 480px;
		img{
			width: 100%;
			height: 100%;
			object-fit: cover;
		}
	}	
	.container{
		position: absolute;
		top: 0px;
		left: 50%;
		transform: translateX(-50%);
		z-index: 20;
		height: 100%;
		display: flex;
		justify-content: center;
		flex-direction: column;	
		h1,h2{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.color01};
			font-style: normal;
			font-weight: 600;
			font-size: 50px;
			line-height: 100%;
			width: 386px;	
			&.right{
				margin-left: auto;
				margin-right: 82.71px;
			}
		}
		h2{
			color: ${props => props.theme.colors.color06};
		}
	}
`;
