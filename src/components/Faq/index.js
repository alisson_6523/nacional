import React, { useRef, useState } from 'react';
import { ContainerFaq } from './styles';

export default function Faq() {
	
	const refItens = useRef([])
	const [arrText, setArrText] = useState(itens)
	function clearHeight(itens){
		itens.forEach(function(item){
			item.parentElement.style.height = '35px'
		})
	}

	function handleOpen(key, active = false){

		clearHeight(refItens.current)

		let el = refItens.current[key]
		let altura = el.offsetHeight

		if(el.parentElement.classList.value.includes('active')){
			el.parentElement.style.height = 35 + 'px'
		}else{
			el.parentElement.style.height = (altura + 35 + 30) + 'px'
		}
		
		let novoText = arrText.map(function(item){
			item.active = false
			return item
		})

		novoText[key].active = !active

		
		setArrText(novoText)
	}

	return (
		<ContainerFaq>
			<div className="container">
				<div className="container-faq">
					<h3>faq</h3>

					<p>Perguntas frequentes</p>

					<span>Respostas rápidas para as perguntas mais frequentes de nossos hóspedes</span>
				</div>

				<div className="container-dir">
					{arrText.map((item,key) => {
						return(
							<div className={`container-itens ${item.active ? 'active': ''}`} key={key} onClick={() => handleOpen(key,item.active)}>
								<div className="container-header">
									<p>{item.title}</p>
								</div>

								<div className="container-content" ref={el => refItens.current[key] = el}>
									<br />
									{item.text.map((text, key) =>{
										return(
											<div className="container-text" key={key}>
												<h4>{text.subTitle}</h4>
												<br />

												<span>{text.text}</span>
											</div>
										)

									})}
								</div>
							</div>
						)
					})}
					
					
				</div>
			</div>
		</ContainerFaq>
	);
}



const itens = [
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
	{
		title: 'Detalhes da reserva',
		active: false,
		text: [
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},
			{
				subTitle: 'Posso cancelar minha reserva?',
				text: 'simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.'
			},

		]
	},
]