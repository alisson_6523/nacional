import styled from 'styled-components';
import setaBaixo from '../../assets/faq/faq-01.svg'

export const ContainerFaq = styled.div`
	margin-bottom: 132px;
	.container{
		font-family: ${props => props.theme.fonts.poppins};
		display: flex;
		justify-content: space-between;
		.container-faq{
			width: 250px;
			h3{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 40px;
				line-height: 100%;	
			}

			p{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 20px;
				line-height: 100%;	
				margin-top: 22px;
				margin-bottom: 29px;
			}

			span{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: normal;
				font-size: 18px;
				line-height: 160%;
			}
		}


		.container-dir{
			width: 799px;
			padding-left: 105px;
			border-left: 1px solid #E0E0E0;
			.container-itens{
				border-bottom: 1px solid #F2F2F2;
				height: 40px;
				overflow: hidden;
				transition: height .5s;
				margin-bottom: 20px;
				cursor: pointer;
				&:first-child{
					margin-top: 0px;
				}
				&.active{
					transition: height .5s;
					.container-header{
						&:before{
							transform: translateY(-50%) rotateX(-180deg);
							transition: transform .5s;
						}
						p{
							color: ${props => props.theme.colors.verde1};	
						}	
					}
					.container-content{
						.container-text{
							&:last-child{
								padding-bottom: 20px;
							}
						}	
					}
				}
				.container-content{
					.container-text{
						margin-bottom: 30px;
					}
				}
				.container-header{
					position: relative;
					&:before{
						content: '';
						display: block;
						width: 12px;
						height: 7px;
						position: absolute;
						top: 50%;
						right: 0px;
						transform: translateY(-50%);
						-webkit-mask: url(${setaBaixo});
						background-color: #8AC537;
						transition: transform .5s;
					}
					p{
						color: ${props => props.theme.colors.preto2};
						font-family: ${props => props.theme.fonts.inter};
						font-style: normal;
						font-weight: normal;
						font-size: 16px;
						line-height: 220%;	
					}
				}
			}
		}
	}
`;