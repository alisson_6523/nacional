import styled from 'styled-components';
import estrela from '../../assets/minhaLista/lista-01.svg'

export const Container = styled.div`
	display: flex;
	border: 1px solid #E0E0E0;
	width: 832px;
	margin-bottom: 19px;
	padding: 33px;
	&:first-child{
		margin-top: 37px;
	}
	
	&:last-child{
		margin-bottom: 171px;
	}

	span{
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto2};
		font-style: normal;
		font-weight: normal;
		font-size: 13px;
		line-height: 130%;
	}

	h2{
		font-family: ${props => props.theme.fonts.poppins};
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: bold;
		font-size: 20px;
		line-height: 100%;
		margin-bottom: 8px;
	}

	.container-text{
		margin-top: 17px;
		margin-left: 24px;
	}

	.container-img{
		img{
			width: 226px;
			height: 145px;
			object-fit: cover;
			border-radius: 4px;
		}	
	}

	.qualificacao{
		display: flex;
		margin-top: 15px;
		.pontuacao{
			width: 32px;
			height: 32px;
			border-radius: 50%;
			background-color: ${props => props.theme.colors.verde1};
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.gray6};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;	
		}

		.estrelas{
			margin-left: 77px;
			.estrela{
				position: relative;
				width: 50px;
				height: 50px;
				img{
					width: 100%;
					height: 100%;
					object-fit: contain
				}
				/* &:before{
					content: '';
					display: block;
					width: 100%;
					height: 100%;
					position: absolute;
					top: 50%;
					
					transform: translateY(-50%);
					-webkit-mask: url(${estrela});
					-webkit-mask-size: cover;
					background-color: #C4C4C4;
				}	 */
			}
		}
	}

`;
