import React from 'react';

import { Container } from './styles';
import hotel from '../../assets/minhaLista/lista.png'
import estrela from '../../assets/minhaLista/teste.svg'
import ListItens from '../../components/CardReservas/listaItens'

function CardMinhaLista() {
	return (
		<Container>
			<div className="container-img">
				<img src={hotel} alt="" />
			</div>

			<div className="container-text">
				<h2>Hotel Nacional Inn Poços de Caldas</h2>

				<span>Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil</span>

				<ListItens />

				<div className="qualificacao">
					<div className="pontuacao">
						4.2
					</div>

					<div className="estrelas">

					</div>
				</div>
			</div>
		</Container>
	);
}

export default CardMinhaLista;