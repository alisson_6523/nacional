import styled from 'styled-components';
import ContentLoader from 'react-content-loader'

export const Container = styled.div`
	padding: 33px 36px 33px 48px;
	border: 1px solid #E0E0E0;
	width: 832px;
	margin-bottom: 64px;
	display: flex;
	position: relative;
	&:last-child{
		margin-bottom: 0px;
	}

	.container-reserva-finalizada, .container-reserva-cancelada{
		position: absolute;
		top: -2px;
		right: -2px;
	}		

	span{
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto2};
		font-style: normal;
		font-weight: normal;
		font-size: 13px;
		line-height: 130%;
	}
	.container-esq{
		display: flex;
    	flex-direction: column;
		.container-img{
			margin-bottom: 41px;
			img{
				border-radius: 4px;
				width: 226px;
				height: 145px;
				object-fit: cover;
			}
		}

		.cancelamento{
			p{
				margin-top: 8px;
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.red};
				font-style: normal;
				font-weight: 600;
				font-size: 13px;
				line-height: 110%;
			}	
		}

		span{
			display: block;
			width: 202px;
			margin-bottom: auto;
		}
	}

	.container-dir{
		margin-left: 24px;
		h2{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: bold;
			font-size: 20px;
			line-height: 100%;
			margin-bottom: 8px;
		}

		span{
			display: block;
			margin-bottom: 16px;
		}

		.container-infs{
			ul{
				display: flex;
				li{
					margin-right: 40px;
					&.active{
						span{
							color: ${props => props.theme.colors.verde1};
							font-weight: 600;
						}
					}
					span{
						display: flex;
						align-items: center;
						margin-bottom: 0px;
						img{
							display: block;
							margin-right: 7px;
						}
					}
					&:last-child{
						margin-right: 0px;
					}
				}
			}
		}

		.container-inf-hotel{
			margin-top: 24px;
			margin-bottom: 24px;
			ul{
				height: 48px;
				border-top: 1px solid #E0E0E0;
				border-bottom: 1px solid #E0E0E0;
				display: flex;
				align-items: center;
				padding-left: 12px;
				li{
					margin-right: 55px;
					display: flex;
					align-items: center;
					&:last-child{
						margin-right: 0;
					}
					p{
						font-family: ${props => props.theme.fonts.poppins};
						color: ${props => props.theme.colors.preto1};
						font-style: normal;
						font-weight: 500;
						font-size: 14px;
						line-height: 100%;
						margin-left: 8px;
					}
				}
			}
		}

		.container-entrada-saida{
			display: grid;
	    	grid-template-columns: repeat(2, 1fr);
			padding-top: 16px;
			padding-bottom: 24px;
			position: relative;
			border-bottom: 1px solid #E0E0E0;
			&:before{
				content: '';
				display: block;
				width: 295px;
				height: 1px;
				background-color: #595C76;
				opacity: 0.3;
				position: absolute;
				top: 0px;
				left: 0px;
			}
			.container-entrada, .container-saida{
				h4{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.verde1};
					font-style: normal;
					font-weight: 600;
					font-size: 14px;
					line-height: 100%;
					margin-bottom: 12px;
					position: relative;
					&:before{
						content: '';
						display: block;
						width: 4px;
						height: 4px;
						border-radius: 50%;
						position: absolute;
						top: -16px;
						left: -1px;
    					transform: translateY(-1.5px);
						background-color: ${props => props.theme.colors.verde1};
					}
				}

				p{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.preto1};
					font-style: normal;
					font-weight: 600;
					font-size: 16px;
					line-height: 100%;
					margin-bottom: 8px;
				}
				span{
					margin-bottom: 0;
				}
			}
			.container-saida{
				margin-left: 72px;
			}
		}

		.valor-total{
			margin-top: 23.5px;
			display: flex;
			align-items: center;
			h3{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: bold;
				font-size: 16px;
				line-height: 100%;
				margin-right: 115px;
			}
			a{
				margin-top: 0px;
			}
		}

	}
`;

export const ListInfs = styled(Container)`
	margin: 0px;
	padding: 0px;
	border: none;
	margin-top: 16px;
	width: 100%;
	.container-infs{
		ul{
			display: flex;
			li{
				margin-right: 40px;
				&.active{
					span{
						color: ${props => props.theme.colors.verde1};
						font-weight: 600;
					}
				}
				span{
					display: flex;
					align-items: center;
					margin-bottom: 0px;
					img{
						display: block;
						margin-right: 7px;
					}
				}
				&:last-child{
					margin-right: 0px;
				}
			}
		}
	}
`