import React from 'react';

import { ListInfs } from './styles';
import check from '../../assets/CardReserva/reserva-02.svg'

function CardReservas() {
	return (
		<ListInfs>
			<div className="container-infs">
				<ul>
					<li>
						<span>
							<img src={check} alt="" />
						Resort
					</span>
					</li>
					<li>
						<span>
							<img src={check} alt="" />
						0,4 Km até centro da cidade
					</span>
					</li>
					<li className="active">
						<span>
							<img src={check} alt="" />
						All Inclusive
					</span>
					</li>
				</ul>
			</div>
		</ListInfs>
	);
}

export default CardReservas;