import React from 'react';

import { Container } from './styles';
import BtnLink from '../btns/BtnLink'
import imgHotel from '../../assets/CardReserva/reserva-01.png'
import check from '../../assets/CardReserva/reserva-02.svg'
import quartos from '../../assets/CardReserva/reserva-03.svg'
import checkReserva from '../../assets/CardReserva/reserva-04.svg'
import calendario from '../../assets/cadastro/cadastro-09.svg'
import pessoa from '../../assets/cadastro/cadastro-10.svg'

export default function CardReservas() {
  return (
	<Container>
		<div className="container-reserva-finalizada">
			<img src={checkReserva} alt=""/>
		</div>

		<div className="container-esq">
			<div className="container-img">
				<img src={imgHotel} alt=""/>
			</div>

			<span>N. de Confirmação: 2401725779 Código PIN: 6760</span>

			<BtnLink title="Termos e Politica do Hotel"/>
		</div>

		<div className="container-dir">
			<h2>Hotel Nacional Inn Poços de Caldas</h2>

			<span>Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil </span>

			<div className="container-infs">
				<ul>
					<li>
						<span>
							<img src={check} alt=""/>
							Resort
						</span>
					</li>
					<li>
						<span>
							<img src={check} alt=""/>
							0,4 Km até centro da cidade
						</span>
					</li>
					<li className="active">
						<span>
							<img src={check} alt=""/>
							All Inclusive
						</span>
					</li>
				</ul>
			</div>

			<div className="container-inf-hotel">
				<ul>
					<li>
						<img src={calendario} alt=""/>
						<p>01 Díaria</p>
					</li>
					<li>
						<img src={pessoa} alt=""/>
						<p>02 Pessoas</p>
					</li>
					<li>
						<img src={quartos} alt=""/>
						<p>02 Quartos</p>
					</li>
				</ul>
			</div>

			<div className="container-entrada-saida">
				<div className="container-entrada">
					<h4>Entrada</h4>

					<p>02 Outubro de 2019</p>

					<span>Quarta - Feira a partir de 16:00</span>

				</div>

				<div className="container-saida">
					<h4>Saída</h4>

					<p>02 Outubro de 2019</p>

					<span>Quinta - até  14:00</span>

				</div>
			</div>

			<div className="valor-total">
				<h3>Valor total R$: 725,00</h3>
				<BtnLink title="Mais detalhes"/>
			</div>
		</div>

	</Container>
  );
}
