import React from 'react';
import ContentLoader from 'react-content-loader'

export default function CardSkeleton() {
	return (
		<ContentLoader
			speed={2}
			width={836}
			height={395}
			viewBox="0 0 836 395"
			backgroundColor="#f3f3f3"
			foregroundColor="#ecebeb"
		>
			<rect x="0" y="3" rx="0" ry="0" width="226" height="145" />
			<rect x="267" y="5" rx="0" ry="0" width="600" height="20" />
			<rect x="267" y="33" rx="0" ry="0" width="600" height="13" />
			<rect x="267" y="57" rx="0" ry="0" width="600" height="13" />
			<rect x="267" y="86" rx="0" ry="0" width="600" height="48" />
			<rect x="268" y="159" rx="0" ry="0" width="600" height="83" />
			<rect x="0" y="182" rx="0" ry="0" width="226" height="34" />
			<rect x="269" y="275" rx="0" ry="0" width="600" height="48" />
			<rect x="0" y="281" rx="0" ry="0" width="226" height="34" />
		</ContentLoader>
	);
}
