import styled from 'styled-components';
import setaBaixo from '../../assets/header/header-07.svg'
import calendario from '../../assets/header/header-09.svg'
export const ContainerForm = styled.div`
	display: flex;
	align-items: center;
	width: 352px;
	height: 100%;
	position: relative;
	border-right: 1px solid #DCDCDC;
	border-left: 1px solid #DCDCDC;
	cursor: pointer;
	span{
		font-family: ${(props) => props.theme.fonts.inter};
		color: ${(props) => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 17px;	
		position: absolute;
		width: 167px;
		white-space: nowrap;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		&:before{
			content: '';
			display: block;
			width: 12px;
    		height: 7px;
			position: absolute;
			top: 55%;
			right: -64px;
			transform: translateY(-50%);
			-webkit-mask: url(${setaBaixo});
			background-color: #8AC537;
		}
		&:after{
			content: '';
			display: block;
			width: 15px;
    		height: 16px;
			position: absolute;
			top: 60%;
			left: -32px;
			transform: translateY(-50%);
			-webkit-mask: url(${calendario});
			background-color: #BDBDBD;
		}
	}

	input{
		width: 100%;
		height: 100%;
		opacity: 0;
		margin: 0px;
		padding: 0px;
		cursor: pointer;
	}
`;
