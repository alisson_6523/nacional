import React, { useRef, useEffect } from "react";
import Lightpick from "lightpick";
import Moment from "moment";

import { ContainerForm } from "./styles";
import setaEsq from "../../assets/calendario/calendario-01.svg";
import setaDir from "../../assets/calendario/calendario-02.svg";

export default function Calendario({ change = () => {} }) {
	const inputEl = useRef(null);
	const labelEl = useRef(null);

	var today = new Date();
	let ano = today.getFullYear();
	let mes =
		today.getMonth() + 1 < 10
			? "0" + (today.getMonth() + 1)
			: today.getMonth() + 1;
	let dia = today.getDate();

	let date = dia + "." + mes + "." + ano;

	function openCalendario() {
		inputEl.current.click();
	}

	useEffect(() => {
		new Lightpick({
			field: inputEl.current,
			singleDate: false,
			numberOfMonths: 2,
			lang: "pt-BR",
			locale: {
				tooltip: {
					one: "dia",
					few: "dias",
					many: "dias",
				},

				buttons: {
					prev: `<img class="img-lightpick" src="${setaEsq}"/>`,
					next: `<img class="img-lightpick" src="${setaDir}"/>`,
				},
				pluralize: function (i, locale) {
					if ("one" in locale && i % 10 === 1 && !(i % 100 === 11))
						return locale.one;
					if (
						"few" in locale &&
						i % 10 === Math.floor(i % 10) &&
						i % 10 >= 2 &&
						i % 10 <= 4 &&
						!(i % 100 >= 12 && i % 100 <= 14)
					)
						return locale.few;
					if (
						"many" in locale &&
						(i % 10 === 0 ||
							(i % 10 === Math.floor(i % 10) &&
								i % 10 >= 5 &&
								i % 10 <= 9) ||
							(i % 100 === Math.floor(i % 100) &&
								i % 100 >= 11 &&
								i % 100 <= 14))
					)
						return locale.many;
					if ("other" in locale) return locale.other;

					return "";
				},
			},
			minDate: Moment().add(1, "days"),

			onSelect: function (start, end) {
				var str = "";
				str += start ? start.format("DD-MM-YYYY") + " " : "";
				str += end ? end.format("DD-MM-YYYY") : " ";
				change(str);
				inputEl.current.setAttribute("data-date", str);

				if (end) {
					str = str.replace(/[-]/g, ".");
					str = str.replace(/[ ]/g, " - ");
					labelEl.current.innerText = str;
				}
			},
		});
	}, [change]);

	return (
		<ContainerForm onClick={openCalendario}>
			<span ref={labelEl}>{`${date} - ${date}`}</span>
			<input ref={inputEl} type="text" />
		</ContainerForm>
	);
}
