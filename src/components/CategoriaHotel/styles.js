import styled from 'styled-components';


export const ContainerCategoriaHotel = styled.section`
	position: relative;
	height: 862px;
	margin-bottom: 140px;
	.container-img{
		height: 445px;
		img{
			height: 100%;
			width: 100%;
			object-fit: cover;
		}	
	}
	.container{
		position: absolute;
		top: 0px;
		left: 50%;
		transform: translateX(-50%);
		height: 100%;
		margin-top: 288px;
		h2{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.gray6};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;
			margin-bottom: 30px;	
		}
		.container-cards{
			display: flex;
			justify-content: space-between;
		}
	}
`;


export const Card = styled.div`
	width: 280px;
	height: 512px;
	border-radius: 8px;
	border: 1px solid rgba(89, 92, 118, 0.2);
	.container-img-card{
		height: 222px;
		img{
			border-radius: 8px 8px 0px 0px;	
			height: 100%;
			width: 100%;
		}
	}

	.container-text{
		padding-left: 34px;
		padding-right: 39px;
		padding-top: 36px;
		background-color: #fff;
		h3{
			margin-bottom: 16px;
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 100%;
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 150%;
		}
	}

`