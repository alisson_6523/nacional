import React from 'react';

import { ContainerCategoriaHotel } from './styles'
import Card from './cardHotel'
import banner from '../../assets/quemSomos/somos-10.png'
import card1 from '../../assets/quemSomos/somos-11.png'
import card2 from '../../assets/quemSomos/somos-12.png'
import card3 from '../../assets/quemSomos/somos-13.png'
import card4 from '../../assets/quemSomos/somos-14.png'

export default function CategoriaHotel() {
	const contents = [
		{
			title: 'Resort',
			text: 'São hotéis que oferecem acomodações amplas e modernas, além da facilidade de serviços de alto padrão. Resort é sinônimo de exclusividade e excelência nos serviços.',
			img: card1
		},
		{
			title: 'Premium',
			text: 'Personalizados e de alto padrão de conforto. Cada hotel tem características individuais e atende um portfólio diversificado. As unidades possuem excelente localização nos destinos de negócio e lazer.',
			img: card2,
		},
		{
			title: 'Classic',
			text: 'São hotéis com localização estratégica, em geral nos centros urbanos ou próximos as principais rodovias e aeroportos. Cada hotel se beneficia de suas características regionais e oferece acomodações confortáveis e versáteis.',
			img: card3
		},
		{
			title: 'Budget',
			text: 'São hotéis limited service, que oferecem um excepcional custo benefício em hospedagem executiva. Proporcionam aos hóspedes localização privilegiada e acomodações funcionais, com comodidade e serviços convenientes.',
			img: card4,
		},
	]
	return (
		<ContainerCategoriaHotel>
			<div className="container-img">
				<img src={banner} alt=""/>
			</div>

			<div className="container">
				<h2>Categorias dos Hotéis</h2>
				<div className="container-cards">
					{contents.map((content, key) => <Card key={key} content={content} />)}
				</div>
			</div>
		</ContainerCategoriaHotel>
	);
}
