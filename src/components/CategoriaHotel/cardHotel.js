import React from 'react';

import { Card } from './styles';


export default function CategoriaHotel({content}) {
	const { title, text, img } = content
	return (
		<Card>
			<div className="container-img-card">
				<img src={img} alt=""/>
			</div>

			<div className="container-text">
				<h3>{title}</h3>

				<span>{text}</span>
			</div>
		</Card>	
	);
}
