import styled from 'styled-components';

export const ContainerTiposEventos = styled.section`
	margin-bottom: 118px;
	.container{
		display: flex;
		.esq{
			width: 425px;
			h2{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 120%;	
				margin-bottom: 24px;
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 47px;
					height: 2px;
					background-color: ${props => props.theme.colors.verde1};
					position: absolute;
					top: -32px;
					left: 0px;
				}
			}

			span{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};	
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 150%;
				display: block;
				margin-bottom: 24px;
			}

			ul{
				display: grid;
				grid-template-columns: repeat(2 ,1fr);
				grid-gap: 5px  0px;
				margin-bottom: 40px;
				li{
					display: flex;
					align-items: center;
					img{
						display: block;
						margin-right: 13px;
					}

					span{
						margin-bottom: 0px;
					}
				}
			}

			h3{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: 600;
				font-size: 16px;
				line-height: 150%;	
			}
		}

		.dir{
			margin-left: auto;
			.card-sala{
				margin-bottom: 32px;
				&:last-child{
					margin-bottom: 0px;
				}
			}
		}
	}
`;


export const Salas = styled.div`
	display: grid;
	grid-template-columns: repeat(2, 1fr);
	align-items: flex-end;
	padding: 26px 40px 32px 25px;
	width: 624px;
	border-radius: 4px;
	border: 1px solid #F2F2F2;
	.container-img{
		img{
			width: 176px;
			height: 123px;
			object-fit: cover;	
		}
	}

	.container-text{
		font-family: ${props => props.theme.fonts.poppins};
		margin-left: 32px;
		h3{
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			margin-bottom: 18px;	
		}

		p{
			font-style: normal;
			color: ${props => props.theme.colors.preto2};
			font-weight: normal;
			font-size: 13px;
			line-height: 130%;	
		}
	}	
`

export const Capacidade = styled.div`
	grid-column: span 2;
	padding-left: 25px;
	padding-right: 40px;
	height: 51px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	border: 1px solid #F2F2F2;
	border-radius: 4px;
	border-top: none;
	h4{
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 160%;
		position: relative;
		&:before{
			content: '';
			display: block;
			width: 47px;
			height: 2px;
			background-color: ${props => props.theme.colors.verde1};
			position: absolute;
			top: -15.8px;
			left: 0px;
		}
	}
`