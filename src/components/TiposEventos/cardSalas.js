import React from 'react';

import { Salas, Capacidade } from './styles';
import sala from '../../assets/orcamento/orcamento-03.png'
import setaDir from '../../assets/orcamento/orcamento-04.png'

export default function CardSalas() {
	return (
		<div className="card-sala">
			<Salas>
				<div className="container-img">
					<img src={sala} alt="" />
				</div>

				<div className="container-text">
					<h3>Centros de Convenções</h3>

					<p>Os nossos Centros de Convenções, localizados em 12 municípios brasileiros, foram pensados para oferecer as melhores estruturas para o seu evento! São indicados para encontros de médio e grande portes. </p>
				</div>
			</Salas>

			<Capacidade>
				<h4>Capacidade 18.500 pessoas em auditório</h4>

				<img src={setaDir} alt="" />
			</Capacidade>
		</div>
	);
}
