import React from 'react';

import { ContainerTiposEventos } from './styles';
import CardSala from './cardSalas'
import check from '../../assets/orcamento/orcamento-02.svg'

export default function TiposEventos() {
	return (
		<ContainerTiposEventos>
			<div className="container">
				<div className="esq">
					<h2>Ideal para qualquer tipo de evento</h2>

					<span>Dentro das unidades estão localizados auditórios e salas: sempre <br /> oferecendo conforto, qualidade e excelência para o seu evento! <br /></span>

					<ul>
						<li>
							<img src={check} alt="" />
							<span>Festas</span>
						</li>
						<li>
							<img src={check} alt="" />
							<span>Palestras</span>
						</li>
						<li>
							<img src={check} alt="" />
							<span>Workshops</span>
						</li>
						<li>
							<img src={check} alt="" />
							<span>Shows</span>
						</li>
						<li>
							<img src={check} alt="" />
							<span>Recepções</span>
						</li>
						<li>
							<img src={check} alt="" />
							<span>Congressos</span>
						</li>
					</ul>

					<h3>São ambientes localizados em 23 destinos e sete estados do Brasil!</h3>
				</div>

				<div className="dir">
					<CardSala />
					<CardSala />
				</div>
			</div>
		</ContainerTiposEventos>
	);
}
