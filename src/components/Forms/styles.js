import styled, { css } from "styled-components";
import { Container as CheckBox } from "../../components/CheckBox/styles";
import { ButtonDefault } from "../../components/btns/btnBusca/styles";
import user from "../../assets/cadastro/cadastro-06.svg";
import setaBaixo from "../../assets/header/header-07.svg";
import { Container as Select } from "../../components/Select/styles";
import { Container as check } from "../../components/CheckBox/styles";
import { ContainerForm as Calendario } from '../../components/calendario/styles'

export const FormDados = styled.div`
	font-family: ${(props) => props.theme.fonts.poppins};
	width: 100%;
	padding-left: 131px;
	padding-right: 129px;
	margin-top: ${(props) => (props.login ? "46px" : "69px")};
	a {
		margin-top: 42px;
		font-family: ${(props) => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 160%;
		display: flex;
		align-items: center;
		color: ${(props) => props.theme.colors.blue1};
		img {
			display: block;
			margin-left: 19px;
		}
	}
	form {
		.container-senha {
			position: relative;
			.container-form {
				width: 298px;
			}

			${check} {
				position: absolute;
				top: 15px;
				right: 0px;
				.container-checks {
					&:first-child {
						margin-top: 0;
					}
				}
				.pretty {
					margin-right: 0px;
					.state {
						label {
							margin-left: 10px;
							line-height: 0px;
							margin-bottom: 0px;
							font-family: ${(props) => props.theme.fonts.inter};
							font-weight: normal;
							font-size: 13px;
							color: #132847;
							&:before {
								top: calc((0% - (100% - 1em)) - -23%);
							}
						}
					}
				}
			}
		}

		h2 {
			font-family: ${(props) => props.theme.fonts.inter};
			color: ${(props) => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 22px;
			line-height: 160%;
			margin-bottom: 44px;
		}

		p {
			&.login {
				font-family: ${(props) => props.theme.fonts.inter};
				color: ${(props) => props.theme.colors.preto1};
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 160%;
				margin-bottom: 31px;
				text-align: center;
				cursor: pointer;
			}
		}

		h4 {
			font-family: ${(props) => props.theme.colors.inter};
			color: ${(props) => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 16px;
			line-height: 160%;
			margin-bottom: 43px;
			&.title {
				margin-bottom: 35px;
			}
		}

		.container-text {
			margin-bottom: 80px;
			p {
				font-family: ${(props) => props.theme.fonts.inter};
				color: ${(props) => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 160%;
			}
		}

		.container-checks {
			display: flex;
			justify-content: space-between;
			${CheckBox} {
				margin-bottom: 80px;
				.pretty {
					.state {
						font-family: ${(props) => props.theme.fonts.inter};
						color: ${(props) => props.theme.colors.preto1};
						font-style: normal;
						font-weight: normal;
						font-size: 14px;
					}
				}
			}
		}
	}

	.login-plataformas {
		display: flex;
		justify-content: space-between;
		.plataforma {
			width: 161px;
			height: 97px;
			background: #c4c4c4;
			border-radius: 4px;
		}
	}

	${ButtonDefault} {
		margin-top: ${(props) => (props.login ? "27px" : "38px")};
		margin-bottom: ${(props) => (props.login ? "16px" : "0px")};
		.login {
			width: 100%;
			height: 57px;
		}
	}
`;

export const FormPagamento = styled.div`
	font-family: ${(props) => props.theme.fonts.poppins};
	width: 100%;
	padding-left: 131px;
	padding-right: 129px;
	margin-top: 69px;
	a {
		margin-top: 42px;
		font-family: ${(props) => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 160%;
		display: flex;
		align-items: center;
		color: ${(props) => props.theme.colors.blue1};
		img {
			display: block;
			margin-left: 19px;
		}
	}

	span {
		font-family: ${(props) => props.theme.fonts.inter};
		color: ${(props) => props.theme.colors.preto2};
		font-style: normal;
		font-weight: normal;
		font-size: 14px;
		line-height: 160%;
	}

	h4 {
		font-family: ${(props) => props.theme.colors.inter};
		color: ${(props) => props.theme.colors.verde1};
		font-style: normal;
		font-weight: 600;
		font-size: 16px;
		line-height: 160%;
		margin-bottom: 43px;
		&.title {
			margin-bottom: 35px;
		}
	}

	form {
		padding-bottom: 57px;
		border-bottom: 1px solid #e0e0e0;
		margin-bottom: 38px;
		.container-form {
			${Select} {
				border: 1px solid rgba(89, 92, 118, 0.3);
				border-radius: 4px;
				width: 211px;
				position: relative;
				cursor: pointer;
				&:before {
					content: "";
					display: block;
					width: 12px;
					height: 7px;
					position: absolute;
					top: 50%;
					right: 18px;
					transform: translateY(-50%);
					-webkit-mask: url(${setaBaixo});
					background-color: #8ac537;
					z-index: 20;
				}
				.react-select__placeholder {
					&:after,
					&:before {
						display: none;
					}
				}
				.react-select__control {
					width: 100%;
				}
				.react-select__value-container {
					cursor: pointer;
				}

				.react-select__menu {
					width: 100%;
				}
				.react-select__option {
					font-family: ${(props) => props.theme.fonts.inter};
					color: ${(props) => props.theme.colors.preto1};
					font-style: normal;
					font-weight: bold;
					font-size: 14px;
					line-height: 160%;
					width: 100%;
				}

				.react-select__single-value {
					font-weight: bold;
				}
			}

			.container-parcelas {
				p {
					font-family: ${(props) => props.theme.fonts.inter};
					color: ${(props) => props.theme.colors.preto2};
					font-style: normal;
					font-weight: normal;
					font-size: 14px;
					line-height: 160%;
				}
			}

			&.select {
				display: flex;
				.container-parcelas {
					margin-left: 42px;
				}
			}

			&.cartao {
				width: 211px;
			}
		}

		.container-text {
			margin-bottom: 80px;
			p {
				font-family: ${(props) => props.theme.fonts.inter};
				color: ${(props) => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 160%;
			}
		}

		${ButtonDefault} {
			margin-bottom: 49px;
			button {
				height: 57px;
			}
		}

		.container-checks {
			display: flex;
			justify-content: space-between;
			${CheckBox} {
				margin-bottom: 80px;
				.pretty {
					.state {
						font-family: ${(props) => props.theme.fonts.inter};
						color: ${(props) => props.theme.colors.preto1};
						font-style: normal;
						font-weight: normal;
						font-size: 14px;
					}
				}
			}
		}
	}

	.container-infs-usuario {
		margin-bottom: 40px;
		padding-bottom: 56px;
		border-bottom: 1px solid #e0e0e0;
		h4 {
			margin-bottom: 14px;
		}
		.container-dados {
			display: flex;
			justify-content: space-between;
			ul {
				padding-left: 35px;
				position: relative;
				&:before {
					content: "";
					display: block;
					width: 18px;
					height: 18px;
					position: absolute;
					top: 5px;
					left: 0px;
					-webkit-mask: url(${user});
					background-color: #409a3c;
				}
				li {
					font-family: ${(props) => props.theme.fonts.inter};
					color: ${(props) => props.theme.colors.preto1};
					font-style: normal;
					font-weight: normal;
					font-size: 14px;
					line-height: 190%;
				}
			}

			a {
				margin-top: 0px;
				height: 20px;
				img {
					margin-right: 16px;
				}
			}
		}
	}

	.container-text {
		padding-bottom: 43px;
		margin-bottom: 56px;
		border-bottom: 1px solid #e0e0e0;
	}

	.container-compra {
		display: flex;
		align-items: flex-start;
		p {
			width: 382px;
			font-family: ${(props) => props.theme.fonts.inter};
			color: ${(props) => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 14px;
			line-height: 160%;
			margin-left: 17px;
			margin-top: -4px;
			img {
				display: block;
				margin-top: 30px;
			}
		}
	}
`;

export const FormDadosUser = styled(FormDados)`
	padding-left: 0px;
	padding-right: 0px;
	margin-top: 76px;
	width: 539px;
	form {
		h4 {
			margin-top: 108px;
			margin-bottom: 43px;
		}

		${ButtonDefault} {
			display: flex;
			justify-content: flex-end;
			margin-top: 17px;
			button {
				width: 300px;
				height: 57px;
			}
		}
	}
`;

export const FormOrcamento = styled.div`
	width: 336px;

	form {
		${ButtonDefault} {
			button {
				width: 100%;
				height: 57px;
			}
		}

		.container-form{
			${Calendario}{
				width: 100%;
				border-radius: 4px;
				border: 1px solid rgba(89,92,118,0.3);
				span{
					&:after{
						left: -30%;
					}
				}
			}
		}
	}

	.container-form{
		${Select} {
			margin-bottom: 0px !important;
			input {
				line-height: unset;
				height: auto;
			}
		}
	}
`;
