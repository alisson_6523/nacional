import React from 'react';

import { FormDadosUser } from './styles'

import { Form, Field, ErrorMessage } from 'formik'
import InputMask from "react-input-mask"
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

function FormMeusDadosUser(props) {

	const { handleChange, handleBlur } = props
	const { telefone } = props.values

	return (
		<FormDadosUser>
			<Form action="" >
				<div className="container-form">
					<label htmlFor="">Nome</label>
					<Field type="text" name="nome" />
					<ErrorMessage component="span" name="nome" />
				</div>

				<div className="container-form">
					<label htmlFor="">Email</label>
					<Field type="text" name="eMail" />
					<ErrorMessage component="span" name="eMail" />
				</div>

				<div className="container-form">
					<label htmlFor="">Telefone</label>
					<InputMask mask="(99)99999-9999" value={telefone} onChange={handleChange} type="tel" name="telefone"></InputMask>
					<ErrorMessage component="span" name="telefone" />
				</div>


				<h4>Senha de Acesso</h4>

				<div className="container-form">
					<label htmlFor="">Senha</label>
					<Field type="password" name="senha" autoComplete="new-password" />
					<ErrorMessage component="span" name="senha" />
				</div>

				<div className="container-form">
					<label htmlFor="">Confirmar Senha</label>
					<Field type="password" name="senhaConfirma" />
					<ErrorMessage component="span" name="senhaConfirma" />
				</div>


				<ButtonDefault><button>Salvar Alterações</button></ButtonDefault>
			</Form>
		</FormDadosUser>
	);
}

export default FormMeusDadosUser;