import React from "react";
import { useHistory } from "react-router-dom";
import { Form, Field, ErrorMessage } from "formik";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import { useDispatch } from "react-redux";

import { Creators as LoginCreators } from "../../store/ducks/login";
import { Creators as UserCreators } from "../../store/ducks/user";

import { FormDados } from "./styles";
import {
	ButtonDefault,
	ButtonLogin,
} from "../../components/btns/btnBusca/styles";
import google from "../../assets/login/google.svg";
import face from "../../assets/login/face.svg";

export default function Forms({ cadastrar, values, handleChange, handleBlur }) {
	const history = useHistory();
	const dispatch = useDispatch();

	const responseGoogle = (response) => {
		const {
			tokenId: token,
			profileObj: { name, email, imageUrl: picture },
		} = response;

		dispatch(LoginCreators.requestLoginGoogle(token));
		dispatch(UserCreators.setUserInfsGoogle({ name, email, picture }));
		history.push("/user");
	};

	const responseFacebook = (response) => {
		const { accessToken: token, email, name, picture: img } = response;

		const picture = img.data.url;

		dispatch(LoginCreators.requestLoginFaceBook(token));
		dispatch(UserCreators.setUserInfsFace({ email, name, picture }));
		history.push("/user");
	};

	const { email, password } = values;
	const node = process.env.NODE_ENV;

	const configFace = {
		id: node === "development" ? "275622326823688" : "715024105968771",
	};

	console.log(configFace.id);

	return (
		<FormDados login={true}>
			<Form action="">
				<h2>Já sou Cadastrado</h2>

				<div className="container-form">
					<label htmlFor="">Email</label>
					<Field
						type="text"
						name="email"
						onChange={handleChange}
						onBlur={handleBlur}
						value={email}
					/>
					<ErrorMessage component="span" name="email" />
				</div>

				<div className="container-form">
					<label htmlFor="">Senha</label>
					<Field
						type="password"
						name="password"
						autoComplete="new-password"
						onChange={handleChange}
						value={password}
					/>
					<ErrorMessage component="span" name="password" />
				</div>

				<ButtonDefault>
					<button className="login">Acessar</button>
				</ButtonDefault>

				<p className="login">Esqueci minha senha</p>
			</Form>

			<div className="login-plataformas">
				<GoogleLogin
					clientId="627563900735-qbgfqaatgvijhpn9mde8nibp7eatj0qb.apps.googleusercontent.com"
					onSuccess={responseGoogle}
					onFailure={responseGoogle}
					render={(renderProps) => (
						<ButtonLogin
							onClick={renderProps.onClick}
							disabled={renderProps.disabled}
						>
							{" "}
							<img src={google} alt="" /> Google
						</ButtonLogin>
					)}
				/>

				<FacebookLogin
					appId={configFace.id}
					fields="name,email,picture.width(640)"
					size="metro"
					callback={responseFacebook}
					render={(renderProps) => (
						<ButtonLogin onClick={renderProps.onClick}>
							{" "}
							<img src={face} alt="" /> Facebook
						</ButtonLogin>
					)}
				/>
			</div>

			<ButtonDefault onClick={() => cadastrar(false)} cadastrar={true}>
				<button className="login">Cadastre-se agora!</button>
			</ButtonDefault>
		</FormDados>
	);
}
