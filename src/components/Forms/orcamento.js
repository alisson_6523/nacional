import React, { useState } from "react";

import { Form, Field, ErrorMessage } from "formik";
import InputMask from "react-input-mask";

import { FormOrcamento } from "./styles";
import Select from "../../components/Select";
import { ButtonDefault } from "../../components/btns/btnBusca/styles";
import Calendario from "../calendario";

export default function Orcamento(props) {
	const [changeCalendario, setchangeCalendario] = useState("");
	const [changeSelect, setchangechangeSelect] = useState("");

	const { telefone, handleChange, setFieldValue } = props;

	const options = [
		{ value: "1", label: "Pessoa Física" },
		{ value: "2", label: "Grupo" },
		{ value: "3", label: "Evento" },
	];

	return (
		<FormOrcamento>
			<Form>
				<h3>Hotel</h3>

				<div className="container-form select">
					<Select options={options} change={setchangechangeSelect} label="Sem Hotel definido" />
					<Field type="hidden" name="hotel" value={changeSelect} />
					<ErrorMessage component="span" name="hotel" />
				</div>

				<div className="container-form">
					<label htmlFor="">Nome</label>
					<Field type="text" name="nome" />
					<ErrorMessage component="span" name="nome" />
				</div>

				<div className="container-form">
					<label htmlFor="">E-Mail</label>
					<Field type="text" name="eMail" />
					<ErrorMessage component="span" name="eMail" />
				</div>

				<div className="container-form">
					<label htmlFor="">Telefone</label>
					<InputMask
						mask="(99)99999-9999"
						value={telefone}
						onChange={handleChange}
						type="tel"
						name="telefone"
					></InputMask>
					<ErrorMessage component="span" name="telefone" />
				</div>

				<div className="container-form">
					<label htmlFor="">Check-in Check-out</label>
					<Calendario change={setchangeCalendario} />
					<Field type="hidden" name="CheckInCheckOut" value={changeCalendario}/>
					<ErrorMessage component="span" name="CheckInCheckOut" />
				</div>

				<div className="container-form">
					<label htmlFor="">Meu Pedido</label>
					<Field name="obersvacoes" component="textarea" />
					<ErrorMessage component="span" name="obersvacoes" />
				</div>

				<ButtonDefault
					onClick={() => {
						setFieldValue("CheckInCheckOut", changeCalendario && changeCalendario)
						setFieldValue("hotel", changeSelect)
					}}>
					<button type="submit" className="login">Enviar</button>
				</ButtonDefault>
			</Form>
		</FormOrcamento>
	);
}
