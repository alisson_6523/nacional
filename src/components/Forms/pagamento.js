import React, { useEffect, useState } from 'react';

import { Form, Field, ErrorMessage } from 'formik'
import { Link } from 'react-router-dom'
import InputMask from "react-input-mask"

import { ButtonDefault } from '../../components/btns/btnBusca/styles'
import Select from '../../components/Select';
import setaDir from '../../assets/cadastro/cadastro-05.svg'
import Cadastro from '../../assets/cadastro/cadastro-07.svg'
import Bandeiras from '../../assets/cadastro/cadastro-08.png'

import { FormPagamento } from './styles';


export default function Forms(props) {

	const [select, setSelect] = useState({})

	const { handleChange, change = () => {} } = props
	const { numeroCartao, dataDeValidade, codigoSeguranca } = props.values

	const options = [
		{ value: '1x', label: '1x R$ 750,00' },
		{ value: '2x', label: '2x R$ 375,00' },
		{ value: '3x', label: '3x R$ 250,00' }
	]

	useEffect(() =>{
		change(select)
	},[change, select])

	return (
		<FormPagamento>
			<Form>
				<h4>Digite abaixo o dado do cartão de credito</h4>

				<div className="container-form">
					<label htmlFor="">Titular do cartão</label>
					<Field type="text" name="titularCartao" />
					<ErrorMessage component="span" name="titularCartao" />
				</div>

				<div className="container-form">
					<label htmlFor="">Numero do Cartão</label>
					<InputMask mask="9999-9999-9999-9999" value={numeroCartao} onChange={handleChange} type="tel" name="numeroCartao" />
					<ErrorMessage component="span" name="numeroCartao" />
				</div>

				<div className="container-form cartao">
					<label htmlFor="">Data de Validade</label>
					<InputMask mask="99/99" value={dataDeValidade} onChange={handleChange} type="tel" name="dataDeValidade" />
					<ErrorMessage component="span" name="dataDeValidade" />
				</div>

				<div className="container-form cartao">
					<label htmlFor="">Código de segurança</label>
					<InputMask mask="999" value={codigoSeguranca} onChange={handleChange} type="tel" name="codigoSeguranca" />
					<ErrorMessage component="span" name="codigoSeguranca" />
				</div>

				<div className="container-form select">
					<label htmlFor="">N. Parcelas</label>
					<Select change={setSelect} options={options} label="" Search={false}/>

					<div className="container-parcelas">
						<p>{select.label ? "R$ 750,00" : ''} </p>
						<p>{select.label ? "Em " + select.label : ""}</p>
					</div>
				</div>
				
				<ButtonDefault><button type="submit" >Reservar </button></ButtonDefault>

				<span>Não é necessário nos ligar! Você receberá uma confirmação na hora em leivastrolez77@gmail.com</span>
			</Form>

			<div className="container-infs-usuario">
				<h4>Seus dados</h4>
				<div className="container-dados">
					<ul>
						<li>André Xavier</li>
						<li>andréxavier@gmail.com</li>
						<li>35. 9 9267 9866</li>
					</ul>

					<Link to="/"><img src={setaDir} alt="" /> Termos e Politica do Hotel</Link>
				</div>
			</div>


			<div className="container-text">
				<span>A reserva é feita diretamente com Hotel Nacional Inn Poços de Caldas, ou seja, ao completar esta reserva, você concorda com as condições da reserva, os termos gerais e a política de privacidade.</span>
			</div>

			<div className="container-compra">
				<img src={Cadastro} alt=""/>
				<p>Compre em um ambiente totalmente seguro e protegido contra fraudes.
					<img src={Bandeiras} alt=""/>
				</p>
			</div>


		</FormPagamento>
	);
}
