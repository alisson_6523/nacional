import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Field, ErrorMessage } from 'formik'
import InputMask from "react-input-mask";

import CheckBox from '../../components/CheckBox'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'
import { FormDados } from './styles'
import setaDir from '../../assets/cadastro/cadastro-05.svg'


export default function FormCadastro(props) {

	const { checksHosped = () => { }, checksTerceiro = () => { }, handleChange } = props
	const { telefone = "", nome = "", eMail = "", confirmEmail = "", nomeCompleto = "", meuPedido = "", senhaConfirma = "", senha = "" } = props.values

	return (
		<FormDados>
			<Form action="">

				<h4>Quase lá! Só falta preencher as informações obrigatórias </h4>

				<div className="container-form">
					<label htmlFor="">Nome</label>
					<Field type="text" name="nome" onChange={handleChange} value={nome} />
					<ErrorMessage component="span" name="nome" />
				</div>

				<div className="container-form">
					<label htmlFor="">E-Mail</label>
					<Field type="text" name="eMail" onChange={handleChange} value={eMail} />
					<ErrorMessage component="span" name="eMail" />
				</div>

				<div className="container-form">
					<label htmlFor="">Confirmação Email</label>
					<Field type="text" name="confirmEmail" onChange={handleChange} value={confirmEmail} />
					<ErrorMessage component="span" name="confirmEmail" />
				</div>

				<div className="container-form">
					<label htmlFor="">Telefone</label>
					<InputMask mask="(99)99999-9999" value={telefone} onChange={handleChange} type="tel" name="telefone"></InputMask>
					<ErrorMessage component="span" name="telefone" />
				</div>


				<div className="container-senha">
					<div className="container-form">
						<label htmlFor="">Senha</label>
						<Field type="password" name="senha" autoComplete="new-password" onChange={handleChange} value={senha} />
						<ErrorMessage component="span" name="senha" />
					</div>

					<CheckBox check={checksHosped} title={'Criar senha depois'} />

					<div className="container-form">
						<label htmlFor="">Confirmar Senha</label>
						<Field type="password" name="senhaConfirma" autoComplete="new-password" onChange={handleChange} value={senhaConfirma} />
						<ErrorMessage component="span" name="senhaConfirma" />
					</div>
				</div>

				<div className="container-checks">
					<CheckBox check={checksHosped} title={'Eu sou o hóspede principal'} />
					<CheckBox check={checksTerceiro} title={'Reservando para outra pessoa'} />
				</div>


				<h4 className="title">Responsável do quarto 1 - Quarto Executivo Duplo</h4>

				<div className="container-form dif">
					<label htmlFor="">Nome Completo</label>
					<Field type="text" name="nomeCompleto" onChange={handleChange} value={nomeCompleto} />
					<ErrorMessage component="span" name="nomeCompleto" />
				</div>

				<h4>Pedidos especiais</h4>

				<div className="container-form">
					<label htmlFor="">Meu Pedido</label>
					<Field name="meuPedido" component="textarea" onChange={handleChange} value={meuPedido} />
					<ErrorMessage component="span" name="meuPedido" />
				</div>


				<div className="container-text">
					<p>Por favor, escreva seus pedidos em inglês e compartilharemos com a acomodação.</p>
					<br />
					<p>Os pedidos especiais não podem ser garantidos - mas a acomodação irá se esforçar para atender ao seu pedido.</p>
					<br />
				</div>

				{/* <Link to="/cadastro-pagamento">   </Link> */}
				<ButtonDefault><button type="submit" >Reservar </button></ButtonDefault>

				<Link to="/">Termos e Politica do Hotel <img src={setaDir} alt="" /></Link>
			</Form>
		</FormDados>
	);
}
