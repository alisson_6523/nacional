import React from 'react';
import { Container } from './styles';
import banner from '../../assets/bannerHome/bannerHome-02.jpg'


export default function BannerHome() {
	console.log(banner)

	return (
		<Container>
			<div className="container">
				<img src={banner} alt="" />
			</div>
		</Container>
	);
}
