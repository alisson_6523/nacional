import styled from 'styled-components';

export const Container = styled.section`
	width: 100%;
	height: 443px;
	.container{
		height: 100%;
		img{
			width: 100%;
			height: 100%;
			object-fit: cover;
		}
	}
`;
