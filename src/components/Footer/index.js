import React from 'react';
import { Link } from 'react-router-dom'

import { ContainerFooter } from './styles';
import logo from '../../assets/footer/footer-01.svg'
import face from '../../assets/footer/footer-04.svg'
import insta from '../../assets/footer/footer-03.svg'
import link from '../../assets/footer/footer-02.svg'

export default function Footer() {
	return (
		<ContainerFooter>
			<div className="container">
				<div className="container-social">
					<img src={logo} alt="" />

					<div className="social">
						<img src={face} alt="" />
						<img src={insta} alt="" />
						<img src={link} alt="" />
					</div>

					<span>© Rede Nacional Inn Hoteis 2020. Todos os direitos reservados.</span>
				</div>

				<div className="list-itens">
					<ul>
						<li><Link to="/">Promoções e Pacotes</Link></li>
						<li><Link to="/">Parque Tematico Walter World</Link></li>
						<li><Link to="/destinos">Todos Destinos</Link></li>
						<li><Link to="/todos-hoteis">Todos Hoteis </Link></li>
						<li><Link to="/grupos">Viagem em Grupos</Link></li>
						<li><Link to="/eventos">Eventos Corporativos</Link></li>
						<li><Link to="/">Solicitar Orçamento</Link></li>
					</ul>
				</div>

				<div className="institucional">
					<ul>
						<li><Link to="/quem-somos"><strong>Sobre nós</strong></Link></li>
						<li><Link to="/">Carreira</Link></li>
						<li><Link to="/">Blog</Link></li>
						<li><Link to="/">Políticas</Link></li>
						<li><Link to="/">Central de Atendimento</Link></li>
					</ul>
				</div>


				<div className="contato">
					<ul>
						<li><span></span><Link to="/"><strong>Reservas São Paulo</strong></Link></li>
						<li><Link to="/">11 3228-6411</Link></li>
					</ul>

					<ul>
						<li><span></span><Link to="/"><strong>Reservas Rio de Janeiro</strong></Link></li>
						<li><Link to="/">11 3228-6411</Link></li>
					</ul>

					<ul>
						<li><Link to="/"><strong>Reservas Minas Gerais</strong></Link></li>
						<li><Link to="/">11 3228-6411</Link></li>
						<li><Link to="/">11 3228-6411</Link></li>
						<li><Link to="/">11 3228-6411</Link></li>
					</ul>
				</div>
			</div>
		</ContainerFooter>
	);
}
