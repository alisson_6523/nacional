import styled from "styled-components";
import tel from "../../assets/footer/footer-06.svg";

export const ContainerFooter = styled.section`
	padding-top: 98px;
	padding-bottom: 134px;
	background-color: ${(props) => props.theme.colors.preto1};
	.container {
		display: flex;
		justify-content: space-between;
		.container-social {
			.social {
				width: 102px;
				display: flex;
				justify-content: space-between;
				margin-top: 88px;
				margin-bottom: 65px;
			}

			span {
				color: ${(props) => props.theme.colors.color01};
				font-family: ${(props) => props.theme.fonts.poppins};
				display: block;
				width: 238px;
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 150%;
			}
		}

		.list-itens {
			ul {
				li {
					margin-bottom: 16px;
					position: relative;
					padding-left: 16px;
					list-style: none;
					&:last-child {
						margin-bottom: 0px;
					}
					&:before {
						content: "";
						display: block;
						width: 6px;
						height: 6px;
						background-color: ${(props) =>
							props.theme.colors.verde2};
						border-radius: 50%;
						position: absolute;
						top: 50%;
						left: 0px;
						transform: translateY(-50%);
					}
					a {
						color: ${(props) => props.theme.colors.color01};
						font-family: ${(props) => props.theme.fonts.inter};
						font-style: normal;
						font-weight: normal;
						font-size: 14px;
						line-height: 150%;
						letter-spacing: -0.01em;
					}
				}
			}
		}

		.institucional {
			ul {
				li {
					margin-bottom: 16px;
					position: relative;
					padding-left: 16px;
					list-style: none;
					&:last-child {
						margin-bottom: 0px;
					}
					&:before {
						content: "";
						display: block;
						width: 6px;
						height: 6px;
						background-color: ${(props) =>
							props.theme.colors.verde2};
						border-radius: 50%;
						position: absolute;
						top: 50%;
						left: 0px;
						transform: translateY(-50%);
					}
					a {
						color: ${(props) => props.theme.colors.color01};
						font-family: ${(props) => props.theme.fonts.inter};
						font-style: normal;
						font-weight: normal;
						font-size: 14px;
						line-height: 150%;
						letter-spacing: -0.01em;
					}
				}
			}
		}

		.contato {
			ul {
				position: relative;
				padding-left: 47px;
				margin-bottom: 36px;
				&:before {
					content: "";
					display: block;
					width: 30px;
					height: 30px;
					background-size: cover;
					background-image: url(${tel});
					background-repeat: no-repeat;
					border-radius: 50%;
					position: absolute;
					top: 12px;
					left: 0px;
					transform: translateY(-50%);
				}
				li {
					margin-bottom: 16px;
					list-style: none;
					&:last-child {
						margin-bottom: 0px;
					}
					a {
						color: ${(props) => props.theme.colors.color01};
						font-family: ${(props) => props.theme.fonts.inter};
						font-style: normal;
						font-weight: normal;
						font-size: 14px;
						line-height: 150%;
						letter-spacing: -0.01em;
					}
				}
			}
		}
	}
`;
