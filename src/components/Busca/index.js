import React, { useState} from 'react';

import { Container } from './styles';
import Select from '../../components/Select'

export default function Busca() {
	const [cidade, setCidade] = useState([])
	const [hotel, setHotel] = useState([])

	const cidades = [
		{
			label: 'POÇOS DE CALDAS - MG',
			value: '1'
		},
		{
			label: 'UBERABA - MG',
			value: '2'
		},
		{
			label: 'BELO HORIZONTE - MG',
			value: '3'
		},
	]

	const hoteis = [
		{
			label: 'HOTEL DAN INN HIGIENÓPOLIS SÃO PAULO',
			value: '1'
		},
		{
			label: 'HOTEL DAN INN ARARAQUARA',
			value: '2'
		},
		{
			label: 'HOTEL NACIONAL INN PREVIDÊNCIA ARAXÁ',
			value: '3'
		},
	]

	
	return (
		<Container>
			<div className="container-search">
				<input placeholder="Qual seu próximo destino?" type="text"/>
			</div>

			<div className="container-select">
				<Select change={setCidade} options={cidades} label="Cidade - UF"/>
				
				<Select change={setHotel} options={hoteis} label="Hotel" Search={false} />
			</div>
		</Container>
	);
}
