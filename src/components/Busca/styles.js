import styled from 'styled-components';

import { Container as Select } from '../../components/Select/styles'
import setaBaixo from '../../assets/header/header-07.svg'
import Lupa from '../../assets/busca/busca-01.svg'

export const Container = styled.div`
	display: flex;
	.container-select{
		display: flex;
		margin-left: auto;
	}

	.container-search{
		position: relative;
		input{
			height: 100%;
			width: 300px;
			padding-left: 33px;
			border: none;
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;
		}

		&:before{
			content: '';
			display: block;
			width: 17px;
			height: 17px;
			position: absolute;
			top: 50%;
			left: 0px;
			transform: translateY(-50%);
			-webkit-mask: url(${Lupa});
			background-color: #8AC537;
			z-index: 20;
		}
	}


	${Select}{
		border-left: 1px solid rgba(89,92,118,0.3);		
		width: 256px;
		position: relative;
		cursor: pointer;
		&:before{
			content: '';
			display: block;
			width: 12px;
			height: 7px;
			position: absolute;
			top: 50%;
			right: 18px;
			transform: translateY(-50%);
			-webkit-mask: url(${setaBaixo});
			background-color: #8AC537;
			z-index: 20;
		}
		.react-select__placeholder{
			&:after, &:before{
				display: none;
			}
		}
		.react-select__control{
			width: 100%;
		}
		.react-select__value-container{
			cursor: pointer;
		}
		
		.react-select__menu{
			width: 100%;
		}
		.react-select__option{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;
			width: 100%;
			
		}

		.react-select__single-value{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;
			width: 70%;
			padding-top: 7px;
			height: 20px;
			display: block;
			text-overflow: ellipsis;
			white-space: pre;
			overflow: hidden;
		}
	}
`;
