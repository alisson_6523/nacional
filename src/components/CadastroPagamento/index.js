import React from 'react';

import { Pagamento } from './styles';
import MinhaRserva from '../Cadastro/minhaReserva'
import DadosPagamento from './dadosPagamento'

export default function CadastroPagamento() {
	return (
		<Pagamento>
			<div className="container">
				<MinhaRserva />

				<DadosPagamento />
			</div>
		</Pagamento>
	);
}
