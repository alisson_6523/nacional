import styled from 'styled-components';

export const Pagamento = styled.div`
	margin-top: 33px;
	margin-bottom: 86px;
	.container{
		display: flex;	
	}
`;

export const ContainerPagamento = styled.section`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	width: 800px;
	margin-left: auto;
	.header-meus-dados{
		display: flex;
		align-items: center;
		height: 71px;
		padding-left: 131px;
		border-bottom: 1px solid #E0E0E0;
		h3{
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;	
		}
	}
`;