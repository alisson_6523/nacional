import React, { useState } from 'react';

import { ContainerPagamento } from './styles'

import { Formik } from 'formik'
import { FormInitialState, validationPagamento } from '../../util/configForm'
import FormPagamento from '../../components/Forms/pagamento'

export default function CadastroPagamento() {
	const [select, setSelect] = useState({});

	return (
		<ContainerPagamento>
			<div className="header-meus-dados">
				<h3>Dados de pagamento</h3>
			</div>

			<div className="container-dados">
				<Formik
					enableReinitialize
					initialValues={FormInitialState.pagamento}
					onSubmit={(value, actions) => {
						value = { ...select, ...value }
						console.log(value, actions)
					}}

					render={props => <FormPagamento change={setSelect} {...props} />}

					validationSchema={validationPagamento}
				/>
			</div>
		</ContainerPagamento>
	);
}
