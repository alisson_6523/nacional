import styled from 'styled-components';

export const Container = styled.div`
	cursor: pointer;
	img{
		width: 100%;
		height: auto;
		object-fit: contain;
		border-radius: 4px;
		display: block;
		margin-bottom: 23px;
	}

	h4{
		font-family: ${props => props.theme.fonts.poppins};
		color: ${props => props.theme.colors.gray1};
		font-style: normal;
		font-weight: 600;
		font-size: 14px;	
		line-height: 170%;
	}

	span{
		font-family: ${props => props.theme.fonts.poppins};
		color: ${props => props.theme.colors.verde1};
		font-style: normal;
		font-weight: 600;
		font-size: 14px;
	}
`;
