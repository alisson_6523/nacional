import React from 'react';
import img from '../../assets/destino/destino-02.jpg'


import { Container } from './styles';

export default function CardDestino() {
	return (
		<Container>
			<img src={img} alt="" />

			<h4>Campos do Jordão - SP</h4>
			<span>3 Hoteis</span>
		</Container>
	);
}