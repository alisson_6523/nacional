import styled from 'styled-components';
import { Container as Select } from '../../components/Select/styles'
import setaBaixo from '../../assets/header/header-07.svg'

export const ContainerTodosEventos = styled.section`
	padding-top: 109px;
	padding-bottom: 233px;
	.container{
		
		.title-select{
			display: flex;
			align-items: center;
			margin-bottom: 71px;
			h1{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;	
			}

			.container-selects{
				display: flex;
				width: 508px;
				margin-left: auto;
				border: 1px solid rgba(89,92,118,0.3);
				border-radius: 4px;
				${Select}{
					border-right: 1px solid rgba(89,92,118,0.3);		
					width: 256px;
					position: relative;
					cursor: pointer;
					&:nth-child(1){
						width: 196px;
						border-left: none;
					}
					&:nth-child(2){
						width: 144px;
					}
					&:nth-child(3){
						width: 168px;
						border-right: none;
					}
					&:before{
						content: '';
						display: block;
						width: 12px;
						height: 7px;
						position: absolute;
						top: 50%;
						right: 18px;
						transform: translateY(-50%);
						-webkit-mask: url(${setaBaixo});
						background-color: #8AC537;
						z-index: 20;
					}
					.react-select__placeholder{
						margin-left: 25px;
						&:after, &:before{
							display: none;
						}
					}
					.react-select__control{
						width: 100%;
						background-color: #FBFBFB;
					}
					.react-select__value-container{
						cursor: pointer;
					}
					
					.react-select__menu{
						width: 100%;
					}
					.react-select__option{
						font-family: ${props => props.theme.fonts.inter};
						color: ${props => props.theme.colors.preto2};
						font-style: normal;
						font-weight: 500;
						font-size: 14px;
						line-height: 100%;
						width: 100%;
						
					}

					.react-select__single-value{
						font-family: ${props => props.theme.fonts.inter};
						color: ${props => props.theme.colors.preto2};
						font-style: normal;
						font-weight: 500;
						font-size: 14px;
						line-height: 100%;
						width: 70%;
						padding-top: 7px;
						height: 20px;
						display: block;
						text-overflow: ellipsis;
						white-space: pre;
						overflow: hidden;
					}
				}	
			}
		}

		.container-cards{
			display: grid;
			grid-template-columns: repeat(auto-fit, minmax(278px, 278px));
			justify-content: center;
			grid-gap: 53px 32px;
		}
	}
`;
