import React, { useState } from 'react';

import CardsDestinos from '../../components/CardDestino'

import Select from '../../components/Select'
import { ContainerTodosEventos } from './styles';

export default function TodosEventos() {
	const [capadicade, setCapadicade] = useState([])
	const [uf, setUf] = useState([])
	const [cidade, setCidade] = useState([])

	return (
		<ContainerTodosEventos>
			<div className="container">
				<div className="title-select">
					<h1>Todos os destinos</h1>

					<div className="container-selects">
						<Select change={setCapadicade} options={capadicades} label="Capacidade" />
						<Select change={setUf} options={ufs} label="UF" />
						<Select change={setCidade} options={cidades} label="Cidade" />
					</div>
				</div>

				<div className="container-cards">
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
					<CardsDestinos />
				</div>
			</div>
		</ContainerTodosEventos>
	);
}


const capadicades = [
	{
		label: '10',
		value: '1'
	},
	{
		label: '20',
		value: '2'
	},
	{
		label: '30',
		value: '3'
	},
]

const ufs = [
	{
		label: 'MG',
		value: '1'
	},
	{
		label: 'SP',
		value: '2'
	},
	{
		label: 'AC',
		value: '3'
	},
]

const cidades = [
	{
		label: 'POÇOS DE CALDAS - MG',
		value: '1'
	},
	{
		label: 'UBERABA - MG',
		value: '2'
	},
	{
		label: 'BELO HORIZONTE - MG',
		value: '3'
	},
]