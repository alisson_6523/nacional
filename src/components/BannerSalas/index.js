import React from 'react';

import { ContainerBannerSalas } from './styles';
import Banner from '../../assets/orcamento/orcamento-05.png'
import logo from '../../assets/orcamento/orcamento-6.svg'

export default function BannerSalas() {
	return (
		<ContainerBannerSalas>
			<div className="container-logo">
				<img src={logo} alt="" />
			</div>

			<div className="container-img">
				<img src={Banner} alt="" />
			</div>
		</ContainerBannerSalas>
	);
}
