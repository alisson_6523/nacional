import styled from 'styled-components';

export const ContainerBannerSalas = styled.div`
	position: relative;
	height: 289px;
	overflow: hidden;
	.container-logo{
		position: absolute;
		top: -225px;
    	right: -2px;
		z-index: 20;
	}
	.container-img{
		height: 100%;
		width: 100%;
		position: relative;
		z-index: 10;
		&:before{
			content: '';
			display: block;
			width: 100%;
			height: 100%;
			position: absolute;
			top: 0px;
			left: 0px;
			background: linear-gradient(0.77deg, #000000 3.08%, rgba(0, 0, 0, 0) 146.57%);
		}
		img{
			height: 100%;
			width: 100%;
			object-fit: cover;
		}
	}
`;
