import styled from 'styled-components';
import setaBaixo from '../../assets/header/header-07.svg'
import local from '../../assets/header/header-08.svg'

export const Container = styled.div`
	position: relative;
	&:before{
		content: '';
		display: block;
		width: 12px;
		height: 7px;
		position: absolute;
		top: 50%;
		right: 18px;
		transform: translateY(-50%);
		-webkit-mask: url(${setaBaixo});
		background-color: #8AC537;
		z-index: 20;
	}
  .react-select__control{
		height: 54px;
		border: none !important;
		width: 361px;
		border-color: transparent !important;
		box-shadow:none !important;
		position: relative;

		&.react-select__control--is-focused{
			.react-select__value-container{
				.react-select__input{
					left: -20px;
				}
			}
		}
	}

	.react-select__menu{
		width: 361px;
		z-index: 99;
	}

	.react-select__indicator-separator{
		display: none;
	}

	.react-select__placeholder{
		font-family: ${(props) => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 100%;
		margin-left: 59px;
		top: 8px;
		position: relative;
		&:after{
			content: '';
			display: block;
			width: 15px;
    		height: 18px;
			position: absolute;
			top: 50%;
			left: -32px;
			transform: translateY(-50%);
			-webkit-mask: url(${local});
			background-color: #BDBDBD;
		}
	}

	.react-select__single-value{
		font-family: ${(props) => props.theme.fonts.inter};
		color: ${(props) => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 100%;
		margin-left: 10px;
	}

	.react-select__indicator{
		display: none;
	}

	.react-select__input{
		position: relative;
		margin-left: 30px;
		left: -273px;
	}
`;
