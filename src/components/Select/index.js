import React from 'react';
import Select from 'react-select';
import { listHotelSelect } from '../../styles/plugins/select'
import { Container } from './styles';

export default function select({ change = () => { }, options = [], label = 'Escolha seu Hotel ou Destino', Search = true }) {
	return (
		<Container>
			<Select
				styles={listHotelSelect}
				placeholder={label}
				classNamePrefix="react-select"
				onChange={change}
				options={options}
				isSearchable={Search} />
		</Container>
	);
}
