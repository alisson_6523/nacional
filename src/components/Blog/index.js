import React from 'react';
import { Link } from 'react-router-dom'

import { ContainerBlog } from './styles'
import setaDir from '../../assets/evento/evento-03.svg'
import img from '../../assets/evento/evento-04.svg'

export default function Blog() {
	return (
		<ContainerBlog>
			<div className="container">
				<h4>Conheça nosso blog</h4>
				<h3>Dicas incríveis para sua viagem <Link to="/">Veja outros posts <img src={setaDir} alt="" /></Link></h3>

				<div className="container-card-blog">
					<div className="card-blog">
						<img src={img} alt=""/>
						<h3>As 7 dúvidas mais comuns sobre promoções de passagens aéreas</h3>
						<p>Recebemos milhares de leitores todos os dias aqui no Melhores Destinos e muitos deles estão...</p>
					</div>
				</div>
			</div>
		</ContainerBlog>
	);
}
