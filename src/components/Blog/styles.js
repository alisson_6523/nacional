import styled from 'styled-components';

export const ContainerBlog = styled.section`
	background-color: ${props => props.theme.colors.graybg};
	padding-top: 99px;
	.container{
		font-family: ${props => props.theme.fonts.poppins};
		h4{
			color: ${props => props.theme.colors.color04};
			font-style: normal;
			font-weight: 600;
			font-size: 12px;
			line-height: 150%;
			letter-spacing: 2.5px;
			text-transform: uppercase;	
			margin-bottom: 16px;
		}

		h3{
			font-style: normal;
			font-weight: 600;
			font-size: 24px;
			line-height: 135%;
			letter-spacing: -0.01em;
			display: flex;
			a{
				font-style: normal;
				font-weight: 600;
				font-size: 18px;
				line-height: 100%;	
				display: flex;
				align-items: center;
				margin-left: auto;
				color: ${props => props.theme.colors.color05};
				img{
					display: block;
					margin-left: 30px;
				}
			}
		}
	}
`;
