import React from 'react';

import { Container } from './styles';

function TitleUser({ title = 'Meus dados' }) {
	return (
		<Container>
			<h1>{title}</h1>
		</Container>
	);
}

export default TitleUser;