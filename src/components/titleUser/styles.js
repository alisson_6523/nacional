import styled from 'styled-components';

export const Container = styled.div`
	width: 832px;
	h1{
		font-family: ${props => props.theme.fonts.poppins};
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 600;
		font-size: 20px;
		line-height: 100%;
		padding-bottom: 19px;	
		border-bottom: 1px solid #E0E0E0;
		position: relative;
		&:before{
			content: '';
			display: block;
			width: 123px;
			height: 2px;
			background-color: ${props => props.theme.colors.verde2};
			position: absolute;
			bottom: 0px;
			left: 0px;
		}
	}
`;
