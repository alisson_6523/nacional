import React, { useState } from 'react';

import { ContainerHotel, Li } from './styles';



export default function Hotel() {
	
	let [arr, setArr] = useState(options)
	let [contentText, setContenText] = useState(content)
	
	function select(key){
		let novoArr = arr.map(item =>{
			item.active = false
			return item
		})


		let novoTex = contentText.map(item =>{
			item.active = false
			return item
		})

		novoArr[key].active = true
		novoTex[key].active = true

		setContenText(novoTex)
		setArr(novoArr)
	}

	

	return (
		<ContainerHotel>
			<div className="container">
				<div className="tab-esq">
					<ul>
						{arr.map((option, key) => <Li onClick={() => select(key)} className={option.active ? 'active': ''} key={key}>{option.label}</Li>)}
					</ul>
				</div>

				<div className="tab-dir">
					<h2>O Hotel</h2>

					{contentText.map((text, key) => <p className={text.active ? 'active': ''} key={key}>{text.text}</p>)}
				</div>
			</div>
		</ContainerHotel>
	);
}

let content = [
	{
		text: 'Como parte de um projeto audacioso, o Thermas Resort está dentro do Parque Temático Walter World, o que torna a hospedagem ainda mais divertida, especialmente para as crianças. O Resort fica a cinco minutos do centro de Poços de Caldas: cidade famosa pelo poder medicinal de suas águas sulfurosas, pela beleza dos cristais Murano e por sua programação cultural intensa. O Thermas conta com 146 apartamentos confortáveis e modernos, nas categorias Luxo e Superior e duas Suítes Presidenciais. A linda vista para as montanhas possibilita momentos de contemplação.',
		active: true,
	},
	{
		text: "Os apartamentos são desenhados pensando no conforto e conveniência dos hóspedes. Todos são equipados com ar-condicionado (quente ou frio), telefone, Wi-Fi, TV LCD, secador de cabelo, cofre, cobertores antialérgicos, banheiros com ducha, frigobar e espelho de aumento.",
		active: false,
	},
	{
		text: "Proporcionar qualidade de vida, cuidado com a saúde e tranquilidade: esses são os compromissos do Thermas com todos os hóspedes. Para isso há um Complexo Aquático com sete piscinas de diferentes tamanhos e profundidades. Um dos ambientes é fechado, aconchegante e com piscinas aquecidas a uma temperatura média de 32 graus. Já em um spa terceirizado, estão disponíveis vários tipos de terapias para relaxamento e cuidados com a saúde e com a beleza.",
		active: false,
	},
	{
		text: "Além disso, o Resort conta com espaço fitness, quadras para a prática de esportes e Espaço Kids: tudo para transformar sua viagem em algo inesquecível, em meio à natureza exuberante de Minas Gerais. Nossos principais diferenciais são os sistemas all inclusive e open bar. Resumindo: o Thermas Resort Walter World é sinônimo de comodidade! ",
		active: false,
	},
	{
		text: 'Como parte de um projeto audacioso, o Thermas Resort está dentro do Parque Temático Walter World, o que torna a hospedagem ainda mais divertida, especialmente para as crianças. O Resort fica a cinco minutos do centro de Poços de Caldas: cidade famosa pelo poder medicinal de suas águas sulfurosas, pela beleza dos cristais Murano e por sua programação cultural intensa. O Thermas conta com 146 apartamentos confortáveis e modernos, nas categorias Luxo e Superior e duas Suítes Presidenciais. A linda vista para as montanhas possibilita momentos de contemplação.',
		active: true,
	},
	{
		text: "Os apartamentos são desenhados pensando no conforto e conveniência dos hóspedes. Todos são equipados com ar-condicionado (quente ou frio), telefone, Wi-Fi, TV LCD, secador de cabelo, cofre, cobertores antialérgicos, banheiros com ducha, frigobar e espelho de aumento.",
		active: false,
	},
	{
		text: "Proporcionar qualidade de vida, cuidado com a saúde e tranquilidade: esses são os compromissos do Thermas com todos os hóspedes. Para isso há um Complexo Aquático com sete piscinas de diferentes tamanhos e profundidades. Um dos ambientes é fechado, aconchegante e com piscinas aquecidas a uma temperatura média de 32 graus. Já em um spa terceirizado, estão disponíveis vários tipos de terapias para relaxamento e cuidados com a saúde e com a beleza.",
		active: false,
	},
	{
		text: "Além disso, o Resort conta com espaço fitness, quadras para a prática de esportes e Espaço Kids: tudo para transformar sua viagem em algo inesquecível, em meio à natureza exuberante de Minas Gerais. Nossos principais diferenciais são os sistemas all inclusive e open bar. Resumindo: o Thermas Resort Walter World é sinônimo de comodidade! ",
		active: false,
	},
	{
		text: 'Como parte de um projeto audacioso, o Thermas Resort está dentro do Parque Temático Walter World, o que torna a hospedagem ainda mais divertida, especialmente para as crianças. O Resort fica a cinco minutos do centro de Poços de Caldas: cidade famosa pelo poder medicinal de suas águas sulfurosas, pela beleza dos cristais Murano e por sua programação cultural intensa. O Thermas conta com 146 apartamentos confortáveis e modernos, nas categorias Luxo e Superior e duas Suítes Presidenciais. A linda vista para as montanhas possibilita momentos de contemplação.',
		active: true,
	},
	{
		text: "Os apartamentos são desenhados pensando no conforto e conveniência dos hóspedes. Todos são equipados com ar-condicionado (quente ou frio), telefone, Wi-Fi, TV LCD, secador de cabelo, cofre, cobertores antialérgicos, banheiros com ducha, frigobar e espelho de aumento.",
		active: false,
	},
	{
		text: "Proporcionar qualidade de vida, cuidado com a saúde e tranquilidade: esses são os compromissos do Thermas com todos os hóspedes. Para isso há um Complexo Aquático com sete piscinas de diferentes tamanhos e profundidades. Um dos ambientes é fechado, aconchegante e com piscinas aquecidas a uma temperatura média de 32 graus. Já em um spa terceirizado, estão disponíveis vários tipos de terapias para relaxamento e cuidados com a saúde e com a beleza.",
		active: false,
	},
	{
		text: "Além disso, o Resort conta com espaço fitness, quadras para a prática de esportes e Espaço Kids: tudo para transformar sua viagem em algo inesquecível, em meio à natureza exuberante de Minas Gerais. Nossos principais diferenciais são os sistemas all inclusive e open bar. Resumindo: o Thermas Resort Walter World é sinônimo de comodidade! ",
		active: false,
	},
	{
		text: "Além disso, o Resort conta com espaço fitness, quadras para a prática de esportes e Espaço Kids: tudo para transformar sua viagem em algo inesquecível, em meio à natureza exuberante de Minas Gerais. Nossos principais diferenciais são os sistemas all inclusive e open bar. Resumindo: o Thermas Resort Walter World é sinônimo de comodidade! ",
		active: false,
	},
]

let options = [
	{
		label: 'O Hotel',
		active: true,
	},
	{
		label: 'Grupos e Eventos',
		active: false
	},
	{
		label: 'Estrutura',
		active: false
	},
	{
		label: 'Localização',
		active: false
	},
	{
		label: 'Condições Gerais',
		active: false
	},
	{
		label: 'Sobre a Cidade',
		active: false
	},
	{
		label: 'Atrações Turísticas',
		active: false
	},
	{
		label: 'Complexo Aquático',
		active: false
	},
	{
		label: 'Spa e Serviços',
		active: false
	},
	{
		label: 'Parque temático',
		active: false
	},
	{
		label: 'Comodidades',
		active: false
	},
	{
		label: 'Férias em Família',
		active: false
	},
	{
		label: 'All Inclusive',
		active: false
	},
]