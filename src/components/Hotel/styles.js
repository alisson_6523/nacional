import styled from 'styled-components';

export const ContainerHotel = styled.section`
	margin-bottom: 92px;
	.container{
		display: flex;
		.tab-esq{
			padding-left: 43px;
			border-left: 1px solid #E0E0E0;
			padding-top: 20px;
			padding-bottom: 20px;
			ul{
				width: 192px;
			}
		}

		.tab-dir{
			width: 903px;
			margin-left: auto;
			position: relative;
			h2{				
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: bold;
				font-size: 32px;
				line-height: 150%;	
				margin-bottom: 32px;
			}

			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.gray2};
				font-style: normal;
				font-weight: normal;
				font-size: 16px;
				line-height: 170%;	
				margin-bottom: 15px;
				position: absolute;
				top: 0px;
				left: 0px;
				opacity: 0;
				pointer-events: none;
				&.active{
					position: static;
					opacity: 1;
					pointer-events: all;
				}
			}
		}
	}
`;

export const Li = styled.li`
	font-family: ${props => props.theme.fonts.inter};
	color: ${props => props.theme.colors.gray2};
	font-style: normal;
	font-weight: normal;
	font-size: 16px;
	line-height: 150%;	
	cursor: pointer;	
	margin-bottom: 15px;
	transition: all .5s;
	&.active{
		color: ${props => props.theme.colors.verde1};	
		position: relative;
		transition: all .5s;
		&:before{
			content: '';
			display: block;
			height: 17px;
			width: 2px;
			background-color: ${props => props.theme.colors.verde1};
			position: absolute;
			top: 4px;
			left: -44px;
		}
	}
`