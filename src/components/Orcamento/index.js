import React from 'react';

import { ContainerOrcamento } from './styles';
import logo from '../../assets/orcamento/orcamento-01.svg'
import check from '../../assets/orcamento/orcamento-02.svg'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'


export default function Orcamento() {
	return (
		<ContainerOrcamento>
			<div className="container">
				<div className="container-text">
					<h1>Orçamento Online</h1>

					<div className="list-btn">
						<div className="list">
							<img src={check} alt="" />
							<span>Faça seu orçamento online de forma fácil <br /> e sem esquecer de nehum detalhe.</span>
						</div>

						<ButtonDefault>
							<a href="">Solicitar Orçamento</a>
						</ButtonDefault>
					</div>
				</div>


				<div className="container-img">
					<img src={logo} alt="" />
				</div>
			</div>
		</ContainerOrcamento>
	);
}
