import styled from 'styled-components';
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const ContainerOrcamento = styled.section`
	margin-bottom: 116px;
	.container{
		height: 239px;
		background-color: ${props => props.theme.colors.verde1};
		padding-left: 72px;
		padding-top: 45px;
		position: relative;
		h1{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.gray6};
			font-style: normal;
			font-weight: 600;
			font-size: 50px;
			line-height: 130%;	
			margin-bottom: 13px;
		}

		.list-btn{
			display: flex;
			align-items: center;
			.list{
				display: flex;
				align-items: flex-start;
				
				img{
					display: block;
    				margin-top: 1px;
				}

				span{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.gray6};
					font-style: normal;
					font-weight: normal;
					font-size: 13px;
					line-height: 130%;	
					display: block;
					margin-left: 9px;
				}
			}

			${ButtonDefault}{
				margin-left: 158px;
				a{
					height: 57px;
					border: 1px solid #FFFFFF;
				}
			}
		}

		.container-img{
			position: absolute;
			height: 349px;
			width: 393px;
			top: -110px;
			right: 0px;
			overflow: hidden;
		}
	}
`;
