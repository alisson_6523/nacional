import styled, { css } from "styled-components";
import lupa from "../../assets/menu/menu-01.svg";

export const Container = styled.div`
	position: fixed;
	top: 0px;
	right: -100vw;
	width: 100vw;
	height: 100vh;
	background-color: #fff;
	pointer-events: none;
	overflow-y: scroll;
	z-index: -1;
	transition: all 0.5s;
	&.active {
		right: 0px;
		pointer-events: all;
		z-index: 90;
		transition: all 0.5s;
	}
	nav {
		height: 100%;
		.container-search {
			border-bottom: 1px solid #bdbdbd;
			.container {
				&.filtro {
					height: 75px;
					display: flex;
					align-items: center;
					.container-filtro {
						position: relative;
						input {
							background: rgba(189, 189, 189, 0.1);
							width: 708px;
							height: 30px;
							border: 0px;
							padding: 0px;
							margin: 0px;
							padding-left: 32px;
							font-family: ${(props) =>
								props.theme.fonts.poppins};
							font-style: normal;
							font-weight: 600;
							font-size: 13px;
							line-height: 180%;
							background: rgba(189, 189, 189, 0.1);
							color: rgba(0, 0, 0, 0.6);
						}

						&:before {
							content: "";
							display: block;
							width: 15px;
							height: 16px;
							position: absolute;
							top: 50%;
							right: 12.14px;
							transform: translateY(-50%);
							-webkit-mask: url(${lupa});
							background-color: #000;
							opacity: 0.3;
							z-index: 20;
						}
					}

					.container-filtro-estatdo {
						margin-left: 43px;
						ul {
							display: flex;
							li {
								font-family: ${(props) =>
									props.theme.fonts.poppins};
								cursor: pointer;
								font-style: normal;
								font-weight: 600;
								font-size: 13px;
								line-height: 180%;
								background: rgba(189, 189, 189, 0.1);
								color: rgba(0, 0, 0, 0.6);
								height: 30px;
								width: 42px;
								border-radius: 1px;
								display: flex;
								align-items: center;
								justify-content: center;
								margin-right: 1px;
								&:last-child {
									margin-right: 0px;
								}
							}
						}
					}

					.toogle {
						display: flex;
						margin-left: auto;
						align-items: center;
						cursor: pointer;
					}
				}
			}
		}

		.list-hoteis-cidade {
			padding-top: 63px;
			padding-bottom: 108px;
			column-count: 3;
		}
	}
`;

export const ListItem = styled.ul`
	margin-bottom: 40px;
	break-inside: avoid-column;
	&:last-child {
		margin-bottom: 0px;
	}
	li {
		&:first-child {
			font-family: ${(props) => props.theme.fonts.poppins};
			color: ${(props) => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 180%;
		}

		font-family: ${(props) => props.theme.fonts.inter};
		color: ${(props) => props.theme.colors.gray2};
		font-style: normal;
		font-weight: normal;
		font-size: 14px;
		line-height: 180%;
	}
`;
