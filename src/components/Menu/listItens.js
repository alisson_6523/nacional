import React from "react";

import { ListItem } from "./styles";

export default function Menu({ options }) {
	return (
		<ListItem>
			{options.map((cidade, key) => (
				<li key={key}>{cidade}</li>
			))}
		</ListItem>
	);
}
