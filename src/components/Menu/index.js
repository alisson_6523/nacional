import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Creators as MicroActions } from "../../store/ducks/microinteracoes";
import { Container } from "./styles";
import ListItens from "./listItens";
import { cidadesHoteis, toggleMenu } from "../../util/helper";
import button from "../../assets/header/header-06.svg";

export default function Menu() {
	const [cidade, setCidade] = useState(cidadesHoteis);
	const active = useSelector((state) => state.microinteracoes.menu_cidade_hoteis.active.active);
	const dispatch = useDispatch();

	function toggle(active_modal) {
		dispatch(MicroActions.toggleMenuCidadeHoteis(active_modal));
	}

	const filtro = (valorDigitado) =>
		setCidade(
			cidadesHoteis.filter((cidade) =>
				JSON.stringify(cidade, ["cidadeHoteis"])
					.toLocaleLowerCase()
					.includes(valorDigitado)
			)
		);

	const filtroEstado = (estado) => 
		setCidade(
			cidadesHoteis.filter((cidade) =>
				JSON.stringify(cidade, ["cidadeHoteis"])
					.toLocaleLowerCase()
					.includes(" - " + estado)
			)
	)

	return (
		<Container className={`${active ? "active" : ""}`}>
			<nav>
				<div className="container-search">
					<div className="container filtro">
						<div className="container-filtro">
							<input
								type="text"
								placeholder="Encontre seu Destino"
								onChange={(event) =>filtro( event.target.value.toLocaleLowerCase())}
							/>
						</div>

						<div className="container-filtro-estatdo">
							<ul>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>MG</li>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>SP</li>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>SC</li>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>BA</li>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>PR</li>
								<li onClick={(event) =>filtroEstado(event.target.innerHTML.toLocaleLowerCase())}>PE</li>
							</ul>
						</div>

						<div className="toogle" onClick={() => toggleMenu(toggle)}>
							<img src={button} alt="" />
						</div>
					</div>
				</div>

				<div className="container list-hoteis-cidade">
					{cidade.map((cidade, key) => (
						<ListItens key={key} options={cidade.cidadeHoteis} />
					))}
				</div>
			</nav>
		</Container>
	);
}
