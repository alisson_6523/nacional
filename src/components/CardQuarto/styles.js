import styled from 'styled-components';


export const Container = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	width: 800px;
	margin-bottom: 29px;
	&:last-child{
		margin-bottom: 100px;
	}
	.header-quarto{
		display: flex;
		align-items: center;
		height: 75px;
		border-bottom: 1px solid #E0E0E0;
		padding-left: 26px;
		h3{
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: bold;
			font-size: 20px;
			line-height: 100%;
			width: 306px;
		}

		span{
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;	
			color: ${props => props.theme.colors.preto1};
			background-color: ${props => props.theme.colors.laranjinha};
			display: flex; 
			justify-content: center;
			text-align: center;
			align-items: center;
			width: 266px;
			padding: 7.5px 0px;
			border-radius: 4px;
			img{
				display: block;
				margin-right: 12px;
			}
		}
	}

	.container-infs{
		padding-left: 26px;
		padding-top: 28px;
		padding-bottom: 30px;
		display: flex;
		.container-status{
			display: flex;
			flex-direction: column;
		}
			
	}
`;
