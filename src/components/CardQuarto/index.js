import React, { useState } from 'react';
import quarto from '../../assets/quarto/quarto-01.svg'

import CardStatus from '../../components/CardHotelStatus'
import InfsHotel from '../../components/CardHotelStatus/infHotel'

import { Container } from './styles';

export default function CardsQuarto() {

	const [qtd, setQtd] = useState({})

	return (
		<Container>
			<div className="header-quarto">
				<h3>Quarto Single</h3>
				<span>
					<img src={quarto} alt=""/>
					Melhor tárifa custo benefício
				</span>
			</div>

			<div className="container-infs">

				<InfsHotel options={options}/>

				<div className="container-status">
					{infs.map((inf, key) => <CardStatus key={key} infs={inf} change={setQtd}/>)}
				</div>

			</div>
		</Container>
	);
}


const infs = [
	{
		status: 'Cancelamento',
		cafe: true,
		valor: '245,00',
		tipo: 'cancelamento'
	},
	{
		status: 'Tárifa não reembolsável',
		cafe: false,
		valor: '202,00',
		tipo: 'reembolsavel'
	}
]


const options = [
	{
		title: '1 cama de Solteiro'
	},
	{
		title: '1 cama de Casal'
	},
]