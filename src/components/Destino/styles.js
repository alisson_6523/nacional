import styled from 'styled-components';
import check from '../../assets/busca/busca-03.svg'

export const ContainerDestino = styled.section`
	margin-bottom: 105px;
	.container{
		display: flex;
		justify-content: center;
		align-items: flex-end;
		.container-text{
			width: 471px;
			margin-right: 49px;
			h3{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;
				margin-bottom: 12px;	
			}

			span{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 16px;
				line-height: 100%;	
			}

			ul{
				display: grid;
				grid-template-columns: repeat(2, 1fr);
				margin-top: 59px;
				width: 101%;
				li{
					padding-left: 28px;
					font-family: ${props => props.theme.fonts.inter};
					color: ${props => props.theme.colors.preto2};
					font-style: normal;
					font-weight: normal;
					font-size: 16px;
					line-height: 150%;
					position: relative;
					&:before{
						content: '';
						display: block;
						width: 16px;
						height: 16px;
						position: absolute;
						top: 50%;
						left: 0px;
						transform: translateY(-50%);
						-webkit-mask: url(${check});
						-webkit-mask-repeat: no-repeat;
						background-color: #409A3C;
					}		
				}
			}
		}

		.logos{
			display: grid;
			grid-template-columns: repeat(2, 1fr);
			grid-template-rows: repeat(4, 91.5px);
			border-top: 1px solid #F2F2F2;	
			border-radius: 4px;
			.container-logo{
				border: 1px solid #F2F2F2;	
				border-top: none;
				border-radius: 4px;
				width: 244px;
				height: 91.5px;
				display: flex;
				justify-content: center;
				align-items: center;
			}
		}
	}
`;
