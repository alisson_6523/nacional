import React from 'react';

import { ContainerDestino } from './styles';
import vilageIn from '../../assets/quemSomos/somos-03.svg'
import thermas from '../../assets/quemSomos/somos-04.svg'
import golden from '../../assets/quemSomos/somos-05.svg'
import danin from '../../assets/quemSomos/somos-06.svg'
import nacional from '../../assets/quemSomos/somos-07.svg'
import hotel from '../../assets/quemSomos/somos-08.svg'
import victoria from '../../assets/quemSomos/somos-09.svg'

export default function Destino() {
	return (
		<ContainerDestino>
			<div className="container">
				<div className="container-text">
					<h3>23 Destinos</h3>
					<span>A Nacional Inn Hotéis está presente nas seguintes cidades:</span>

					<ul>
						<li>Araraquara</li>
						<li>Araxá</li>
						<li>Barretos</li>
						<li>Belo Horizonte</li>
						<li>Campinas</li>
						<li>Campos do Jordão</li>
						<li>Curitiba</li>
						<li>Foz do Iguaçu</li>
						<li>Franca</li>
						<li>Limeira</li>
						<li>Piracicaba</li>
						<li>Poços de Caldas</li>
						<li>Porto Alegre</li>
						<li>Recife</li>
						<li>Ribeirão Preto</li>
						<li>Rio de Janeiro</li>
						<li>Salvador</li>
						<li>Santa Rita do Passa Quatro</li>
						<li>São Carlos</li>
						<li>São José dos Campos</li>
						<li>São Paulo</li>
						<li>Sorocaba</li>
						<li>Uberaba</li>
					</ul>
				</div>

				<div className="logos">
					<div className="container-logo">
						<img src={vilageIn} alt=""/>
					</div>

					<div className="container-logo">
						<img src={thermas} alt=""/>
					</div>

					<div className="container-logo">
						<img src={golden} alt=""/>
					</div>

					<div className="container-logo">
						<img src={danin} alt=""/>
					</div>

					<div className="container-logo">
						<img src={nacional} alt=""/>
					</div>

					<div className="container-logo">
						<img src={hotel} alt=""/>
					</div>

					<div className="container-logo">
						<img src={victoria} alt=""/>
					</div>
				</div>
			</div>
		</ContainerDestino>
	);
}
