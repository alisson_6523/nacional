import React from 'react';

import { ContainerTodosDestinos } from './styles';
import CardDestino from './cardDestino'

export default function TodosDestinos() {
	return (
		<ContainerTodosDestinos>
			<div className="container">
				<h1>Confira todos nossos Destinos</h1>

				<div className="container-cards">
					<CardDestino />
					<CardDestino />
					<CardDestino />
					<CardDestino />
					<CardDestino />
					<CardDestino />
					<CardDestino />
					<CardDestino />
				</div>
			</div>
		</ContainerTodosDestinos>
	);
}
