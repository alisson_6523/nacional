import styled from 'styled-components';
import setaDir from '../../assets/destino/destino-08.svg'

export const ContainerTodosDestinos = styled.section`
	padding-bottom: 214px;
	.container{
		h1{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;
			margin-top: 47px;
			margin-bottom: 55px;
		}

		.container-cards{
			display: grid;
			grid-template-columns: repeat(2, 591px);
			grid-gap: 30px 32px;
		}
	}
`;

export const CardDestisno = styled.div`
	width: 100%;
	
	border-radius: 4px;
	.container-img{
		height: 167px;
		width: 100%;
		img{
			width: 100%;
			height: 100%;
			object-fit: cover;
			border-radius: 4px 4px 0px 0px;
		}
	}

	.container-text{
		height: 71px;
		display: flex;
		align-items: center;
		justify-content: space-between;
		padding-left: 31px;
		padding-right: 48px;
		border: 1px solid rgba(89,92,118,0.3);
		border-top: none;
		border-radius: 0px 0px 4px 4px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			position: relative;
			&:before{
				content: '';
				display: block;
				position: absolute;
				top: -27px;
				left: 0px;
				background-color: #409A3C;
				width: 47px;
				height: 3px;
			}
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 100%;
			position: relative;
			&:before{
				content: '';
				display: block;
				width: 7px;
    			height: 10px;
				position: absolute;
				top: 57%;
				right: -11px;
				transform: translateY(-50%);
				-webkit-mask: url(${setaDir});
				-webkit-mask-repeat: no-repeat;
				background-color: ${props => props.theme.colors.verde1};
			}	
		}
	}
`
