import React from 'react';

import { CardDestisno } from './styles';
import img from '../../assets/destino/destino-07.png'


export default function CardDestino() {
	return (
		<CardDestisno>
			<div className="container-img">
				<img src={img} alt=""/>
			</div>

			<div className="container-text">
				<p>Araxá - MG</p>

				<span>3 Hoteis</span>
			</div>
		</CardDestisno>
	);
}
