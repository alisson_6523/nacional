import React from 'react';

import { Hoteis } from './styles';
import ContainerCardHotel from '../../components/CardHotel'

export default function TodosHoteis() {
	return (
		<Hoteis>
			<div className="container">
				
				<ContainerCardHotel />
				<ContainerCardHotel />
				<ContainerCardHotel />
				<ContainerCardHotel />
				<ContainerCardHotel />
				
			</div>
		</Hoteis>
	);
}
