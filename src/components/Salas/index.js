import React from 'react';

import setaDir from '../../assets/orcamento/orcamento-07.svg'

import CardsDestinos from '../../components/CardDestino'
import { ContainerSalas, LinkSala } from './styles'


export default function Salas() {
	return (
		<ContainerSalas>
			<div className="container titles">
				<h2>Salas em 23 destinos</h2>
				<LinkSala to="/todos-destino">Ver todos os destinos <img src={setaDir} alt="" /></LinkSala>
			</div>

			<div className="container cards">
				<CardsDestinos />
				<CardsDestinos />
				<CardsDestinos />
				<CardsDestinos />
			</div>
		</ContainerSalas>
	);
}
