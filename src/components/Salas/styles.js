import styled from 'styled-components';
import { Link } from 'react-router-dom'

export const ContainerSalas = styled.section`
    margin-top: -134px;
    position: relative;
    z-index: 40;
	margin-bottom: 208px;
	.container{
		&.titles{
			display: flex;
			justify-content: space-between;
			h2{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.gray6};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 120%;
				margin-bottom: 26px;	
			}
		}

		&.cards{
			display: flex;
			justify-content: space-between;
		}
	}
`;

export const LinkSala = styled(Link)`
	font-family: ${props => props.theme.fonts.inter};
	color: #8AC537;
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	line-height: 100%;
	display: flex;
	align-items: center;
	img{
		display: block;
		margin-left: 61px;
	}
`
