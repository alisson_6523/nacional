import React from 'react';

import { ContainerCard } from './styles';
import ImgHotel from '../../assets/card/card-01.jpg'
import users from '../../assets/card/card-02.svg'
import setaBaixo from '../../assets/card/card-03.svg'
import setaDir from '../../assets/card/card-06.svg'
import setaDirPacotes from '../../assets/card/card-05.svg'
import ImgPacote from '../../assets/card/card-07.jpg'

export default function CardOfertaDia({ tipo = false }) {
	switch (tipo) {
		//Card Pacote
		case 'pacote':
			return (
				<ContainerCard tipo='pacote'>
					<img className="img-hotel" src={ImgPacote} alt="" />

					<div className="container-text">
						<h4>Feriado de Finados 2020</h4>

						<h3>Nacional Inn</h3>

						<span className="sub-title">Poços de Caldas - MG</span>

						<div className="container-diaria">
							<div className="data">
								<span>02  de Novembro </span>
								<span>04  de Novembro </span>
							</div>

							<div className="min-diaria">
								<span>Min. 4 diárias</span>
							</div>
						</div>

						<div className="diaria">
							<img src={users} alt="" />
							<span><strong>Diaria para Casal</strong></span>
						</div>

						<div className="valor">
							<span>Diárias a partir de </span>
							<img src={setaDirPacotes} alt="" />
							<p><strong>R$ 432,90</strong></p>
						</div>

					</div>
				</ContainerCard>
			);

		default:
			return (
				<ContainerCard>
					<img className="img-hotel" src={ImgHotel} alt="" />

					<div className="container-text">
						<h3>Nacional Inn</h3>

						<span className="sub-title">Poços de Caldas - MG Poços de Caldas - MG</span>

						<div className="diaria">
							<img src={users} alt="" />
							<span>Diaria para Casal</span>

							<div className="desconto">
								<span> -20% <img src={setaBaixo} alt="" /></span>
							</div>
						</div>

						<div className="valor">
							<span>R$ 345,00</span>
							<img src={setaDir} alt="" />
							<p><strong>R$ 179,00</strong></p>
						</div>

					</div>
				</ContainerCard>
			);
	}
}
