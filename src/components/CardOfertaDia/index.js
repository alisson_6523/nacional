import React from 'react';
import Card from './card'


export default function CardOfertaDia({ tipo = false }) {
	switch (tipo) {
		case 'pacote':
			return <Card tipo={tipo} />

		default:
			return <Card />
	}
}
