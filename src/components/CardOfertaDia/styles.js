import styled, { css } from 'styled-components';

const pacote = css`
	h4{
		font-family: ${props => props.theme.fonts.poppins};
		font-style: normal;
		font-weight: normal;
		font-size: 14px;
		line-height: 100%;
		color: ${props => props.theme.colors.preto2};
		margin: 24px 0px;
		padding-left: 17px;
	}

	.container-text{
		.container-diaria{
			padding-left: 17px;
			display: flex;
			align-items: center;
			margin-top: 24px;
			.data{
				display: flex;
				flex-direction: column;
				height: 33px;
				justify-content: space-between;
				border-left: 1px solid #595C76;
				padding-left: 10px;
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 4px;
					height: 4px;
					border-radius: 50%;
					background-color: ${props => props.theme.colors.preto2};
					position: absolute;
					top: 0px;
					left: 0px;
					transform: translateX(-60%)
				}
				&:after{
					content: '';
					display: block;
					width: 4px;
					height: 4px;
					border-radius: 50%;
					background-color: ${props => props.theme.colors.preto2};
					position: absolute;
					bottom: 0px;
					left: 0px;
					transform: translateX(-60%)
				}
				span{
					font-family: ${props => props.theme.fonts.poppins};
					font-style: normal;
					font-weight: normal;
					font-size: 12px;
					line-height: 100%;	
					color: ${props => props.theme.colors.preto2};
				}	
			}

			.min-diaria{
				border: 1px solid ${props => props.theme.colors.preto2Opacity};
				display: flex;
				align-items: center;
				justify-content: center;
				margin-left: auto;
				border-radius: 4px;
				width: 103px;
				height: 40px;
				margin-left: 35px;
				span{
					color: ${props => props.theme.colors.verde1};
					font-style: normal;
					font-weight: 500;
					font-size: 14px;
					margin-bottom: 0px;
					display: flex;
					align-items: center;
					justify-content: center;
					padding-left: 0px;
					text-overflow: ellipsis;
					white-space: pre;
					img{
						display: block;
						margin: 0px;
						margin-left: 6px;
					}
				}
			}
		}

		.valor{
			span{
				font-size: 12px;
				color: ${props => props.theme.colors.preto2};
				&:before{
					display: none;
				}
			}
		}
	}	
`

export const ContainerCard = styled.div`
	width: 280px;
	display: flex;
	flex-direction: column;
	cursor: pointer;
	span{
		font-family: ${props => props.theme.fonts.poppins};
	}

	img{
		&.img-hotel{
			width: 100%;	
			object-fit: contain;		
			border-radius: 8px 8px 0px 0px;	
		}
	}

	.container-text{
		border: 1px solid ${props => props.theme.colors.preto2Opacity};
		border-radius: 0px 0px 8px 8px;	
		border-top: 0px;
		h3{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 100%;	
			margin-bottom: 8px;
			margin-top: 24px;
			padding-left: 17px;
		}

		span{
			&.sub-title{
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;	
				margin-bottom: 13px;
				padding-left: 17px;
				width: 84%;
				display: block;
				text-overflow: ellipsis;
				white-space: pre;
				overflow: hidden;
			}
		}

		.diaria{
			display: flex;
			align-items: center;
			height: 32px;
			width: 233px;
			padding-left: 17px;
			margin-top: 13px;
			img{
				display:block;
				margin-right: 7px;
			}

			span{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 500;
				font-size: 12px;
				margin-bottom: 0px;
				padding-left: 0px;
			}

			.desconto{
				border: 1px solid ${props => props.theme.colors.preto2Opacity};
				display: flex;
				align-items: center;
				justify-content: center;
				margin-left: auto;
				border-radius: 4px;
				width: 65px;
				height: 32px;
				
				span{
					color: ${props => props.theme.colors.red};
					font-style: normal;
					font-weight: 500;
					font-size: 14px;
					margin-bottom: 0px;
					display: flex;
					align-items: center;
					justify-content: center;
					padding-left: 0px;
					text-overflow: ellipsis;
					white-space: pre;
					img{
						display: block;
						margin: 0px;
						margin-left: 6px;
					}
				}
			}
		}

		.valor{
			background-color: ${props => props.theme.colors.laranjinha};
			border-radius: 0px 0px 8px 8px;	
			padding-left: 17px;
			padding-right: 37px;
			display: flex;
			align-items: center;
			justify-content: space-between;
			height: 63px;
			margin-top: 16px;
			span{
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;	
				padding-left: 0px;
				position: relative;
				margin-bottom: 0px;
				padding-left: 0;
				&:before{
					content: '';
					display: block;
					height: 1px;
					width: 100%;
					background-color: ${props => props.theme.colors.red};
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
				}
			}

			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 18px;
				line-height: 100%;	
			}
		}
	}

	${props => props.tipo === 'pacote' ? pacote : null}
`;
