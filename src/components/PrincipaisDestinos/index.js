import React, { useState, useEffect, useRef } from 'react'
import { Link } from 'react-router-dom'
import Swiper from 'swiper';

import { PrincipalDestino } from './styles'
import Select from '../../components/Select'
import CardDestino from '../../components/CardDestinoSlide'
import Controles from '../../components/btns/Controles'
import { swiperDestinoSlide } from '../../styles/plugins/swiper'
import setaDir from '../../assets/destino/destino-01.svg'
import Tripadvisor from '../../assets/destino/destino-04.svg'
import Logistica from '../../assets/destino/destino-05.svg'
import Quality from '../../assets/destino/destino-06.svg'

export default function PrincipaisDestinos() {
	const [change, setChange] = useState({})
	const swiperDestino = useRef(null)

	const options = [
		{ value: 'ARAÇATUBA - SP', label: 'ARAÇATUBA - SP' },
		{ value: 'ARARAQUARA - SP', label: 'ARARAQUARA - SP' },
		{ value: 'ARAXÁ - MG', label: 'ARAXÁ - MG' }
	]


	useEffect(() => {
		new Swiper(swiperDestino.current, swiperDestinoSlide);
	}, [])

	return (
		<PrincipalDestino>
			<div className="container">
				<div className="container-text">
					<h2>Estamos nos principais destino do Brasil.</h2>
					<h4>60 hotéis em 25 destinos em todo o Brasil!</h4>


					<Link to="/" >Todos os Destinos <img src={setaDir} alt="" /></Link>

					<p>A Nacional Inn Hotéis é referência no setor de hotelaria. Presente em sete estados e 23 destinos das regiões Sudeste, Sul e Nordeste, a empresa está em expansão no mercado nacional. A Nacional Inn marca presença em importantes capitais como São Paulo, Rio de Janeiro, Belo Horizonte, Curitiba, Salvador, Porto Alegre e Recife.</p>
					<br /><br />
					<p>As unidades se destacam por oferecer a melhor relação custo x benefício, localização estratégica e estrutura completa para lazer, negócios e eventos.</p>
				</div>

				<div className="container-destino">
					<Select options={options} change={setChange} label="Encontre seu destino" />

					<div className="container-slide-destino" ref={swiperDestino}>
						<div className="swiper-wrapper">
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
							<div className="swiper-slide">
								<CardDestino />
							</div>
						</div>
					</div>

					<Controles esq="seta-esq-destino" dir="seta-dir-destino" />

					<div className="premeio-hotel">
						<h3>Prêmios Nacional inn Hotéis.</h3>
						<Link to="/"><img src={Tripadvisor} alt="" /></Link>
						<Link to="/"><img src={Logistica} alt="" /></Link>
						<Link to="/"><img src={Quality} alt="" /></Link>
					</div>
				</div>
			</div>
		</PrincipalDestino>
	);
}
