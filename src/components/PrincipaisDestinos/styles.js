import styled from 'styled-components';

export const PrincipalDestino = styled.section`
	margin-bottom: 110px;
	.container{
		font-family: ${props => props.theme.fonts.poppins};
		display: flex;
		.container-text{
			width: 571px;
			position: relative;
			&:before{
				content: '';
				display: block;
				background-color: #DCDCDC;
				width: 71px;
				height: 1px;
				position: absolute;
				top: 50%;
				transform: translateY(-50%);
			}
			h2{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: bold;
				font-size: 42px;
				line-height: 150%;
				margin-bottom: 24px;
			}

			h4{
				color: ${props => props.theme.colors.gray3};
				font-style: normal;
				font-weight: normal;
				font-size: 22px;
				line-height: 100%;
				margin-bottom: 38px;
			}

			a{
				display: flex;
				align-items: center;
				justify-content: center;
				width: 241px;
				height: 51px;

				font-style: normal;
				font-weight: 600;
				font-size: 16px;
				line-height: 100%;

				color: ${props => props.theme.colors.color01};
				background-color: ${props => props.theme.colors.verde1};
				border-radius: 4px;
				margin-bottom: 100px;
				img{
					display: block;
					margin-left: 17px;
				}
			}

			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.gray2};
				font-style: normal;
				font-weight: normal;
				font-size: 16px;
				line-height: 150%;
			}
		}

		.container-destino{
			margin-left: auto;
			width: 537px;
			display: flex;
    		flex-direction: column;
			.react-select__control{
				width: 100%;
				border: 1px solid ${(props) => props.theme.colors.color03} !important;
				border-radius: 4px;
			}

			.container-slide-destino{
				width: 889px;
				overflow: hidden;
				margin-top: 42px;
			}

			.container-controles{
				position: relative;
				width: 108px;
				top: 40px;
				right: 0px;
				transform: unset;
				margin-left: auto;
			}

			.premeio-hotel{
				margin-top: auto;
				margin-left: 20px;
				h3{
					font-style: normal;
					font-weight: 600;
					font-size: 14px;
					line-height: 100%;
					margin-left: 20px;
					margin-bottom: 32px;
				}
			}
		}
	}
`;
