import styled from 'styled-components';

export const ContainerPricipaisOpcoes = styled.section`
	margin-bottom: 157px;
	.container{
		display: flex;
		.tab-esq{
			width: 571px;
			h3{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: bold;
				font-size: 32px;
				line-height: 150%;
				margin-bottom: 72px;
				position: relative;	
				&:before{
					content: '';
					display: block;
					width: 35.84px;
					height: 1px;
					background-color: ${props => props.theme.colors.verde2};
					position: absolute;
					left: 0px;
					bottom: -36px;
				}
			}

			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 16px;
				line-height: 150%;	
			}
		}


		.tab-dir{
			width: 491px;
			margin-left: auto;
			font-family: ${props => props.theme.fonts.poppins};	
			.premeio-hotel{
				margin-bottom: 127px;
				position: relative;	
				&:before{
					content: '';
					display: block;
					width: 71px;
					height: 1px;
					background-color: #DCDCDC;
					position: absolute;
					left: 0px;
					bottom: -64px;
				}
				h4{
					color: ${props => props.theme.colors.preto1};				
					font-style: normal;
					font-weight: 600;
					font-size: 14px;
					line-height: 100%;
					margin-left: 17px;
					margin-bottom: 46px;
				}
			}	

			h3{
				color: ${props => props.theme.colors.preto1};	
				font-style: normal;
				font-weight: bold;
				font-size: 22px;
				line-height: 150%;	
				display: flex;
				align-items: center;
				margin-bottom: 33px;
				img{
					display: block;
					margin-right: 16px;
				}
			}

			.container-infs{
				margin-bottom: 32px;
				&:last-child{
					margin-bottom: 0;
				}
				p{
					color: ${props => props.theme.colors.verde1};
					font-style: normal;
					font-weight: 600;
					font-size: 16px;
					line-height: 150%;	
				}

				span{				
					color: ${props => props.theme.colors.preto2};	
					font-family: ${props => props.theme.fonts.inter};
					font-style: normal;
					font-weight: normal;
					font-size: 14px;
					line-height: 150%;
				}
			}
		}
	}
`;
