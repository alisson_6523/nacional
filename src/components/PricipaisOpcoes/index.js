import React from 'react';

import { Link } from 'react-router-dom'
import { ContainerPricipaisOpcoes } from './styles'
import Tripadvisor from '../../assets/destino/destino-04.svg'
import Logistica from '../../assets/destino/destino-05.svg'
import Quality from '../../assets/destino/destino-06.svg'
import atencao from '../../assets/quarto/quarto-07.svg'


export default function PricipaisOpcoes() {
	return (
		<ContainerPricipaisOpcoes>
			<div className="container">
				<div className="tab-esq">
					<h3>Uma das nossas principais opções em Poços de Caldas.</h3>

					<p>O Nacional Inn fica a 50 metros da Praça dos Macacos, em Poços de Caldas. A propriedade dispõe de piscinas ao ar livre e coberta, academia e quadra de esportes. O Wi-Fi é gratuito.</p>
					<br />
					<p>Os quartos apresentam decoração clássica com tons pastel e incluem frigobar, ventilador de teto e TV a cabo. Alguns quartos são maiores e oferecem banheira, varanda e área de estar com sofá.</p>
					<br />
					<p>O Hotel Nacional Inn serve um buffet de café da manhã variado em um restaurante luxuoso. Uma região animada, cheia de bares e restaurantes, fica a apenas 2 minutos a pé do local.</p>
					<br />
					<p>Você poderá desfrutar da sauna e do salão de jogos, enquanto as crianças se divertem no parquinho infantil. A propriedade dispõe de recepção 24h, que pode providenciar serviços de lavanderia.</p>
					<br />
					<p>A Estação Rodoviária de Poços de Caldas fica a 5 km do hotel, e o aeroporto da cidade fica a 9 km. O Parque José Junqueira fica a 8 minutos a pé, e a Represa Bortolan fica a 12,5 km do local.</p>
					<br />
					<p>Viajantes individuais particularmente gostam da localização — eles deram nota 8,5 para hospedagem individual.</p>
					<br />
					<p>Nós falamos a sua língua!</p>
				</div>

				<div className="tab-dir">
					<div className="premeio-hotel">
						<h4>Prêmios Nacional inn Hotéis.</h4>
						<Link to="/"><img src={Tripadvisor} alt="" /></Link>
						<Link to="/"><img src={Logistica} alt="" /></Link>
						<Link to="/"><img src={Quality} alt="" /></Link>
					</div>

					<h3><img src={atencao} alt=""/> Informações Importantes</h3>

					<div className="container-infs">
						<p>Cancelamento/ pré-pagamento</p>

						<span>As políticas de cancelamento e pré-pagamento variam de acordo com o tipo de acomodação. Por favor, verifique quais condições se aplicam a cada opção quando fizer sua escolha.</span>
					</div>

					<div className="container-infs">
						<p>Crianças, camas e animais</p>

						<span>
							Crianças de qualquer idade são bem-vindas.
							Crianças com 5 anos ou mais são consideradas adultos nesta acomodação.
							Para ver os preços e as informações de ocupação certos, informe quantas crianças fazem parte do seu grupo e as idades delas.
							Não há capacidade para berços nesta acomodação.
							Não há capacidade para camas extras nesta acomodação.
							Animais de estimação: não permitidos
						</span>
					</div>
				</div>
			</div>
		</ContainerPricipaisOpcoes>
	);
}
