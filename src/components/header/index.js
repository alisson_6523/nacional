import React from "react";
import HeaderCadastro from "./etapa";
import HeaderUser from "./user";
import Header from "./header";

export default function header({ location, computedMatch }) {
	const { pathname, search, hash, state } = location;

	let rota = pathname.replace(/\//g, "");

	let authUser = ["user", "meus-dados", "minha-lista"];

	switch (rota) {
		case "cadastro-dados":
			return <HeaderCadastro etapa={[1]} />;

		case "cadastro-pagamento":
			return <HeaderCadastro etapa={[1, 2]} />;

		case "confirmacao-dados":
			return <HeaderCadastro etapa={[1, 2, 3]} />;

		case "user":
			return <HeaderUser />;

		case "meus-dados":
			return <HeaderUser />;

		case "minha-lista":
			return <HeaderUser />;

		default:
			return <Header />;
	}
}
