import React from 'react';

import { Link } from 'react-router-dom'
import { ContainerHeader } from './styles';
import Title from '../Titles'
import { ButtonDefault } from '../btns/btnBusca/styles'

export default function header() {
	const title = "Thermas Resort Walter World"
	const subTittle = "Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil"
	return (
		<ContainerHeader>
			<div className="container">
				<Title title={title} subTitle={subTittle} />
				<div className="container-tarifa">
					<span>Melhor tárifa</span>

					<h3>R$ 245,00</h3>

					<ButtonDefault><Link to="/">Reservar quarto</Link></ButtonDefault>
				</div>
			</div>
		</ContainerHeader>
	);
}
