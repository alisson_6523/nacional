import React from 'react';

import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Header, Containerlogo } from './styles'
import header01 from "../../assets/header/header-01.svg";
import homeImg from "../../assets/header/header-15.svg";
import imgUser from "../../assets/header/header-16.png";
import carrinho from "../../assets/header/header-17.svg";
import cupom from "../../assets/header/header-18.svg";
import dadosImg from "../../assets/header/header-19.svg";
import sairImg from "../../assets/header/header-20.svg";
import { userloggedFaceOrGoogle } from '../../util/helper'

function HeaderUser(props) {
	
	const { email, name, picture } = userloggedFaceOrGoogle(props)


	return (
		<Header>
			<div className="container">
				<Containerlogo>
					<Link to="/">
						<img src={header01} alt="" />
					</Link>
				</Containerlogo>

				<div className="container-menu">
					<div className="container-img-text">
						<img src={homeImg} alt="" />
						<Link to="/">Voltar a Home</Link>
					</div>

					<div className="container-menu-user">
						<div className="container-img">
							<img src={picture ? picture : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQo76m5TxJ9ltWuPLWezHcVs_7n3-9BjlHxDD5PP8LYfAL99RiB&usqp=CAU"} alt="" />
						</div>

						<h3>Olá <strong>{name}</strong></h3>

						<ul>
							<li>
								<Link to="/user">
									<img src={carrinho} alt="" />
									Minhas Reservas
								</Link>
							</li>
							<li>
								<Link to="/minha-lista">
									<img src={cupom} alt="" />
									Meus Cupons
								</Link>
							</li>
							<li>
								<Link to="/meus-dados">
									<img src={dadosImg} alt="" />
									Meus Dados
								</Link>
							</li>
							<li>
								<Link to="/">
									<img src={sairImg} alt="" />
									Sair
								</Link>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</Header>
	)
}

const mapStateToProps = state => ({
  face: state.user.face,
  google: state.user.google,
  user: state.user.user
});

export default connect(mapStateToProps)(HeaderUser)