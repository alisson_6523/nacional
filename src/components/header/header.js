import React from "react";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Creators as MicroActions } from "../../store/ducks/microinteracoes";
import { toggleMenu, userloggedFaceOrGoogle } from "../../util/helper";

import { Link } from "react-router-dom";
import logo from "../../assets/header/header-01.svg";
import whatsapp from "../../assets/header/header-02.svg";
import tel from "../../assets/header/header-03.svg";
import orcamento from "../../assets/header/header-04.svg";
import chat from "../../assets/header/header-05.svg";
import button from "../../assets/header/header-06.svg";
import userDefault from "../../assets/header/header-22.svg";

import {
	Header,
	Containerlogo,
	Containercontato,
	Contato,
	ButtonToggle,
} from "./styles";

function HeaderSite(props) {
	const { email, name, picture } = userloggedFaceOrGoogle(props);
	const { toggleMenuCidadeHoteis, ModalOrcamento } = props;

	return (
		<Header>
			<div className="container">
				<Containerlogo>
					<Link to="/">
						<img src={logo} alt="" />
					</Link>
				</Containerlogo>

				<Containercontato>
					<Contato>
						<a href="/">
							<img src={whatsapp} alt="" />
							Whatssap
						</a>
					</Contato>

					<Contato>
						<a href="/">
							<img src={tel} alt="" />
							11. 3292.9087
						</a>
					</Contato>

					<Contato onClick={() => toggleMenu(ModalOrcamento)}>
						<img src={orcamento} alt="" />
						Solicitar Orçamento
					</Contato>

					<Contato>
						<a href="/">
							<img src={chat} alt="" />
							Char Online
						</a>
					</Contato>
				</Containercontato>

				<img
					src={picture ? picture : userDefault}
					alt=""
					className="user-default img-user"
				/>

				<ButtonToggle
					onClick={() => toggleMenu(toggleMenuCidadeHoteis)}
				>
					<img src={button} alt="" />
				</ButtonToggle>
			</div>
		</Header>
	);
}

const mapDispatchToProps = (dispatch) =>
	bindActionCreators(MicroActions, dispatch);

const mapStateToProps = (state) => ({
	face: state.user.face,
	google: state.user.google,
	user: state.user.user,
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderSite);
