import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { userloggedFaceOrGoogle } from "../../util/helper";
import header01 from "../../assets/header/header-01.svg";
import userDefault from "../../assets/header/header-22.svg";

import { Header, Etapas, Containerlogo } from "./styles";

function Etapa(props) {
	let arr = [1, 2, 3];

	const { etapa } = props;
	const { picture, name } = userloggedFaceOrGoogle(props);

	return (
		<Header>
			<div className="container">
				<Containerlogo>
					<Link to="/">
						<img src={header01} alt="" />
					</Link>
				</Containerlogo>

				<Etapas>
					<div className="container-inf-user">
						<img
							src={picture ? picture : userDefault}
							alt=""
							className="img-user"
						/>

						<span>Olá, {name}</span>
					</div>
					<div className="container-etapas">
						{arr.map((span, key) => (
							<span
								key={key}
								className={etapa[key] ? "active" : ""}
							>
								{span}
							</span>
						))}
					</div>
				</Etapas>
			</div>
		</Header>
	);
}

const mapStateToProps = (state) => ({
	face: state.user.face,
	google: state.user.google,
	user: state.user.user,
});

export default connect(mapStateToProps, null)(Etapa);
