import styled from "styled-components";
import { Container } from "../Titles/styles";
import { ButtonDefault } from "../btns/btnBusca/styles";
import estrela from "../../assets/header/header-12.svg";
import setadir from "../../assets/header/header-13.svg";
import setadirCadastro from "../../assets/header/header-14.svg";

export const Header = styled.header`
	height: 81px;
	width: 100%;
	border-bottom: 1px solid ${(props) => props.theme.colors.gray6};
	.container {
		display: flex;
		height: 100%;
		align-items: center;
		.user-default {
			margin-right: 24px;
		}

		.img-user {
			display: block;
			width: 38px;
			border-radius: 50%;
			object-fit: cover;
		}
		.container-menu {
			height: 100%;
			width: 278px;
			margin-left: auto;
			position: relative;
			.container-img-text {
				background-color: ${(props) => props.theme.colors.verde1};
				width: 100%;
				display: flex;
				align-items: center;
				justify-content: center;
				height: 100%;
				a {
					font-style: normal;
					font-weight: 600;
					font-size: 15px;
					line-height: normal;
					font-family: ${(props) => props.theme.fonts.poppins};
					color: ${(props) => props.theme.colors.color01};
					display: flex;
					align-items: center;
					margin-left: 11px;
				}
			}

			.container-menu-user {
				background-color: ${(props) => props.theme.colors.verde1};
				width: 100%;
				display: flex;
				align-items: center;
				flex-direction: column;
				position: absolute;
				top: 81px;
				z-index: 30;
				padding-top: 95px;
				padding-bottom: 143px;
				.container-img {
					padding-bottom: 24px;
					img {
						width: 160px;
						height: 160px;
						border-radius: 50%;
						object-fit: cover;
					}
				}

				h3 {
					font-family: ${(props) => props.theme.fonts.poppins};
					color: ${(props) => props.theme.colors.color01};
					font-style: normal;
					font-weight: normal;
					font-size: 16px;
					line-height: 100%;
					margin-bottom: 42px;
				}

				ul {
					li {
						a {
							font-family: ${(props) =>
								props.theme.fonts.poppins};
							color: ${(props) => props.theme.colors.color01};
							font-style: normal;
							font-weight: 600;
							font-size: 13px;
							line-height: 180%;
							display: flex;
							align-items: center;
							img {
								display: block;
								margin-right: 13px;
							}
						}
					}
				}
			}
		}
	}
`;

export const Containerlogo = styled.div`
	display: block;
	margin-right: 95px;
`;

export const Containercontato = styled.div`
	display: flex;
	align-items: center;
	width: 100%;
`;

export const ButtonToggle = styled.button`
	width: 22.4px;
	height: 16px;
	background-color: transparent;
	margin-left: auto;
`;

export const Contato = styled.div`
	margin-right: 80px;
	display: flex;
	align-items: center;
	font-family: ${(props) => props.theme.fonts.inter};
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	color: ${(props) => props.theme.colors.preto1};
	cursor: pointer;

	a {
		display: flex;
		align-items: center;
		font-family: ${(props) => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		color: ${(props) => props.theme.colors.preto1};
	}

	img {
		display: block;
		margin-right: 16.1px;
	}
`;

export const ContainerHeader = styled.section`
	padding-top: 45px;
	.container {
		display: flex;
		align-items: flex-start;
		${Container} {
			margin-bottom: 38px;
			h2 {
				margin-bottom: 5px;
			}
		}

		.container-tarifa {
			display: flex;
			justify-content: center;
			font-family: ${(props) => props.theme.fonts.poppins};
			width: 653px;
			margin-left: auto;
			span {
				color: ${(props) => props.theme.colors.preto2};
				font-style: normal;
				font-weight: 500;
				font-size: 16px;
				line-height: 100%;
				display: flex;
				align-items: center;
				position: relative;
				&:before {
					content: "";
					display: block;
					width: 16px;
					height: 15px;
					position: absolute;
					top: 49%;
					left: -30px;
					transform: translateY(-50%);
					-webkit-mask: url(${estrela});
					background-color: #595c76;
				}
				&:after {
					content: "";
					display: block;
					width: 23.4px;
					height: 13.8px;
					position: absolute;
					top: 50%;
					right: -46px;
					transform: translateY(-50%);
					-webkit-mask: url(${setadir});
					background-color: #595c76;
				}
			}

			h3 {
				color: ${(props) => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 28px;
				line-height: 100%;
				display: flex;
				align-items: center;
				margin-left: 77px;
			}

			${ButtonDefault} {
				margin-left: 26px;
			}
		}
	}
`;

export const Etapas = styled.div`
	display: flex;
	margin-left: auto;
	cursor: pointer;
	position: relative;
	width: 798px;
	.container-inf-user {
		display: flex;
		align-items: center;
		span {
			color: ${(props) => props.theme.colors.preto2};
			font-family: ${(props) => props.theme.fonts.inter};
			font-style: normal;
			font-weight: 600;
			font-size: 12px;
			display: block;
			margin-left: 19px;
		}
	}
	.container-etapas {
		display: flex;
		margin-left: auto;
		span {
			display: flex;
			justify-content: center;
			align-items: center;
			width: 24px;
			height: 24px;
			background-color: ${(props) => props.theme.colors.gray4};
			border-radius: 50%;
			color: ${(props) => props.theme.colors.color01};
			font-family: ${(props) => props.theme.fonts.inter};
			font-style: normal;
			font-weight: 400;
			font-size: 12px;
			line-height: 100%;
			margin-right: 81px;
			position: relative;
			&:nth-child(2) {
				&.active {
					&:after {
						width: 105px;
						left: -202%;
					}
				}
			}
			&:nth-child(3) {
				&.active {
					&:after {
						width: 105px;
						left: -202%;
					}
				}
			}
			&.active {
				background-color: ${(props) => props.theme.colors.verde1};
				&:before {
					background-color: ${(props) => props.theme.colors.verde1};
				}

				&:after {
					content: "";
					display: block;
					width: 86px;
					height: 2px;
					position: absolute;
					bottom: -29px;
					left: -119%;
					transform: translateX(-3%);
					background-color: ${(props) => props.theme.colors.verde1};
				}
			}
			&:before {
				content: "";
				display: block;
				width: 6px;
				height: 10px;
				position: absolute;
				top: 50%;
				right: -37px;
				transform: translateY(-50%);
				-webkit-mask: url(${setadirCadastro});
				background-color: ${(props) => props.theme.colors.gray4};
			}
			&:last-child {
				margin-right: 0px;
				&:before {
					display: none;
				}
			}
		}
	}
`;
