import styled from 'styled-components';

export const Listhotel = styled.section`
	height: 193px;
	width: 100%;
	display: flex;
	align-items: center;
	background-color: ${(props) => props.theme.colors.color01};
	.container{
		h1{
			font-family: ${(props) => props.theme.fonts.poppins};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;	
			margin-bottom: 22px;
		}
	}
`;


export const ContainerSelect = styled.div`
	height: 56px;
	background: ${(props) => props.theme.colors.color02};
	border: 1px solid ${(props) => props.theme.colors.color03};
	box-sizing: border-box;
	border-radius: 4px;
	display: flex;
`