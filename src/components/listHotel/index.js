import React, { useState, useEffect } from 'react';

import Select from '../../components/Select';
import { Listhotel, ContainerSelect } from './styles'
import Calendario from '../calendario'
import ContainerAcomodacao from '../SelectAcomodacao'
import BtnBusca from '../btns/btnBusca'


export default function ListHotel() {
	const [change, setChanger] = useState({});
	const [people, setPeople] = useState({});

	const options = [
		{ value: 'chocolate', label: 'Chocolate' },
		{ value: 'strawberry', label: 'Strawberry' },
		{ value: 'vanilla', label: 'Vanilla' }
	]



	return (
		<Listhotel>
			<div className="container">
				<h1>60 hotéis em 25 destinos em todo o Brasil!</h1>

				<ContainerSelect>
					<Select change={setChanger} options={options} />

					<Calendario />

					<ContainerAcomodacao />

					<BtnBusca />
				</ContainerSelect>
			</div>
		</Listhotel>
	);
}
