import React from 'react';

import { ContainerConfirmacao } from './styles';
import Confirmacao from './confirmacao'
import CriacaoSenha from './criacaoSenha'

export default function CadastroConfirmacao() {
	return (
		<ContainerConfirmacao>
			<div className="container">
				<Confirmacao />
				
				<CriacaoSenha />
			</div>
		</ContainerConfirmacao>
	);
}
