import styled from 'styled-components';
import chicara from '../../assets/quarto/quarto-05.svg'
import check from '../../assets/cadastro/cadastro-12.svg'
export const ContainerConfirmacao = styled.section`
	padding-top: 33px;
	margin-bottom: 86px;
	.container{
		display: flex;
		align-items: start;
	}
`;

export const ContainerDados = styled.div`
	width: 902px;
	border: 1px solid #E0E0E0;
	border-radius: 4px;
	padding-bottom: 57px;

	a{
		margin-top: 42px;
		font-family: ${props => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 160%;	
		display: flex;
		align-items: center;
		color: ${props => props.theme.colors.blue1};
		img{
			display: block;
			margin-right: 19px;
		}
	}

	span{
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto2};
		font-style: normal;
		font-weight: normal;
		font-size: 14px;
		line-height: 100%;	
		&.verde{
			color: ${props => props.theme.colors.verde1};
		}
	}

	.container-header{
		border-bottom: 1px solid #E0E0E0;	
		height: 67px;
		display: flex;
		align-items: center;
		justify-content: center;
		h3{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;	
		}
	}
	.container-dados{
		width: 579px;
		margin-left: auto;
		margin-right: auto;
		.container-agradecimento{
			padding: 32px 0px;
			border-bottom: 1px solid #E0E0E0;	
			h2{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.verde1};				
				font-style: normal;
				font-weight: 600;
				font-size: 24px;
				line-height: 100%;	
				margin-bottom: 8px;
			}
		}

		.container-numero-confirmacao{
			display: flex;
			padding: 24px 0px;
			border-bottom: 1px solid #E0E0E0;	
			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.verde1};	
				font-style: normal;
				font-weight: bold;
				font-size: 16px;
				line-height: 100%;	
				&:first-child{
					margin-right: 54px;
				}
			}	
		}

		.container-diaria{
			display: flex;
			padding: 25px 0px;
			width: 290px;
			border-bottom: 1px solid #E0E0E0;
			position: relative;
			&:before{
				content: '';
				display: block;
				width: 4px;
				height: 4px;
				border-radius: 50%;
				background-color: ${props => props.theme.colors.verde1};
				position: absolute;
				bottom:	-1px;
				transform: translateY(1.5px);
				left: 0px;
			}
			&:after{
				content: '';
				display: block;
				width: 4px;
				height: 4px;
				border-radius: 50%;
				background-color: ${props => props.theme.colors.verde1};
				position: absolute;
				bottom:	-1px;
				transform: translateY(1.5px);
				right: 0px;
			}
			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;	
				display: flex;
				align-items: center;
				&:first-child{
					margin-right: 101px;	
				}

				img{
					display: block;
					margin-right: 8px;
				}
			}	
		}

		.container-entrada-saida{
			display: grid;
	    	grid-template-columns: repeat(2, 1fr);
			padding-top: 17px;
			padding-bottom: 24px;
			border-bottom: 1px solid #E0E0E0;
			.container-entrada, .container-saida{
				h4{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.verde1};
					font-style: normal;
					font-weight: 600;
					font-size: 14px;
					line-height: 100%;	
					margin-bottom: 12px;
				}

				p{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.preto1};
					font-style: normal;
					font-weight: 600;
					font-size: 16px;
					line-height: 100%;
					margin-bottom: 8px;	
				}
			}
			.container-saida{
				margin-left: 72px;	
			}
		}

		.container-hotel{
			display: flex;
			padding-top: 32px;
			padding-bottom: 56px;
			border-bottom: 1px solid #E0E0E0;
			.container-img{
				margin-right: 28px;
				img{
					width: 179px;
					height: 123px;
					border-radius: 4px;	
				}
			}

			.container-text{
				margin-top: 5px;
				h3{
					font-family: ${props => props.theme.fonts.poppins};
					color: ${props => props.theme.colors.preto1};
					font-style: normal;
					font-weight: bold;
					font-size: 16px;
					line-height: 130%;	
				}

				span{
					&.verde{
						display: block;
						margin-top: 13px;
						margin-bottom: 18px;	
					}
				}

				p{
					font-family: ${props => props.theme.fonts.inter};
					color: ${props => props.theme.colors.preto2};
					font-style: normal;
					font-weight: bold;
					font-size: 14px;
					line-height: 160%;
					margin-top: 16px;
					margin-bottom: 11px;	
				}

				a{
					margin-top: 24px;	
				}
			}
		}

		.quartos{
			padding: 32px 0px;
			border-bottom: 1px solid #E0E0E0;
			h2{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.verde1};				
				font-style: normal;
				font-weight: 600;
				font-size: 24px;
				line-height: 100%;	
				margin-bottom: 8px;
			}

			.container-img-text{
				display: flex;
				padding-top: 32px;
				.container-img{
					margin-right: 32px;
					p{
						font-family: ${props => props.theme.fonts.poppins};
						padding-top: 24px;
						font-style: normal;
						font-weight: 600;
						font-size: 13px;
						line-height: 100%;	
						color: #2F80ED;
						display: flex;
						align-items: center;
						justify-content: center;
						img{
							display: block;
							margin-right: 9px;	
						}
					}
				}

				.container-text{
					h3{
						font-family: ${props => props.theme.fonts.poppins};
						color: ${props => props.theme.colors.preto1};	
						font-style: normal;
						font-weight: 600;
						font-size: 16px;
						line-height: 100%;
						margin-bottom: 10px;
						&.valor{
							font-size: 18px;	
							font-family: ${props => props.theme.fonts.inter};
							color: ${props => props.theme.colors.preto1};	
							display: flex;
							align-items: center;
							margin-top: 16px;
							margin-bottom: 16px;
							p{
								font-family: ${props => props.theme.fonts.poppins};
								font-style: normal;
								font-weight: 600;
								font-size: 22px;
								line-height: 100%;
								margin-left: 75px;
								position: relative;
								&:before{
									content: '';
									display: block;
									width: 27px;
									height: 1px;
									background-color: #132847;
									position: absolute;
									top: 50%;
									left: -51px;
									transform: translateY(-50%);
								}
							}
						}	
					}

					h5{
						font-family: ${props => props.theme.fonts.poppins};
						color: ${props => props.theme.colors.verde1};
						font-style: normal;
						font-weight: 500;
						font-size: 13px;
						margin-bottom: 23px;
						position: relative;
						padding-left: 30px;
						&.sem-cafe{
							color: ${props => props.theme.colors.red};	
							&:before{
								background-color: ${props => props.theme.colors.red};	
							}
						}
						&:before{
							content: '';
							display: block;
							width: 20px;
							height: 19px;
							position: absolute;
							top: 50%;
							left: 0px;
							transform: translateY(-50%);
							-webkit-mask: url(${chicara});
							background-color: #409A3C;
						}
					}

					ul{
						display: grid;
						grid-template-columns: repeat(2, 1fr);
						grid-gap: 19px 46px;
						li{
							padding-left: 28px;	
							position: relative;
							font-family: ${props => props.theme.fonts.poppins};
							color: #515254;
							font-style: normal;
							font-weight: normal;
							font-size: 13px;
							line-height: 100%;
							&:before{
								content: '';
								display: block;
								width: 15px;
								height: 16px;
								position: absolute;
								top: 50%;
								left: 0px;
								transform: translateY(-50%);
								-webkit-mask: url(${check});
								background-color: #595C76;
							}
						}
					}

				}
			}
		}


		.valor-total{
			font-family: ${props => props.theme.fonts.inter};
			display: flex;
			padding: 24px 0px;
			border-bottom: 1px solid #E0E0E0;
			h3{
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: bold;
				font-size: 16px;
				line-height: 100%;	
				&:first-child{
					margin-right: 187px;
				}
			}
		}

		.contato{
			padding: 31px 0px;
			border-bottom: 1px solid #E0E0E0;
			padding-left: 20px;
			span{
				display: block;
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.preto2};	
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 160%;
				&.link{
					display: flex;
					align-items: center;
					a{
						margin-top: 0px;
						margin-left: auto;
						img{
							margin-left: 17px;
						}
					}
				}
			}
		}

		.text{
			padding-top: 32px;
			padding-bottom: 57px;
			span{
				display: block;
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.preto2};	
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 160%;
				strong{
					font-weight: bold;
					a{
						color: #2F80ED;
						margin-top: 0;
						display: inline;	
						font-weight: bold;
					}
				}
			}
		}
	}
`	

export const ContainerSenha = styled.div`
	border: 1px solid #E0E0E0;
	width: 278px;
	margin-left: auto;
	padding-bottom: 59px;
	a{
		font-family: ${props => props.theme.fonts.inter};
		font-style: normal;
		font-weight: 500;
		font-size: 14px;			
		display: flex;
		align-items: center;
		color: ${props => props.theme.colors.blue1};
		&.cancelar{
			color: #EB5757;	
		}
		img{
			display: block;
			margin-right: 19px;
		}
	}
	h4{
		font-family: ${props => props.theme.fonts.poppins};
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 600;
		font-size: 15px;
		line-height: 100%;
	}
	.container-senha{
		background: #F2F2F2;
		padding-top: 45px;
		padding-bottom: 51px;	
		padding-left: 62px;
		span{
			width: 192px;
			display: block;
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 150%;
		}
	}


	.container-verficacao{
		width: 206px;
		margin-left: auto;
		margin-right: auto;
		padding-top: 36px;
		h4{
			margin-bottom: 8px;	
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 150%;
			display: block;
			margin-bottom: 24px;
			&:last-child{
				margin-bottom: 0;
			}
		}
	}
`