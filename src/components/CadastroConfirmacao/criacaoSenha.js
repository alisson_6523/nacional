import React from 'react';
import { Link } from 'react-router-dom'

import { ContainerSenha } from './styles';

export default function CriacaoSenha() {
	return (
		<ContainerSenha>
			<div className="container-senha">
				<h4>Crie sua Senha</h4>	
				<span>Crie sua senha de forma rápida e prática para ter acesso a sua área.</span>
			</div>

			<div className="container-verficacao">
				<h4>Está tudo certo?</h4>
				<span>Você sempre pode ver ou alterar sua reserva online - não é preciso fazer um cadastro.</span>

				<Link to="/">Alterar datas</Link>
				<Link to="/">Atualizar cartão de crédito</Link>
				<Link to="/">Adicionar outro quarto</Link>
				<Link to="/">Ver políticas</Link>
				<Link to="/">Imprimir confirmação</Link>
				<br />

				<Link className="cancelar" to="/">Imprimir confirmação</Link>
				<br />
				<span>Esta reserva pode ser cancelada sem custo dentro das primeiras 24 horas após ter sido feita. Depois disso, a acomodação poderá cobrar uma taxa de cancelamento.</span>			</div>
		</ContainerSenha>
	);
}
