import React from 'react';
import { Link } from 'react-router-dom'

import { ContainerDados } from './styles';
import calendario from '../../assets/cadastro/cadastro-09.svg'
import pessoa from '../../assets/cadastro/cadastro-10.svg'
import setaDir from '../../assets/cadastro/cadastro-05.svg'
import ImgHotel from '../../assets/cadastro/cadastro-11.png'
import Camera from '../../assets/cadastro/cadastro-13.svg'
import Bandeiras from '../../assets/cadastro/cadastro-08.png'

export default function Confirmacao() {
	const cafe = true;

	return (
		<ContainerDados>
			<div className="container-header">
				<h3>Sua reserva em Poços de Caldas está confirmada</h3>
			</div>

			<div className="container-dados">

				<div className="container-agradecimento">
					<h2>Obrigado, André!</h2>
					<span>Abaixo você encontra os dados de sua reserva</span>
				</div>

				<div className="container-numero-confirmacao">
					<p><span>N. de Confirmação: </span>2401725779</p>
					<p><span>Código PIN: </span>6760</p>
				</div>

				<div className="container-diaria">
					<p><img src={calendario} alt=""/> 01 Díaria</p>
					<p><img src={pessoa} alt=""/> 02 Pessoas</p>
				</div>

				<div className="container-entrada-saida">
					<div className="container-entrada">
						<h4>Entrada</h4>

						<p>02 Outubro de 2019</p>

						<span>Quarta - Feira a partir de 16:00</span>

					</div>

					<div className="container-saida">
						<h4>Saída</h4>

						<p>02 Outubro de 2019</p>

						<span>Quinta - até  14:00</span>

					</div>

					<Link to="/"><img src={setaDir} alt="" /> Termos e Politica do Hotel</Link>
				</div>

				<div className="container-hotel">
					<div className="container-img">
						<img src={ImgHotel} alt=""/>
					</div>

					<div className="container-text">
						<h3>Hotel Nacional Inn Poços de Caldas</h3>

						<span className="verde">Poços de Caldas - MG</span>

						<span>Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil </span>

						<p>+55 35 3722 2253</p>

						<span>Coordenadas para GPS S 021° 47.187, W 46° 34.011</span>
						
						<Link to="/"><img src={setaDir} alt="" /> Termos e Politica do Hotel</Link>
					</div>
				</div>

				<div className="quartos">
					<h2>Quartos Escolhidos</h2>
					<span>Confira abaixo o que está incluído no seu quarto.</span>

					<div className="container-img-text">
						<div className="container-img">
							<img src={ImgHotel} alt=""/>

							<p><img src={Camera} alt=""/> Fotos</p>
						</div>

						<div className="container-text">
							<h3>Quarto Executivo Duplo</h3>

							<span>Hospede: André Xavier</span>

							<h3 className="valor" >01  <p>R$ 245,00</p></h3>

							<h5 className={`${cafe ? 'com-cafe': 'sem-cafe'}`}>{cafe ? 'Café da manhã incluso.' : 'Sem Café da manhã'}</h5>

							<ul>
								<li>1 Cama de Casal</li>
								<li>Frigobar</li>
								<li>Acomoda 2 pessoas</li>
								<li>Ar Condicionado</li>
								<li>Chuveiro</li>
								<li>Tv</li>
								<li>Telefone</li>
								<li>Ventilador</li>
							</ul>
						</div>
					</div>


				</div>
				
				<div className="valor-total">
					<h3>Valor total de sua Reserva</h3>
					<h3>R$ 545,00</h3>
				</div>

				<div className="contato">
					<span className="link">Enviamos seu e-mail de confirmação para <strong>andre@cryptos.com.br</strong> <Link to="/"><img src={setaDir} alt="" /> Alterar</Link> </span>
					<br />
					<span>O seu quarto atual é cancelamento grátis e o outro é não reembolsável, mas você pode conseguir mudar essas opções nas próximas 23 horas.</span>
					<br />
					<span>Você pode alterar sua reserva online quando quiser</span>
					<br />
					<span>O pré-pagamento é necessário para esta reserva. Todos os pagamentos são administrados por Hotel Dan Inn Poços de Caldas. Veja seus dados de pagamento para mais informações.</span>
				</div>

				<div className="text">
					<span><strong>O preço final exibido é o valor que você irá pagar à acomodação.</strong></span>
					<span>A Nacional Inn não cobra nenhuma taxa de reserva, administração ou para outros fins. O emissor do seu cartão pode cobrar uma taxa de transação internacional.</span>

					<br />

					<span><strong>Informação sobre pagamentos</strong></span>
					<span>A acomodação irá cobrar: R$ 122,40. Esta propriedade aceita as seguintes formas de pagamento:</span>

					<img src={Bandeiras} alt=""/>

					<br />
					<br />

					<span><strong>Outras informações</strong></span>
					<span>Por favor, observe que pedidos adicionais (por exemplo, cama extra) não estão incluídos neste valor. Impostos adicionais ainda poderão ser cobrados pela acomodação se você não comparecer ou cancelar.</span>
					<br />

					<span>Por favor, lembre-se de ler as Informações importantes abaixo, pois podem conter dados importantes que não foram mencionados aqui.</span>
					
					<br />

					<span><strong>Quer saber mais sobre pagamentos?</strong></span>
					<span><strong>Leia nossas <Link to="/">Perguntas Frequentes</Link> sobre como e quando pagar.</strong></span>

					<br />

					<span><strong>Antes de ir: dicas e regras da casa</strong></span>
					<span>Você escolheu um ótimo lugar para ficar durante a sua viagem. Você terá mais privacidade, mais espaço e uma casa fora de casa que é toda sua. Muito bem! Queremos ter certeza de que você está ciente de alguns detalhes antes de ir.</span>
					<br />

					<span><strong>Viajando com crianças ou pets?</strong></span>
					<span>Todas as crianças são bem-vindas. Por favor, verifique a Política de crianças e camas extras para saber onde os pequenos vão dormir.</span>
					
					<br />

					<span><strong>Animais de estimação: não permitidos.</strong></span>
					<span>Caso ainda tenha dúvidas ou alguma questão urgente, seu anfitrião terá prazer em ajudar você - basta entrar em contato diretamente com ele: +55 35 3722 2253.</span>

				</div>
			</div>
		</ContainerDados>
	);
}
