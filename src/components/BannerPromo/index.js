import React from 'react';

import { ContainerBannerPromo } from './styles';
import Banner from '../../assets/banners/banner-01.jpg'
import Banner2 from '../../assets/banners/banner-02.jpg'
import Banner3 from '../../assets/banners/banner-03.png'
import Banner4 from '../../assets/banners/banner-04.png'
import Banner5 from '../../assets/banners/banner-05.png'
import Banner6 from '../../assets/banners/banner-06.png'
import Banner7 from '../../assets/banners/banner-07.png'
import Banner8 from '../../assets/banners/banner-08.png'
import BannerQuemSomo from '../../assets/quemSomos/somos-02.png'

export default function BannerPromo({styledDefault = null}) {
	switch(styledDefault){
		case 'galeria':
			return(
				<ContainerBannerPromo styledDefault={'detalhe'} >
					<div className="container galeria">
						<img className="span" src={Banner2} alt="" />
						<img src={Banner3} alt="" />
						<img src={Banner4} alt="" />
						<img src={Banner5} alt="" />
						<img src={Banner6} alt="" />
						<img src={Banner7} alt="" />
						<img src={Banner8} alt="" />
					</div>
				</ContainerBannerPromo>	
			)
				
		case 'detalhe':
			return (
				<ContainerBannerPromo styledDefault={'galeria'}>
					<div className="container">
						<img className="banner" src={Banner} alt="" />
					</div>
				</ContainerBannerPromo>
			)

		case 'quem-somos':
			return (
				<ContainerBannerPromo>
					<div className="container">
						<img className="banner" src={BannerQuemSomo} alt="" />
					</div>
				</ContainerBannerPromo>
			)

		default:
			return (
				<ContainerBannerPromo>
					<div className="container">
						<img className="banner" src={Banner} alt="" />
					</div>
				</ContainerBannerPromo>
			)
	}
}
