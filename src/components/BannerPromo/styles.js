import styled, { css } from 'styled-components';

export const galeria =  css`
	margin-bottom: 0px;
`
export const detalhe =  css`
	margin-bottom: 73px;
`

export const ContainerBannerPromo = styled.section`
	margin-bottom: 89px;

	${props => props.styledDefault === 'galeria' ? galeria : null}
	${props => props.styledDefault === 'detalhe' ? detalhe : null}

	.container{
		&.galeria{
			display: grid;
			grid-template-columns: repeat(5, 1fr);
			grid-gap: 3px;
			img{
				width: 100%;
				object-fit: cover;
				&.span{
					grid-column: span 2;
    				grid-row: span 2;
					width: 100%;
					height: 100%;
				}
			}
		}
		img{
			&.banner{
				width: 100%;
				object-fit: contain;
				border-radius: 8px;
			}
		}
	}
`;
