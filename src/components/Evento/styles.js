import styled from 'styled-components';

export const ContainerEvento = styled.section`
	margin-bottom: 150px;
	position: relative;
	&:before{
		content: '';
		display: block;
		width: 100%;
		height: 1px;
		background-color: #E0E0E0;
		position: absolute;
		top: 472px;
		left: 0px;
	}
	.container{
		display: grid;
		grid-template-columns: repeat(2 ,1fr);
		grid-gap: 35px 0px;
		align-items: center;
		span{
			color: ${props => props.theme.colors.preto2};
			font-family: ${props => props.theme.fonts.poppins};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			display: block;
			line-height: 150%;
			margin-bottom: 24px;
		}

		.container-text{
			font-family: ${props => props.theme.fonts.poppins};
			width: 415px;
			margin-left: 87px;
			h4{
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: 800;
				font-size: 16px;
				line-height: 100%;	
				margin-bottom: 32px;
			}

			h1{
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 50px;
				line-height: 100%;	
				margin-bottom: 40px;
			}
		}

		.container-infs{
			grid-column: span 2;
			ul{
				display: flex;
				justify-content: space-between;
				li{
					margin-right: 40px;
					span{
						display: flex;
						align-items: flex-start;
						margin-bottom: 0px;
						line-height: normal;
						font-family: ${props => props.theme.fonts.inter};
						img{
							display: block;
							margin-right: 7px;
							margin-top: 4px;
						}
					}
					&:last-child{
						margin-right: 0px;
					}
				}
			}
		}
	}
`;
