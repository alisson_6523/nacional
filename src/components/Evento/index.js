import React from 'react';

import { ContainerEvento } from './styles';
import bannerEvento from '../../assets/evento/evento-05.png'
import Filtro from '../../components/btns/Filtro'
import check from '../../assets/CardReserva/reserva-02.svg'

export default function Evento() {
	return (
		<ContainerEvento>
			<div className="container">
				<div className="container-img">
					<img src={bannerEvento} alt="" />
				</div>

				<div className="container-text">
					<h4>NACIONAL INN HOTÉIS</h4>

					<h1>Excelência para o seu evento</h1>

					<span>A Nacional Inn Hóteis dispõe de amplos espaços para a promoção de eventos em diversos destino brasileiros.</span>


					<Filtro />
				</div>

				<div className="container-infs">
					<ul>
						<li>
							<span>
								<img src={check} alt="" />
								Mais de 170 salas <br /> em 23 destinos
							</span>
						</li>
						<li>
							<span>
								<img src={check} alt="" />
								Condições especiais <br /> para a sua empresa
							</span>
						</li>
						<li>
							<span>
								<img src={check} alt="" />
								Serviços de <br /> alta qualidade
							</span>
						</li>
						<li>
							<span>
								<img src={check} alt="" />
								Equipe preparada para auxiliá-lo <br /> no processo de organização
							</span>
						</li>
					</ul>
				</div>
			</div>
		</ContainerEvento>
	);
}
