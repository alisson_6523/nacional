import styled from 'styled-components';

export const Container = styled.div`
	.container-checks{
		&:first-child{
			margin-top: 20px;
		}
		margin-top: 15px;
		.pretty{
			.state{
				label{
					margin-left: 10px;
				}
			}
			input{
				&:checked{
					background-color: red;
					& ~ .state{
						&.p-success{
							
							label{
								&:after{
									background-color: transparent !important;
								}
							}
						}
					}
				}
			}
		}
	}	
`;
