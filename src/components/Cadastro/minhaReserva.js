import React from 'react'
import { Link } from 'react-router-dom'
import { Reserva } from './styles'

import close from '../../assets/quarto/quarto-06.svg'
import fotos from '../../assets/cadastro/cadastro-01.png'
import fotoHotel from '../../assets/cadastro/cadastro-02.jpg'
import calendario from '../../assets/cadastro/cadastro-03.svg'
import pessoa from '../../assets/cadastro/cadastro-04.svg'
import setaDir from '../../assets/cadastro/cadastro-05.svg'

export default function MinhaReserva() {
	return (
		<Reserva>

			<div className="header-minha-reserva">
				<h3>Minha Reserva</h3>
			</div>

			<div className="container-descricao">
				<img src={fotoHotel} alt="" />

				<h2>Thermas Resort Walter World</h2>

				<span>Poços de Caldas - MG</span>

				<p>Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil</p>
			</div>

			<div className="data-and-inf">

				<div className="data-pessoas">
					<span><img src={calendario} alt="" /> 01 Díaria</span>
					<span><img src={pessoa} alt="" /> 02 Pessoas</span>
				</div>

			</div>

			<div className="container-entrada-saida">
				<div className="content">
					<div className="entrada-saida">
						<span>Entrada</span>
						<h3>02 Outubro de 2019</h3>
						<p>Quarta - Feira a partir de 16:00</p>
					</div>

					<div className="entrada-saida">
						<span>Entrada</span>
						<h3>02 Outubro de 2019</h3>
						<p>Quinta - até  14:00</p>
					</div>
				</div>

				<Link to="/">Termos e Politica do Hotel <img src={setaDir} alt="" /></Link>
			</div>

			<div className="executivo">
				<h4>Executivo Duplo</h4>

				<span>Tarifa não reembolsável</span>

				<div className="parcelas">
					<p>01</p>

					<h4>R$ 245,00</h4>

					<img src={close} alt="" />
				</div>

				<div className="incluso">
					<p>Café da manhã incluso.</p>
					<span>1 Cama de Casal</span>
					<span>Acomoda 2 pessoas</span>
				</div>

				<div className="fotos">
					<img src={fotos} alt="" />
				</div>
			</div>

			<div className="subTotal">
				<p>Sub Total</p>
				<p>R$ 245,00</p>
			</div>

			<div className="subTotal">
				<p>5 % ISS</p>
				<p>R$ 245,00</p>
			</div>

			<div className="total">
				<p>Total</p>
				<p>R$ 245,00</p>
			</div>

			<div className="text">
				<span>Se você cancelar, terá que pagar R$ 179,28</span>
			</div>

		</Reserva>
	);
}
