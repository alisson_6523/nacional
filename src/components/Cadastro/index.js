import React from 'react';

import { ContainerCadastro } from './styles';
import MinhaRserva from './minhaReserva'
import SeusDados from './seusDados'

export default function Cadastro() {
	return (
		<ContainerCadastro>
			<div className="container">
				<MinhaRserva />

				<SeusDados />
			</div>
		</ContainerCadastro>
	);
}
