import React, { useState } from 'react'
import { Formik } from 'formik'
import { useHistory } from "react-router-dom"
import { useDispatch } from 'react-redux'

import FormCadastro from '../../components/Forms/cadastro'
import Login from '../../components/Forms/login'
import { ContainerSeusDados } from './styles'
import { FormInitialState, validationDados, validationLogin } from '../../util/configForm'
import { Creators as LoginActions } from '../../store/ducks/login'

export default function SeusDados(){
	const [checksHosped, setChecksHosped] = useState({})
	const [checksTerceiro, setChecksTerceiro] = useState({})
	const [cadastrar, setCadastrar] = useState(true)
	const dispatch = useDispatch()
	const history = useHistory()

	function toggleForms() {
		if (cadastrar) {
			return (
				<Formik
					enableReinitialize
					initialValues={FormInitialState.login}
					onSubmit={(value, actions) => {
						dispatch(LoginActions.requestLogin({ value, history }))
					}}
					render={props =>
						<Login cadastrar={setCadastrar} {...props} />
					}
					validationSchema={validationLogin}
				/>
			)
		} else {
			return (
				<Formik
					enableReinitialize
					initialValues={FormInitialState.cadastroDados}
					onSubmit={(value, actions) => {
						value = { ...value, checksHosped, checksTerceiro }
						
						const { nome, senha: password , eMail: email } = value

						dispatch(LoginActions.requestCreateUser({value: { nome, password, email }, history}))
						// console.log(value, actions)
					}}

					render={props =>
						<FormCadastro checksHosped={setChecksHosped} checksTerceiro={setChecksTerceiro} {...props} />
					}

					validationSchema={validationDados}
				/>
			)
		}
	}

	return (
		<ContainerSeusDados>
			<div className="header-meus-dados">
				<h3>Insira seus dados</h3>
			</div>

			<div className="container-dados">
				{toggleForms()}
			</div>
		</ContainerSeusDados>
	);
}