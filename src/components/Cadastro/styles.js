import styled from 'styled-components'
import chicara from '../../assets/quarto/quarto-05.svg'
import check from '../../assets/quarto/quarto-04.svg'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const ContainerCadastro = styled.section`
	padding-top: 33px;
	margin-bottom: 86px;
	.container{
		display: flex;
	}
`;

export const Reserva = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	width: 382px;
	.header-minha-reserva{
		display: flex;
		align-items: center;
		height: 71px;
		padding-left: 41px;
		h3{
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;	
		}
	}

	.container-descricao{
		padding-left: 41px;
		padding-bottom: 32px;
		padding-top: 21px;
		border-top: 1px solid #E0E0E0;
		border-bottom: 1px solid #E0E0E0;
		img{
			display: block;
			border-radius: 4px;
		}

		h2{
			margin-top: 24px;
			width: 298px;
			font-style: normal;
			font-weight: bold;
			font-size: 22px;
			line-height: 130%;	
			color: ${props => props.theme.colors.preto1};
			margin-bottom: 16px;
		}

		span{
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
			color: ${props => props.theme.colors.verde1};
		}

		p{
			margin-top: 19px;
			width: 290px;
			font-family: ${props => props.theme.fonts.inter};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 160%;	
		}
	}

	.data-and-inf{
		padding-left: 44px;
		padding-top: 32px;
		margin-bottom: 42px;
		.data-pessoas{
			display: flex;
			span{
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;	
				color: ${props => props.theme.colors.preto1};
				display: flex;
				align-items: center;
				margin-right: 35px;
				img{
					display: block;
					margin-right: 8px;	
				}
			}
		}
	}

	.container-entrada-saida{
		padding-left: 44px;
		padding-bottom: 27px;
		.content{
			position: relative;
			&:before{
				content: '';
				display: block;
				background-color: rgba(89, 92, 118, 0.3);
				width: 1px;
				height: calc(100% - 71px);
				position: absolute;
				top: 11px;
				left: 0px;
			}

			.entrada-saida{
				margin-bottom: 24px;
				padding-left: 23px;
				&:last-child{
					margin-bottom: 0px;
				}
				span{
					font-style: normal;
					font-weight: 600;
					font-size: 14px;
					line-height: 100%;
					color: ${props => props.theme.colors.verde1};	
					position: relative;
					&:before{
						content: '';
						display: block;
						width: 4px;
						height: 4px;
						border-radius: 50%;
						background-color: ${props => props.theme.colors.verde1};
						position: absolute;
						top: 50%;
						left: -24.51px;
						transform: translate(0.5px, -50%);
					}
				}

				h3{
					font-style: normal;
					font-weight: 600;
					font-size: 16px;
					line-height: 100%;
					color: ${props => props.theme.colors.preto1};	
					margin-top: 12px;
					margin-bottom: 8px;
				}

				p{
					font-style: normal;
					font-weight: normal;
					font-size: 14px;
					line-height: 100%;
					color: ${props => props.theme.colors.preto2};	
				}
			}
		}
		

		a{
			margin-top: 42px;
			font-family: ${props => props.theme.fonts.inter};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 160%;	
			display: flex;
			align-items: center;
			color: ${props => props.theme.colors.blue1};
			img{
				display: block;
				margin-left: 19px;
			}
		}
	}

	.executivo{
		padding-left: 41px;
		background: rgba(89, 92, 118, 0.05);
		padding-top: 35px;
		padding-right: 38px;
		padding-bottom: 42px;
		margin: 0px 3px;
		h4{
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			margin-bottom: 16px;
		}	

		span{
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 100%;
		}

		.parcelas{
			display: flex;
			margin-top: 24px;
			margin-bottom: 25px;
			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 18px;
				line-height: 100%;	
				margin-right: 77px;
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 27px;
					height: 1px;
					background-color: #132847;
					position: absolute;
					top: 50%;
					transform: translateY(-50%);
					right: -56px;
				}
			}

			h4{
				margin-bottom: 0px;
				font-size: 22px;
			}

			img{
				display: block;
				margin-left: auto;
				cursor: pointer;
			}
		}

		.incluso{
			p{
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: 500;
				font-size: 13px;
				line-height: 100%;
				margin-bottom: 23px;
				position: relative;
				padding-left: 30px;
				&.sem-cafe{
					color: ${props => props.theme.colors.red};	
					&:before{
						background-color: ${props => props.theme.colors.red};	
					}
				}
				&:before{
					content: '';
					display: block;
					width: 20px;
					height: 19px;
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${chicara});
					background-color: #409A3C;
				}
			}

			span{
				display: block;
				margin-bottom: 16px;
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;
				padding-left: 28px;
				color: ${props => props.theme.colors.preto2};
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 15px;
					height: 15px;
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${check});
					background-color: rgba(89, 92, 118, 0.6);
					z-index: 20;
				}
				&.red{
					color: ${props => props.theme.colors.red};
					&:before{
						background-color: ${props => props.theme.colors.red};
					}
				}
			}
		}

		.fotos{
			margin-top: 32px;
			margin-left: -12px;
		}
	}


	.subTotal{
		display: flex;
		justify-content: space-between;
		align-items: center;
		height: 69px;
		padding-left: 41px;
		padding-right: 38px;
		border-bottom: 1px solid #E0E0E0;
		&:last-child{
			border-bottom: none;
		}
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}
	}

	.cupom{
		display: flex;
		padding-left: 41px;
		padding-right: 38px;
		height: 103px;
		border-top: 1px solid #E0E0E0;
		border-bottom: 1px solid #E0E0E0;
		padding-top: 33px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}

		.container-input{
			display: flex;
			flex-direction: column;
			margin-left: auto;
			input{
				margin: 0px;
				padding: 0px;
				border: none;
				border-bottom: 1px solid ${props => props.theme.colors.verde1};
				width: 209px;
			}

			span{
				margin-top: 14px;
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;
			}
		}
	}

	.total{
		display: flex;
		justify-content: space-between;
		align-items: center;
		height: 69px;
		padding-left: 41px;
		padding-right: 38px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 100%;	
		}	
	}

	.text{
		height: 73px;
		padding-top: 33px;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		padding-bottom: 32px;
		border-top: 1px solid #E0E0E0;
		${ButtonDefault}{
			margin-bottom: 27px;
			button{
				width: 300px;
			}
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 100%;	
		}
	}
`

export const ContainerSeusDados = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	width: 800px;
	margin-left: auto;
	.header-meus-dados{
		display: flex;
		align-items: center;
		height: 71px;
		padding-left: 131px;
		border-bottom: 1px solid #E0E0E0;
		h3{
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;	
		}
	}
`