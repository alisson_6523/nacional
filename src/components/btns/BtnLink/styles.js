import styled from 'styled-components';

export const Container = styled.div`
  a{
	margin-top: 42px;
	font-family: ${props => props.theme.fonts.inter};
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	line-height: 160%;
	display: flex;
	align-items: center;
	color: ${props => props.theme.colors.blue1};
	img{
		display: block;
		margin-left: 19px;
	}
	}
`;
