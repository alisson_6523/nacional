import React from 'react';

import { Link } from 'react-router-dom'

import { Container } from './styles';
import setaDir from '../../../assets/cadastro/cadastro-05.svg'

export default function BtnLink({title = ''}) {
  return (
		<Container>
			<Link to="/">{title} <img src={setaDir} alt="" /></Link>
		</Container>
  );
}
