import styled from 'styled-components';

export const Container = styled.div`
	.container-controles{
		position: absolute;
		top: 50%;
		right:0px;
		transform: translateY(-50%);
		display: flex;
		.seta-dir, .seta-esq{
			width: 44px;
			height: 44px;
			border-radius: 50%;
			background-color: rgba(196, 196, 196, 0.3);
			display: flex;
			align-items: center;
			justify-content: center;
			cursor: pointer;	
		}

		.seta-dir{
			margin-left: 20px;
		}
	}
`;
