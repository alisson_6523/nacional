import React from 'react';
import setaEsq from '../../../assets/card/card-04.svg'
import setaDir from '../../../assets/card/card-05.svg'

import { Container } from './styles';

export default function Controles({ esq = '', dir = '' }) {
	return (
		<Container>
			<div className="container-controles">
				<div className="seta-esq" data-esq={esq}>
					<img src={setaEsq} alt="" />
				</div>

				<div className="seta-dir" data-dir={dir}>
					<img src={setaDir} alt="" />
				</div>
			</div>
		</Container>
	);
}
