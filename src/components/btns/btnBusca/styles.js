import styled from 'styled-components';
import busca from '../../../assets/header/header-11.svg';

export const Button = styled.button`
	width: 76px;
	height: 105%;
	background-color: ${(props) => props.theme.colors.verde1};
	border-radius: 0px 4px 4px 0px;
	position: relative;
	border: 0px solid transparent;
	top: -1px;
	&:before{
		content: '';
		display: block;
		width: 17px;
		height: 17px;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		-webkit-mask: url(${busca});
		background-color: #fff;
	}
`;

export const ButtonDefault = styled.div`
	a,button{
		font-family: ${props => props.theme.fonts.poppins};
		border: 1px solid ${props => props.cadastrar ? props.theme.colors.verde2 : "transparent"};
		color: ${props => props.cadastrar ? props.theme.colors.verde1 : "#fff"};
		font-style: normal;
		font-weight: 600;
		font-size: 20px;
		line-height: 100%;
		background-color: ${props => props.cadastrar ? "#fff" : props.theme.colors.verde1};
		border-radius: 4px;
		display: flex;
		align-items: center;
		justify-content: center;
		width: 280px;
		height: 63px;
	}
`
export const ButtonLogin = styled.button`
	background-color: rgba(89, 92, 118, 0.05);
	border-radius: 4px;
	width: 255px;
	height: 77px;
	display: flex;
	align-items: center;
	justify-content: center;

	font-family: ${props => props.theme.fonts.inter};
	line-height: 21px;
	color: #103A51;
	img{
		margin-right: 16px;
	}
`