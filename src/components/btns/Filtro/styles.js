import styled from 'styled-components';
import { Button } from '../btnBusca/styles'
export const Container = styled.div`
	width: 415px;
	height: 56px;
	border: 1px solid #DCDCDC;
	border-radius: 4px;
	position: relative;
	box-sizing: border-box;
	${Button}{
		position: absolute;
		top: -1px;
		right: 0px;
		
	}
	input{
		width: 100%;
		height: 100%;
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto1};
		padding-left: 37px;
		padding-right: 20px;
		font-style: normal;
		font-weight: 500;
		font-size: 14px;
		line-height: 100%;
		border: none;
		&::placeholder{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;	
		}
	}
`;
