import React from 'react';

import { Link } from 'react-router-dom'

import { ContainerViagem } from './styles';
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

import imgGrupo from '../../assets/grupo/grupo-01.png'
import imgGrupo1 from '../../assets/grupo/grupo-02.png'
import imgGrupo2 from '../../assets/grupo/grupo-03.png'
import imgGrupo3 from '../../assets/grupo/grupo-04.png'
import imgGrupo4 from '../../assets/grupo/grupo-05.png'
import imgGrupo5 from '../../assets/grupo/grupo-06.png'
import banner from '../../assets/grupo/grupo-07.png'

export default function ViagenGrupos() {
	return (
		<ContainerViagem>
			<div className="container">
				<div className="container-text">
					<h2>Viagem em Grupos</h2>

					<p>Viajar é sempre uma experiência magnífica! É a possibilidade de conhecer novos lugares, pessoas, costumes e culturas diversas. O passeio pode ser ainda mais divertido e prazeroso quando você está com muitas pessoas!</p>
					<br />
					<p>Isso sem contar que planejar uma viagem em grupo pode trazer muitos benefícios: novas amizades, segurança, divisão de custos e comodidade!</p>
					<br />
					<p>Já pensou em conhecer as belezas naturais de Foz do Iguaçu, no Paraná? Aproveitar o ar puro das montanhas de Poços de Caldas, em Minas Gerais? Você pode, ainda, ir para Campos do Jordão: a Suíça Brasileira!</p>
					<br />
					<p>E as opções não param por aí: Rio de Janeiro, Porto Alegre, Curitiba, Recife, Araxá, Belo Horizonte, São Paulo e Salvador!</p>

					<ButtonDefault>
						<Link to="/">Solicitar Orçamento</Link>
					</ButtonDefault>
				</div>


				<div className="container-galeria">
					<img src={imgGrupo} alt=""/>
					<img src={imgGrupo1} alt=""/>
					<img src={imgGrupo2} alt=""/>
					<img src={imgGrupo3} alt=""/>
					<img src={imgGrupo4} alt=""/>
					<img src={imgGrupo5} alt=""/>
				</div>
			</div>


			<div className="container banner-img">
				<img src={banner} alt=""/>
			</div>
		</ContainerViagem>
	);
}
