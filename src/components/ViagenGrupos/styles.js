import styled from 'styled-components';
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const ContainerViagem = styled.section`
	padding-top: 123px;
	padding-bottom: 120px;
	.container{
		display: flex;
		justify-content: center;
		&.banner-img{
			margin-top: 120px;	
		}
		
		.container-text{
			width: 416px;
			h2{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;	
				margin-bottom: 16px;
			}

			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 150%;
			}

			${ButtonDefault}{
				margin-top: 56px;	
			}
		}

		.container-galeria{
			display: grid;
			grid-template-columns: repeat(2, 1fr);
			width: 487px;
			margin-left: 104px;
		}
	}
`;
