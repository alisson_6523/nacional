import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'

import { Container } from './styles';
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export default function CardHotelStatus({change, infs}) {
	const [qtd, setQtd] = useState({tipo: '', qtd: 1})
	const { status, cafe, valor, tipo } = infs

	function contador(operador){
		if(operador === 'soma'){
			setQtd({tipo: tipo, qtd: (qtd.qtd + 1) })
		}else{
			if(qtd.qtd > 1){
				setQtd({tipo: tipo, qtd: (qtd.qtd - 1) })
			}
		}
	}


	useEffect(() => {
		change(qtd)
	},[change, qtd])


	return (
		<Container>
			<h4>{status}</h4>
			<p className={`${cafe ? '': 'sem-cafe'}`}>{cafe ? 'Café da manhã incluso.' : 'Sem Café da manhã'}</p>

			<div className="container-contador">
				<h3>R$ {valor}</h3>
				<ButtonDefault><Link to="/">Adicionar</Link></ButtonDefault>
			</div>
		</Container>
	);
}
