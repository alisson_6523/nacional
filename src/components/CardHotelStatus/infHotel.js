import React from 'react';
import fotoHotel from '../../assets/quarto/quarto-03.png'
import CheckBox from '../CheckBox'
import { ContainerImgHotel } from './styles'

export default function CardHotelStatus({options}) {
	return (
		<ContainerImgHotel>
			<div className="container-img">
				<img src={fotoHotel} alt=""/>
			</div>	

			<div className="container-itens">
				<span>Acomoda 2 pessoas</span>
				<span className="red">Restam só 3 quartos</span>
			</div>

			{options.map((title,key) => <CheckBox key={key} title={title.title}/>)}
		</ContainerImgHotel>
	);
}
