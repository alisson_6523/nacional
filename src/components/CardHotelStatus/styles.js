import styled from 'styled-components';
import chicara from '../../assets/quarto/quarto-05.svg'
import img from '../../assets/quarto/quarto-02.svg'
import check from '../../assets/quarto/quarto-04.svg'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const Container = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	padding: 30px 40px;
	height: 202px;
	width: 439px;
	margin-left: 24px;
	border-radius: 4px;
	margin-bottom: 13px;
	&:last-child{
		margin-bottom: 29px;
	}
	h4{
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 600;
		font-size: 18px;
		line-height: 100%;
		margin-bottom: 23px;
	}

	p{
		color: ${props => props.theme.colors.verde1};
		font-style: normal;
		font-weight: 500;
		font-size: 13px;
		line-height: 100%;
		margin-bottom: 23px;
		position: relative;
		padding-left: 30px;
		&.sem-cafe{
			color: ${props => props.theme.colors.red};
			&:before{
				background-color: ${props => props.theme.colors.red};
			}
		}
		&:before{
			content: '';
			display: block;
			width: 20px;
    		height: 19px;
			position: absolute;
			top: 50%;
			left: 0px;
			transform: translateY(-50%);
			-webkit-mask: url(${chicara});
			background-color: #409A3C;
		}
	}

	.container-contador{
		display: flex;
		justify-content: space-between;
		background-color: ${props => props.theme.colors.laranjinha};
		padding: 12px 20px;
		border-radius: 4px;
		h3{
			display: flex;
			align-items: center;
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
		}

		${ButtonDefault}{
			a{
				width: 171px;
				height: 42px;
				font-size: 16px;
			}
		}
		.contador{
			display:flex;
			align-items: center;
			background: ${props => props.theme.colors.gray5};
			border-radius: 4px;
			cursor: pointer;
			span{
				width: 43px;
				height: 43px;
				display: flex;
				align-items: center;
				text-align: center;
				justify-content: center;
				&.valor{
					width: 82px;
					border-right: 1px solid rgba(89, 92, 118, 0.2);
					border-left: 1px solid rgba(89, 92, 118, 0.2);
				}
			}
		}
	}
`;


export const ContainerImgHotel = styled.div`
	width: 283px;
	.container-img{
		position: relative;
		width: 100%;
		&:before{
			content: '';
			display: block;
			width: 45px;
			height: 45px;
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			background-image: url(${img});
			z-index: 30;
		}
	}

	.container-itens{
		margin-top: 26px;
		margin-left: 30px;
		border-bottom: 1px solid #E0E0E0;
		span{
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;
			display: block;
			color: ${props => props.theme.colors.preto1};
			position: relative;
			margin-bottom: 14px;
			&:last-child{
				margin-bottom: 27px;
			}
			&:before{
				content: '';
				display: block;
				width: 15px;
				height: 15px;
				position: absolute;
				top: 50%;
				left: -30px;
				transform: translateY(-50%);
				-webkit-mask: url(${check});
				background-color: ${props => props.theme.colors.preto1};
			}
			&.red{
				color: ${props => props.theme.colors.red};
				&:before{
					background-color: ${props => props.theme.colors.red};
				}
			}
		}
	}
`
