import React, { useState } from 'react';

import { ContainerAcomodacao } from './styles';

export default function SelectAcomodacao() {
	const [quarto, setQuarto] = useState(1)
	const [adultos, setAdultos] = useState(1)
	const [criancas, setcriancas] = useState(1)
	const [active, setActive] = useState(false)

	function handleActive(e){
		console.log(e.target.classList)

		if(e.target.classList.value.includes('container-itens')) {
			setActive(!active)
		}
	}

	return (
		<ContainerAcomodacao onClick={(e) => {handleActive(e)}}>  
			<div className="container-itens">
				<p> {quarto < 10 ? '0'+quarto: quarto} Quarto </p>
				<p> &nbsp;- {adultos < 10 ? '0'+adultos: adultos}  Adultos </p>
				<p> &nbsp;- {criancas < 10 ? '0'+criancas: criancas} Crianças</p>
			</div>


			<div className={`container-contador ${active ? 'active' : ''}`}>
				<div className="container-text">
					<p>Quartos</p>
					<div className="contador">
						<span onClick={() => {setQuarto(quarto > 1 ? quarto - 1:quarto )}}>-</span>
						<span className="valor">{quarto < 10 ? '0'+quarto: quarto}</span>
						<span onClick={() => {setQuarto(quarto + 1)}}>+</span>
					</div>
				</div>

				<div className="container-text">
					<p>Adultos</p>
					<div className="contador">
						<span onClick={() => {setAdultos(adultos > 1 ? adultos - 1: adultos )}}>-</span>
						<span className="valor">{adultos < 10 ? '0'+adultos: adultos}</span>
						<span onClick={() => {setAdultos(adultos + 1)}}>+</span>
					</div>
				</div>

				<div className="container-text">
					<p>Crianças</p>
					<div className="contador">
						<span onClick={() => {setcriancas(criancas > 1 ? criancas - 1: criancas )}}>-</span>
						<span className="valor">{criancas < 10 ? '0'+criancas: criancas}</span>
						<span onClick={() => {setcriancas(criancas + 1)}}>+</span>
					</div>
				</div>
			</div>
		</ContainerAcomodacao>
	);
}
