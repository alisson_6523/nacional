import styled, { css } from 'styled-components';
import setaBaixo from '../../assets/header/header-07.svg'
import usuers from '../../assets/header/header-10.svg'

export const ContainerAcomodacao = styled.div`
	display: flex;
	align-items: center;
	width: 437px;
	position: relative;
	cursor: pointer;

	&:after{
		content: '';
		display: block;
		width: 12px;
		height: 7px;
		position: absolute;
		top: 55%;
		right: 32px;
		transform: translateY(-50%);
		-webkit-mask: url(${setaBaixo});
		background-color: #8AC537;
	}
	&:before{
		content: '';
		display: block;
		width: 22px;
		height: 16px;
		position: absolute;
		top: 50%;
		left: 31px;
		transform: translateY(-50%);
		-webkit-mask: url(${usuers});
		background-color: #BDBDBD;
	}
	.container-itens{
		position: absolute;
		top: 50%;
		left: 0px;
		transform: translateY(-50%);
		display: flex;
		align-items: center;
		width: 100%;
		height: 100%;
		padding-left: 69px;
		p{
			font-family: ${(props) => props.theme.fonts.inter};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;	
			color: ${(props) => props.theme.colors.preto1};
			pointer-events: none;
		}
	}


	.container-contador{
		background-color: #fff;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		width: 100%;
		position: absolute;
		top: 66px;
		left: 0px;
		height: 0px;
		overflow: hidden;
		transition: height .5s;
		z-index: 30;
		&.active{
			height: 241px;
			padding: 40px 0px;
			transition: all .5s;
		}

		.container-text{
			display: flex;
			align-items: center;
			margin-bottom: 16px;
			&:last-child{
				margin-bottom: 0;
			}
			p{
				font-family: ${(props) => props.theme.fonts.poppins};
				font-style: normal;
				font-weight: 600;
				font-size: 13px;
				line-height: 180%;
				color: ${(props) => props.theme.colors.verde1};	
				width: 60px;
			}

			.contador{
				display:flex;
				align-items: center;
				background: rgba(89, 92, 118, 0.1);
				border-radius: 1px;
				cursor: pointer;
				margin-left: 33px;
				span{
					width: 43px;
					height: 43px;
					display: flex;
					align-items: center;
					text-align: center;
					justify-content: center;
					&:first-child{
						color: ${(props) => props.theme.colors.verde1};	
						font-weight: 600;
					}
					&:last-child{
						color: ${(props) => props.theme.colors.verde1};	
						font-weight: 600;
					}
					&.valor{
						width: 82px;	
						border-right: 1px solid rgba(89, 92, 118, 0.2);
						border-left: 1px solid rgba(89, 92, 118, 0.2);
						font-weight: bold;
						font-family: ${(props) => props.theme.fonts.poppins};
						font-style: normal;
						font-weight: 600;
						font-size: 13px;
						line-height: 180%;
					}
				}	
			}
		}
		
	}
`;

export const active = css`	
	
	
`