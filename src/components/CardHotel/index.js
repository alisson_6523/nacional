import React from 'react';
import { Link } from 'react-router-dom'

import { ContainerCardHotel } from './styles'
import hotel from '../../assets/busca/busca-02.png'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export default function CardHotel() {
	return (
		<ContainerCardHotel>
			<div className="container-img">
				<img src={hotel} alt=""/>
			</div>

			<div className="container-text">
				<h2>Thermas Resort Walter World</h2>
				<span>Poços de Caldas - MG</span>

				<p>Rua Barros Cobra, 35, Poços de Caldas, CEP 37701-018, Brasil</p>

				<ul>
					<li>Resort</li>
					<li>All Inclusive</li>
				</ul>

				<small>Cancelamento Grátuito Disponível</small>
			</div>

			<div className="container-disponiveis">
				<small>Restam 4 quartos</small>

				<div className="tarifa">
					<p>Tárifas</p>
					<p className="valor">R$ 245,00</p>
				</div>

				<ButtonDefault><Link to="/">Reservar Quartos</Link></ButtonDefault>
			</div>
		</ContainerCardHotel>
	);
}
