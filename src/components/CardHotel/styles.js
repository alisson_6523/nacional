import styled from 'styled-components';
import check from '../../assets/busca/busca-03.svg'
import setaDir from '../../assets/busca/busca-04.svg'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const ContainerCardHotel = styled.div`
	display: flex;
	padding: 32px 40px;
	background: ${props => props.theme.colors.color01};
	margin-bottom: 24px;
	&:last-child{
		margin-bottom: 0;
	}
	.container-text{
		margin-left: 43.88px;
		h2{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: bold;
			font-size: 22px;
			line-height: 100%;
			margin-bottom: 10px;
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}

		small{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 12px;
			line-height: 100%;
		}

		p{
			margin-top: 16px;
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}

		ul{
			display: grid;
			grid-template-columns: repeat(2, 1fr);
			grid-gap: 0px 36px;
			padding: 24px 0px;
			width: 250px;
			li{
				position: relative;
				padding-left: 23px;
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;
				&:before{
					content: '';
					display: block;
					width: 16px;
					height: 16px;
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${check});
					-webkit-mask-repeat: no-repeat;
					background-color: #409A3C;
				}
			}
		}
	}

	.container-disponiveis{
		margin-left: auto;
		width: 295px;
		small{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.red};
			font-style: normal;
			font-weight: 500;
			font-size: 14px;
			line-height: 100%;	
		}

		.tarifa{
			margin-top: 18px;
			border-radius: 4px;
			border: 1px solid ${props => props.theme.colors.verde2};
			width: 100%;
			height: 51px;
			padding-left: 51px;
			padding-right: 40px;
			display: flex;
			align-items: center;
			justify-content: space-between;
			position: relative;
			&:before{
				content: '';
				display: block;
				width: 17px;
				height: 10px;
				position: absolute;
				top: 50%;
				left: 48%;
				transform: translate(-50%, -50%);
				-webkit-mask: url(${setaDir});
				-webkit-mask-repeat: no-repeat;
				background-color: #595C76;
			}
			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 500;
				font-size: 14px;
				line-height: 100%;
				&.valor{
					font-weight: bold;
				}
			}	
		}

		${ButtonDefault}{
			margin-top: 8px;
			height: 61px;	
			a{
				width: 100%;
			}
		}
	}
`;
