import React from 'react';

import { ContainerCardSala } from './styles';
import hotel from '../../assets/sala/sala-01.png'
import check from '../../assets/CardReserva/reserva-02.svg'

export default function CardSala() {
	return (
		<ContainerCardSala>
			<div className="container-img">
				<img src={hotel} alt="" />
			</div>

			<div className="container-text">
				<h3>Nacional Inn</h3>

				<span><img src={check} alt="" /> 03 salas</span>

				<p>270 pessoas em auditório</p>
			</div>
		</ContainerCardSala>
	);
}
