import styled from 'styled-components';

export const ContainerSalaCidade = styled.section`
	padding-top: 109px;
	margin-bottom: 211px;
	.container{
		h1{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 32px;
			line-height: 100%;	
			margin-bottom: 70px;
		}

		.container-cards{
			display: grid;
			grid-template-columns: repeat(auto-fit, minmax(591px, 591px));
			grid-gap: 24px 34px;
		}
	}
`;

export const ContainerCardSala = styled.div`
	display: flex;
	align-items: center;
	width: 100%;
	padding: 20px 0px 18px 23px;
	border: 1px solid #E0E0E0;
	.container-img{
		img{
			border-radius: 4px;
			width: 194px;
			height: 145px;	
		}	
	}

	.container-text{
		margin-left: 39px;
		h3{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: bold;
			font-size: 18px;
			line-height: 100%;
			margin-bottom: 18px;	
		}

		span{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 100%;	
			display: flex;
			align-items: center;
			margin-bottom: 16px;
			img{
				display: block;
				margin-right: 7px;
			}
		}

		p{
			font-family: ${props => props.theme.fonts.inter};
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: bold;
			font-size: 13px;
			line-height: 100%;	
		}
	}
`
