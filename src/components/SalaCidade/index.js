import React from 'react';

import CardSala from './cardSala';
import { ContainerSalaCidade } from './styles';


export default function SalaCidade() {
	return (
		<ContainerSalaCidade>
			<div className="container">
				<h1>Salas em Poços de Caldas</h1>

				<div className="container-cards">
					<CardSala />
					<CardSala />
					<CardSala />
					<CardSala />
					<CardSala />
				</div>
			</div>
		</ContainerSalaCidade>
	);
}
