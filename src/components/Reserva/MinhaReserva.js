import React from 'react';
import { Link } from 'react-router-dom'
import { Reserva } from './styles'
import close from '../../assets/quarto/quarto-06.svg'
import { ButtonDefault } from '../btns/btnBusca/styles'

export default function MinhaReserva() {
	return (
		<Reserva>	
			
			<div className="header-minha-reserva">
				<h3>Minha Reserva</h3>
			</div>

			<div className="executivo">
				<h4>Executivo Duplo</h4>

				<span>Tarifa não reembolsável</span>

				<div className="parcelas">
					<p>01</p>

					<h4>R$ 245,00</h4>

					<img src={close} alt=""/>
				</div>

				<div className="incluso">
					<p>Café da manhã incluso.</p>
					<span>1 Cama de Casal</span>
					<span>Acomoda 2 pessoas</span>
				</div>
			</div>

			<div className="subTotal">
				<p>Sub Total</p>
				<p>R$ 245,00</p>
			</div>
		
			<div className="cupom">
				<p>Cupom</p>
				<div className="container-input">
					<input type="text"/>
					<span>Ver Meus Cupons</span>
				</div>	
			</div>

			<div className="total">
				<p>Total</p>
				<p>R$ 245,00</p>
			</div>

			<div className="reserva">
				<ButtonDefault>
					<Link to="/cadastro-dados">Reservar</Link>
				</ButtonDefault>

				<span>Impostos de taxas incluídas</span>
			</div>
		
		</Reserva>
	);
}
