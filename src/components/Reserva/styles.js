import styled from 'styled-components';
import { Container } from '../Titles/styles'
import chicara from '../../assets/quarto/quarto-05.svg'
import check from '../../assets/quarto/quarto-04.svg'
import { ButtonDefault } from '../../components/btns/btnBusca/styles'

export const ContainerReserva = styled.section`
	padding-top: 44px;
	border-bottom: 1px solid #DCDCDC;
	margin-bottom: 64px;
	${Container}{
		margin-bottom: 52px;
		grid-column: span 2;
	}

	.container{
		display: grid;
		grid-template-columns: repeat(2, 1fr);
		.container-selects{
			width: 800px;
			display: flex;
			height: 66px;
			background-color: ${props => props.theme.colors.color02};
			border: 1px solid ${props => props.theme.colors.color03};
			border-left: none;
			margin-bottom: 25px;
		}
	}
`;


export const Reserva = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	border: 1px solid #E0E0E0;
	width: 382px;
	margin-left: auto;
	.header-minha-reserva{
		display: flex;
		align-items: center;
		height: 71px;
		padding-left: 41px;
		h3{
			color: ${props => props.theme.colors.verde1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;	
		}
	}

	.executivo{
		padding-left: 41px;
		background: rgba(89, 92, 118, 0.05);
		padding-top: 35px;
		padding-right: 38px;
		padding-bottom: 42px;
		margin: 0px 3px;
		h4{
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: 600;
			font-size: 20px;
			line-height: 100%;
			margin-bottom: 16px;
		}	

		span{
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 100%;
		}

		.parcelas{
			display: flex;
			margin-top: 24px;
			margin-bottom: 25px;
			p{
				font-family: ${props => props.theme.fonts.inter};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 18px;
				line-height: 100%;	
				margin-right: 77px;
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 27px;
					height: 1px;
					background-color: #132847;
					position: absolute;
					top: 50%;
					transform: translateY(-50%);
					right: -56px;
				}
			}

			h4{
				margin-bottom: 0px;
				font-size: 22px;
			}

			img{
				display: block;
				margin-left: auto;
				cursor: pointer;
			}
		}

		.incluso{
			p{
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: 500;
				font-size: 13px;
				line-height: 100%;
				margin-bottom: 23px;
				position: relative;
				padding-left: 30px;
				&.sem-cafe{
					color: ${props => props.theme.colors.red};	
					&:before{
						background-color: ${props => props.theme.colors.red};	
					}
				}
				&:before{
					content: '';
					display: block;
					width: 20px;
					height: 19px;
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${chicara});
					background-color: #409A3C;
				}
			}

			span{
				display: block;
				margin-bottom: 16px;
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;
				padding-left: 28px;
				color: ${props => props.theme.colors.preto2};
				position: relative;
				&:before{
					content: '';
					display: block;
					width: 15px;
					height: 15px;
					position: absolute;
					top: 50%;
					left: 0px;
					transform: translateY(-50%);
					-webkit-mask: url(${check});
					background-color: rgba(89, 92, 118, 0.6);;
					z-index: 20;
				}
				&.red{
					color: ${props => props.theme.colors.red};
					&:before{
						background-color: ${props => props.theme.colors.red};
					}
				}
			}
		}
	}


	.subTotal{
		display: flex;
		justify-content: space-between;
		align-items: center;
		height: 69px;
		padding-left: 41px;
		padding-right: 38px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}
	}

	.cupom{
		display: flex;
		padding-left: 41px;
		padding-right: 38px;
		height: 103px;
		border-top: 1px solid #E0E0E0;
		border-bottom: 1px solid #E0E0E0;
		padding-top: 33px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-size: 14px;
			line-height: 100%;	
		}

		.container-input{
			display: flex;
			flex-direction: column;
			margin-left: auto;
			input{
				margin: 0px;
				padding: 0px;
				border: none;
				border-bottom: 1px solid ${props => props.theme.colors.verde1};
				width: 209px;
			}

			span{
				margin-top: 14px;
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.verde1};
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;
			}
		}
	}

	.total{
		display: flex;
		justify-content: space-between;
		align-items: center;
		height: 69px;
		padding-left: 41px;
		padding-right: 38px;
		p{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto1};
			font-style: normal;
			font-weight: normal;
			font-weight: 600;
			font-size: 18px;
			line-height: 100%;	
		}	
	}

	.reserva{
		height: 168px;
		padding-top: 33px;
		display: flex;
		align-items: center;
		justify-content: center;
		flex-direction: column;
		padding-bottom: 32px;
		border-top: 1px solid #E0E0E0;
		${ButtonDefault}{
			margin-bottom: 27px;
			button{
				width: 300px;
			}
		}

		span{
			font-family: ${props => props.theme.fonts.poppins};
			color: ${props => props.theme.colors.preto2};
			font-style: normal;
			font-weight: normal;
			font-size: 13px;
			line-height: 100%;	
		}
	}
`
