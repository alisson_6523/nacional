import React from 'react';

import { ContainerReserva } from './styles'
import Title from '../../components/Titles'
import Calendario from '../calendario'
import ContainerAcomodacao from '../SelectAcomodacao'
import CardQuartos from '../../components/CardQuarto'
import MinhaReserva from '../../components/Reserva/MinhaReserva'


export default function Reserva() {
	const title = "Reserve agora e garanta a melhor tarifa!"
	const subTitle = "Os preços podem subir em breve: Garanta já sua reserva e aproveite um ótimo preço"

	return (
		<ContainerReserva>
			<div className="container">
				<Title title={title} subTitle={subTitle}/>

				<div className="container-itens-inf-reserva">
					<div className="container-selects">
						<Calendario />	

						<ContainerAcomodacao />
					</div>

					<div className="container-quartos">
						<CardQuartos />
						<CardQuartos />
						<CardQuartos />
					</div>
				</div>


				<div className="minha-reserva">
					<MinhaReserva />
				</div>
			</div>
		</ContainerReserva>
	);
}
