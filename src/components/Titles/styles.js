import styled from 'styled-components';

export const Container = styled.div`
	font-family: ${props => props.theme.fonts.poppins};
	margin-bottom: 42px;
	h2{
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: 600;
		font-size: 32px;
		line-height: 100%;	
		margin-bottom: 12px;
	}

	span{
		color: ${props => props.theme.colors.gray3};
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		line-height: 100%;	
	}
`;
