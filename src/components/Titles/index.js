import React from 'react';

import { Container } from './styles';

export default function Titles({ title, subTitle }) {
	return (
		<Container>
			<h2>{title ? title : 'Pacotes com descontos incríveis '}</h2>
			<span>{subTitle ? subTitle : 'Aproveite para pagar menos com nossos pacotes, Aproveite!'}</span>
		</Container>
	);
}
