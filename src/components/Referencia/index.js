import React from 'react';

import { ContainerReferencia } from './styles';
import telasMobile from '../../assets/quemSomos/somos-16.png'


export default function Referencia() {
  return (
	<ContainerReferencia>
		<div className="container">
			<div className="container-text">
				<h3>Referência de mercado:</h3>

				<p>O processo de expansão é resultado de estratégia que contempla atuação em diferentes segmentos como lazer, corporativo e eventos.</p>
				<br />
				<p>Essas características aliadas à alta capilaridade conferem à empresa maior penetração no mercado.</p>
				<br />
				<p>A Nacional Inn Hotéis, atenta às mudanças do setor, investe na frente digital com site e aplicativo e mantém seu foco na experiência do hóspede oferecendo em todas as unidades, como cortesia, café da manhã e internet Wi-Fi.</p>
				<img src={telasMobile} alt=""/>
			</div>
		</div>
	</ContainerReferencia>
  );
}
