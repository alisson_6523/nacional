import styled from 'styled-components';
import mobile from '../../assets/quemSomos/somos-15.png'

export const ContainerReferencia = styled.section`
	background-image: url(${mobile});
	background-repeat: no-repeat;
	margin-bottom: 182px;
	.container{
		.container-text{
			width: 418px;
			margin-left: auto;
			margin-right: 206px;
			h3{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 32px;
				line-height: 100%;
				margin-bottom: 16px;	
			}

			p{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 14px;
				line-height: 150%;	
			}

			img{
				margin-left: -167px;
			}
		}
	}
`;
