import React from 'react';

import { ContainerItens } from './styles';

export default function Itens(props) {
	const { title, icon } = props
	return (
		<ContainerItens>
			<img src={icon} alt=""/>
			<span>{title}</span>
		</ContainerItens>
	);
}
