import React, { useRef, useEffect} from 'react';
import Swiper from 'swiper'
import { swiperContemSlide } from '../../styles/plugins/swiper'
import Controles from '../../components/btns/Controles'

import { ContainerContem } from './styles'
import { separar } from '../../util/helper'
import Itens from './itens'
import wifi from '../../assets/contem/wifi.svg'
import restaurante from '../../assets/contem/restaurantes.svg'
import piscina from '../../assets/contem/piscina.svg'
import play from '../../assets/contem/play.svg'
import arCondi from '../../assets/contem/ar-condi.svg'
import cafe from '../../assets/contem/cafe.svg'
import garagem from '../../assets/contem/garagem.svg'
import academia from '../../assets/contem/academia.svg'
import acessibilidade from '../../assets/contem/acessibilidade.svg'
import elevador from '../../assets/contem/elevador.svg'

export default function Contem() {
	const swiperContem = useRef(null)
	let arrayItens = separar(itens, 2)

	useEffect(() => {
		new Swiper(swiperContem.current, swiperContemSlide);
	}, [])

	return (
		<ContainerContem>
			<div className="container">
				<div className="container-swiper-contem" ref={swiperContem}>
					<div className="swiper-wrapper">
						{arrayItens.map((itens, key) => renderItens(itens, key) )}
					</div>
				</div>

				<Controles esq='seta-esq-contem' dir="seta-dir-contem" />
			</div>
		</ContainerContem>
	);
}

const renderItens = (itens, key) =>{
	return (
		<div className="swiper-slide" key={key}>
			{itens.map(item => <Itens key={item.title} title={item.title} icon={item.icon} />)}
		</div>
	)
}

const itens = [
	{
		title: 'Wifi',
		icon: wifi
	},
	{
		title: 'Restaurantes',
		icon: restaurante
	},
	{
		title: 'Piscina',
		icon: piscina
	},
	{
		title: 'Playgroud',
		icon: play
	},
	{
		title: 'Ar condicionado',
		icon: arCondi
	},
	{
		title: 'Café da Manhã',
		icon: cafe
	},
	{
		title: 'Garagem',
		icon: garagem
	},
	{
		title: 'Academia',
		icon: academia
	},
	{
		title: 'Acessibilidade',
		icon: acessibilidade
	},
	{
		title: 'Elevador',
		icon: elevador
	},
]