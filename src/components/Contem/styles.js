import styled from 'styled-components';
import { Container } from '../btns/Controles/styles'

export const ContainerContem = styled.section`
	border-bottom: 1px solid #DCDCDC;
	padding-bottom: 66px;
	.container{
		position: relative;
		.container-swiper-contem{
			padding-left: 42px;
			margin-right: 25px;
			height: 136px;
			position: relative;
			overflow: hidden;
		}

		${Container}{
			.container-controles{
				z-index: 30;
				height: 0px;
				width: 105%;
				justify-content: space-between;
				right: -2.5%;
				top: 25%;
			}
		}
	}
`;


export const ContainerItens = styled.div`
	margin-bottom: 23px;
	display: flex;
	align-items: center;
	img{
		width: 45px;
		height: 45px;
		margin-right: 16px;
		display: block;
	}

	span{
		font-family: ${props => props.theme.fonts.inter};
		color: ${props => props.theme.colors.preto1};
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		line-height: 150%;
	}
`
