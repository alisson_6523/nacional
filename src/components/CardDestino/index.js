import React from 'react';

import imgDestino from '../../assets/orcamento/orcamento-08.png'
import setaDir from '../../assets/orcamento/orcamento-09.svg'
import check from '../../assets/CardReserva/reserva-02.svg'

import { ContainerCardsDestino, LinkSalas } from './styles';

export default function CardsDestino() {
	return (
		<ContainerCardsDestino>
			<div className="container-img">
				<img src={imgDestino} alt="" />
			</div>

			<div className="container-content">
				<div className="container-text">
					<h2>São Paulo</h2>

					<span><img src={check} alt="" /> 03 salas</span>
				</div>

				<div className="container-salas">
					<LinkSalas to="/sala">Ver Salas <img src={setaDir} alt="" /></LinkSalas>
				</div>
			</div>
		</ContainerCardsDestino>
	);
}
