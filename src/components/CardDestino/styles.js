import styled from 'styled-components';
import { Link } from 'react-router-dom';


export const ContainerCardsDestino = styled.div`
	border-radius: 8px;
	width: 278px;
	
	.container-img{
		width: 100%;
		height: 265px;
		img{
			width: 100%;
			height: 100%;	
			object-fit: cover;
		}
	}
	img{
		border-radius: 8px 8px 0px 0px;	
		
	}

	.container-content{
		border: 1px solid #E0E0E0;
		.container-text{
			padding-left: 40px;
			h2{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto1};
				font-style: normal;
				font-weight: 600;
				font-size: 18px;
				line-height: 100%;	
				margin-top: 24px;
				margin-bottom: 16px;
				width: 80%;

				text-overflow: ellipsis;
				white-space: pre;
				overflow: hidden;
			}

			span{
				font-family: ${props => props.theme.fonts.poppins};
				color: ${props => props.theme.colors.preto2};
				font-style: normal;
				font-weight: normal;
				font-size: 13px;
				line-height: 100%;	
				display: flex;
				align-items: center;
				margin-bottom: 24px;
				img{
					display: block;
					margin-right: 13px;
				}
			}
		}

		.container-salas{
			border-top: 1px solid #E0E0E0;
		}
	}
`;

export const LinkSalas = styled(Link)`
	font-family: ${props => props.theme.fonts.inter};
	color: ${props => props.theme.colors.blue1};
	font-style: normal;
	font-weight: 500;
	font-size: 14px;
	line-height: 160%;
	display: flex;
	align-items: center;
	height: 55px;
	padding-left: 40px;
	img{
		display: block;
		margin-left: 112px;
	}
`