import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import '../node_modules/swiper/css/swiper.min.css'
import '../node_modules/pretty-checkbox/dist/pretty-checkbox.min.css'


ReactDOM.render(<App />, document.getElementById("root"));
