export default {
	title: "",

	colors: {
		preto1: "#132847",
		preto2: "#595C76",
		preto2Opacity: "rgba(89, 92, 118, 0.3)",
		color01: "#fff",
		color02: "#FBFBFB",
		color03: "#DCDCDC",
		color04: "#404040",
		color05: "#000",
		color06: "#FAFF00",
		verde1: "#409A3C",
		verde2: "#8AC537",
		gray1: "#333333",
		gray2: "#4F4F4F",
		gray3: "#828282",
		gray4: "#BDBDBD",
		gray5: "#FEFEFE",
		gray6: '#F2F2F2',
		graybg: '#F8F9FA',
		red: "#EB5757",
		blue1: "#2F80ED",
		laranjinha: "#ffe1c666",
	},

	fonts: {
		poppins: "Poppins, sans-serif",
		lora: "lora",
		inter: "'Inter', sans-serif"
	},
};
